<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/convention-schedule.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-md-12 d-none d-md-block py-2">
                                <div class="text-center">
                                    <ul class="list-unstyled events-tabslist d-inline-block">
                                        <li class="rightborder2">
                                            <a class="event-tab event_active_tab_btn" target-id="#day_1_tab">Thu, Day 1 </a>
                                        </li>
                                        <li class="rightborder2">
                                            <a class="event-tab" target-id="#day_2_tab">FRI, DAY 2 </a>
                                        </li>
                                        <li class="rightborder2">
                                            <a class="event-tab" target-id="#day_3_tab">Sat, Day 3 </a>
                                        </li>
                                        <li class="rightborder2">
                                            <a class="event-tab" target-id="#day_4_tab">Sun, Day 4 </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12 d-block d-md-none py-2">
                                <div class="row">
                                    <div class="col-12 col-sm-6 my-2">
                                        <div class="mobile-events-tabslist">
                                            <a class="event-tab text-decoration-none event_active_tab_btn" target-id="#day_1_tab">Thu, Day 1 </a>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 my-2">
                                        <div class="mobile-events-tabslist">
                                            <a class="event-tab text-decoration-none" target-id="#day_2_tab">Fri, Day 2 </a>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 my-2">
                                        <div class="mobile-events-tabslist">
                                            <a class="event-tab text-decoration-none" target-id="#day_3_tab">Sat, Day 3 </a>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 my-2">
                                        <div class="mobile-events-tabslist">
                                            <a class="event-tab text-decoration-none" target-id="#day_4_tab">Sun, Day 4 </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row event_active_tab" id="day_1_tab">
                            <div class="col-12 px-0 px-md-3">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="datatable table-bordered table table-hover table-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>DAY</th>
                                                    <th>TIME</th>
                                                    <th>PROGRAM</th>
                                                    <th>ROOM</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td rowspan="2"><strong>Thursday, July 4th 2019</strong></td>
                                                    <td>7:00PM to 12:00AM</td>
                                                    <td>Leadership Appreciation Dinner</td>
                                                    <td>CRYSTAL/SAPPHIRE/RUBY</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>7:00PM to 11:00PM</td>
                                                    <td>Young Adults Social Mixer (Matrimony)</td>
                                                    <td>Coutyard (By Hyatt)</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="day_2_tab" style="display: none;">
                            <div class="col-12 px-0 px-md-3">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="datatable table-bordered table table-hover table-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th>S. No</th>
                                                    <th>Day</th>
                                                    <th>Time</th>
                                                    <th>Program</th>
                                                    <th>Room</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td rowspan="13">Friday, July 5th 2019</td>
                                                    <td>7:30 AM to 9:30 AM</td>
                                                    <td>Breakfast</td>
                                                    <td>DIAMOND ROOM HALLWAY</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>9:00 AM to 5:00 PM</td>
                                                    <td>Business conference</td>
                                                    <td>CRYSTAL/SAPPHIRE/RUBY</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>9:30 AM to 2:00 PM</td>
                                                    <td>Women’s Conference</td>
                                                    <td>PLATINUM BALL ROOM</td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>9:00 AM to 2:30 PM</td>
                                                    <td>Parents Meet up (Matrimony)</td>
                                                    <td>ONYX/OPAL/GARNET</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>3:00 AM to 7:00 PM</td>
                                                    <td>Young Adults Meet up (Matrimony)</td>
                                                    <td>ONYX/OPAL/GARNET</td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>11:30 PM to 2:00 PM</td>
                                                    <td>Lunch</td>
                                                    <td>DIAMOND ROOM HALLWAY</td>
                                                </tr>
                                                <tr>
                                                    <td>7</td>
                                                    <td>12:30 pm -5:15pm</td>
                                                    <td>CME</td>
                                                    <td>EMERALD/AMETHYST</td>
                                                </tr>
                                                <tr>
                                                    <td>8</td>
                                                    <td>3:00 PM to 5:00 PM</td>
                                                    <td>Youth Conference</td>
                                                    <td>PLATINUM BALL ROOM</td>
                                                </tr>
                                                <tr>
                                                    <td>9</td>
                                                    <td>6:00 PM to 12:00 AM</td>
                                                    <td>Youth Banquet</td>
                                                    <td>PLATINUM BALL ROOM</td>
                                                </tr>
                                                <tr>
                                                    <td>10</td>
                                                    <td>6:00 PM to 7:00 PM</td>
                                                    <td>Meet and Greet</td>
                                                    <td>HALL C</td>
                                                </tr>
                                                <tr>
                                                    <td>11</td>
                                                    <td>7:00 PM to 1:00 AM</td>
                                                    <td>Awards Banquet</td>
                                                    <td>HALL A</td>
                                                </tr>
                                                <tr>
                                                    <td>12</td>
                                                    <td>6:00 PM to 11:00 PM</td>
                                                    <td>Day Care</td>
                                                    <td>JADE/PEARL/CORAL</td>
                                                </tr>
                                                <tr>
                                                    <td>13</td>
                                                    <td>10:00 PM to Midnight</td>
                                                    <td>Late Night Lock-In</td>
                                                    <td>Paradise Park</td>
                                                </tr>
                                                <tr>
                                                    <td>14</td>
                                                    <td>Exhibits - Friday, July 5th Saturday, July 6th 2019</td>
                                                    <td>8:00 AM to 11:00 PM</td>
                                                    <td>Exhibits</td>
                                                    <td>HALL B</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row event_active_tab" id="day_3_tab" style="display: none;">
                            <div class="col-12 px-0 px-md-3">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="datatable table-bordered table table-hover table-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>DAY</th>
                                                    <th>TIME</th>
                                                    <th>PROGRAM</th>
                                                    <th>ROOM</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td rowspan="22"><strong>Saturday, July 6th 2019</strong></td>
                                                    <td>7:30AM to 9:30AM</td>
                                                    <td>Breakfast</td>
                                                    <td>HALL C</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>8:00 AM to 10:00 AM</td>
                                                    <td>Inaugural</td>
                                                    <td>HALL A</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>10:00 AM to 12:30 PM</td>
                                                    <td>Cultural Programs</td>
                                                    <td>HALL A</td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>10:00 AM to 1:00 PM</td>
                                                    <td>Shatamanam Bhavathi</td>
                                                    <td>2nd Half of Hall B</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>9:00 AM to 5:30 PM</td>
                                                    <td>Matrimony 1-On-1 Sessions for Parents and Young Adults</td>
                                                    <td>JADE/PEARL/CORAL</td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>2:00 PM to 5:00 PM</td>
                                                    <td>Matrimony Networking &amp; Consulting Session (Matrimony)</td>
                                                    <td>2nd Half of Hall B</td>
                                                </tr>
                                                <tr>
                                                    <td class="latobold_italic" style="text_align: center !important;">7</td>
                                                    <td>1:00 PM to 2:00 PM</td>
                                                    <td>Education Session- NRIVA Education Committee</td>
                                                    <td>ONYX/OPAL/GARNET</td>
                                                </tr>
                                                <tr>
                                                    <td>8</td>
                                                    <td>2:00 PM to 2:30 PM</td>
                                                    <td>College Prep Session by Saunak Badam</td>
                                                    <td>ONYX/OPAL/GARNET</td>
                                                </tr>
                                                <tr>
                                                    <td>9</td>
                                                    <td>11:30 AM to 2:00 PM</td>
                                                    <td>Lunch Break</td>
                                                    <td>HALL C</td>
                                                </tr>
                                                <tr>
                                                    <td>10</td>
                                                    <td>12:00 PM to 3:00 PM</td>
                                                    <td>Vasavites Got Talent</td>
                                                    <td>HALL A</td>
                                                </tr>
                                                <tr>
                                                    <td>11</td>
                                                    <td>1:00 PM to 2:30 PM</td>
                                                    <td>Financial Planning/ Real Estate/ EB5- Sponsored Sessions</td>
                                                    <td>EMERALD/AMETHYST</td>
                                                </tr>
                                                <tr>
                                                    <td>12</td>
                                                    <td>1:30 PM to 3:00 PM</td>
                                                    <td>Immigration Forum</td>
                                                    <td>PLATINUM BALL ROOM</td>
                                                </tr>
                                                <tr>
                                                    <td>13</td>
                                                    <td>12:00 PM to 3:00 PM</td>
                                                    <td>Miss and Mrs.NRIVA - Elimination Round</td>
                                                    <td>CRYSTAL/SAPPHIRE/RUBY</td>
                                                </tr>
                                                <tr>
                                                    <td>14</td>
                                                    <td>3:00 PM to 6:00 PM</td>
                                                    <td>Youth Hangout</td>
                                                    <td>CRYSTAL/SAPPHIRE/RUBY</td>
                                                </tr>
                                                <tr>
                                                    <td>15</td>
                                                    <td>3:00 PM to 6:00 PM</td>
                                                    <td>Miss and Mrs.NRIVA - Finals</td>
                                                    <td>HALL A</td>
                                                </tr>
                                                <tr>
                                                    <td>16</td>
                                                    <td>2:30 PM to 4:30 PM</td>
                                                    <td>Stocks and Investments</td>
                                                    <td>EMERALD/AMETHYST</td>
                                                </tr>
                                                <tr>
                                                    <td>17</td>
                                                    <td>3:00 PM to 4:30 PM</td>
                                                    <td>Financial Planning - Education Seminars</td>
                                                    <td>PLATINUM BALL ROOM</td>
                                                </tr>
                                                <tr>
                                                    <td>18</td>
                                                    <td>4:30 PM to 5:30 PM</td>
                                                    <td>Real Estate Session</td>
                                                    <td>EMERALD/AMETHYST</td>
                                                </tr>
                                                <tr>
                                                    <td>19</td>
                                                    <td>5:30 PM to 6:30 PM</td>
                                                    <td>Break (Buffer)</td>
                                                    <td>--</td>
                                                </tr>
                                                <tr>
                                                    <td>20</td>
                                                    <td>6:30 PM to 9:00 PM</td>
                                                    <td>Main Stage Cultural Programs</td>
                                                    <td>HALL A</td>
                                                </tr>
                                                <tr>
                                                    <td>21</td>
                                                    <td>7:30 PM to 9:30 PM</td>
                                                    <td>Dinner</td>
                                                    <td>HALL C</td>
                                                </tr>
                                                <tr>
                                                    <td>22</td>
                                                    <td>9:00 PM to 1:00 AM</td>
                                                    <td>Musical Night with Live Orchestra</td>
                                                    <td>HALL A</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row event_active_tab" id="day_4_tab" style="display: none;">
                            <div class="col-12 px-0 px-md-3">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="datatable table-bordered table table-hover table-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>DAY</th>
                                                    <th>TIME</th>
                                                    <th>PROGRAM</th>
                                                    <th>ROOM</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td rowspan="6"><strong>Sunday, July 7th 2019</strong></td>
                                                    <td>7:30 AM to 8:30 AM</td>
                                                    <td>Yoga</td>
                                                    <td>HALL A</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>7:30 AM to 9:00 AM</td>
                                                    <td>Breakfast</td>
                                                    <td>HALL C</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>9:30 AM to 2:00 PM</td>
                                                    <td>Sri Laxmi Narasimha Swamy Kalyanam</td>
                                                    <td>HALL A</td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>10:00 AM to 1:00 PM</td>
                                                    <td>Goal Setting- Workshop by Sri V V Sanyasi Rao</td>
                                                    <td>PLATINUM BALL ROOM</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>10:30 AM to 11:00 AM</td>
                                                    <td>Spiritual Talk - Blissful Living</td>
                                                    <td>HALL A</td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>1:00 PM to 4:00 PM</td>
                                                    <td>Lunch</td>
                                                    <td>HALL C</td>
                                                </tr>
                                                <tr>
                                                    <td>7</td>
                                                    <td><strong>Exhibits - Sunday, July 7th 2019</strong></td>
                                                    <td>8:00 AM to 1:00 PM</td>
                                                    <td>Exhibits</td>
                                                    <td>HALL B</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>

<script>
    $(".event-tab").click(function () {
        //active_tab
        $(".event_active_tab_btn").removeClass("event-tab");
        $(".event_active_tab_btn").removeClass("event_active_tab_btn");
        $(this).addClass("event-tab");
        $(this).addClass("event_active_tab_btn");

        $(".event_active_tab").hide();
        $(".event_active_tab").removeClass("event_active_tab");
        $($(this).attr("target-id")).addClass("event_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>
