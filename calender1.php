<?php include 'header.php';?>

<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.css'>
<link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/cupertino/jquery-ui.css'><link rel="stylesheet" href="css/style.css">

<style type="text/css">
    body{
        font-family: 'Kanit', sans-serif;
    }
    span.fc-title {
        white-space: normal;
    }
    /*.fc-view-container{
        overflow: scroll;
    }
    th.fc-week-number.ui-widget-header, td.fc-week-number, .fc-day, th.fc-day-header.ui-widget-header, td.fc-day-top.fc-sun.fc-past, td.fc-day-top.fc-mon.fc-past, td.fc-day-top.fc-tue.fc-past, td.fc-day-top.fc-wed.fc-past, td.fc-day-top.fc-thu.fc-past, td.fc-day-top.fc-fri.fc-past, td.fc-day-top.fc-sat.fc-past, .fc-day-number {
        width: 150px !important;
    }*/
</style>
    
<section class="container-fluid my-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p20">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-4 shadow-small py-3">
                <div class="row">
                    <div class="col-6 my-auto">
                        <div class="fs18">Time Compos</div>
                    </div>
                    <div class="col-6 my-auto">
                        <div class=" text-right">
                            <button class="btn btn-info"><i class="fas fa-plus fs14 pr-2"></i> New</button>
                        </div>
                    </div>  
                </div>
                <div class="col-12 shadow-small mt-3" style="height: 580px;overflow-y: auto;">
                    <div class="row">
                        <div class="col-12 px-0 border-bottom bg-light">
                            <div class="p-2">
                                <div class="border-left-2-violet  pl-2">
                                   <div>
                                        <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                        <span class="float-right">
                                            <a href="#" class="lh25 text-decoration-none text-dark fs12">Now</a>
                                        </span>
                                   </div>
                                   <div>Sri Laxmi Narasimha Swamy Kalyanam at 7:15pm...</div>
                                   <div class="my-1">
                                       <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2 my-2 d-table d-sm-block">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp; Bob, Tim</a>
                                       </span>
                                       <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2 my-2 d-table d-sm-block">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Thu Jul 4th, 10am</a>
                                       </span>
                                   </div>
                                </div>   
                            </div>
                        </div>
                        <div class="col-12 px-0 border-bottom bg-light">
                            <div class="p-2">
                                <div class="border-left-2-violet  pl-2">
                                   <div>
                                        <span class="text-violet font-weight-bold fs16">Breakfast and Matrimony</span>
                                        <span class="float-right">
                                            <a href="#" class="lh25 text-decoration-none text-dark fs12">Now</a>
                                        </span>
                                   </div>
                                   <div>Breakfast at 7:15am...</div>
                                   <div>
                                       <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2 my-2 d-table d-sm-block">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp; Bob, Tim</a>
                                       </span>
                                       <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2 my-2 d-table d-sm-block">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                       </span>
                                   </div>
                                </div>   
                            </div>
                        </div>
                        <div class="col-12 px-0 border-bottom bg-white">
                            <div class="p-2">
                                <div class="pl-2">
                                   <div>
                                        <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                        <span class="float-right">
                                            <a href="#" class="lh25 text-decoration-none text-dark fs12">Now</a>
                                        </span>
                                   </div>
                                   <div>Sri Laxmi Narasimha Swamy Kalyanam at 7:15pm...</div>
                                   <div class="my-1">
                                       <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2 my-2 d-table d-sm-block">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp; Bob, Tim</a>
                                       </span>
                                       <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2 my-2">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                       </span>
                                       <span class="bg-secondary fs13 px-2 py3 border-radius-3 float-right mt-0 mt_lg_-4">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-trash-alt text-white"></i></a>
                                       </span>
                                   </div>
                                </div>   
                            </div>
                        </div>
                        <div class="col-12 px-0 border-bottom bg-white">
                            <div class="p-2">
                                <div class="pl-2">
                                   <div>
                                        <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                        <span class="float-right">
                                            <a href="#" class="lh25 text-decoration-none text-dark fs12">Now</a>
                                        </span>
                                   </div>
                                   <div>Sri Laxmi Narasimha Swamy Kalyanam at 7:15pm...</div>
                                   <div class="my-1">
                                       <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2 my-2 d-table d-sm-block">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp; Bob, Tim</a>
                                       </span>
                                       <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2 my-2">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                       </span>
                                       <span class="bg-secondary fs13 px-2 py3 border-radius-3 float-right mt-0 mt_lg_-4">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-trash-alt text-white"></i></a>
                                       </span>
                                   </div>
                                </div>   
                            </div>
                        </div>
                        <div class="col-12 px-0 border-bottom bg-white">
                            <div class="p-2">
                                <div class="pl-2">
                                   <div>
                                        <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                        <span class="float-right">
                                            <a href="#" class="lh25 text-decoration-none text-dark fs12">Now</a>
                                        </span>
                                   </div>
                                   <div>Sri Laxmi Narasimha Swamy Kalyanam at 7:15pm...</div>
                                   <div class="my-1">
                                       <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2 my-2 d-table d-sm-block">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp; Bob, Tim</a>
                                       </span>
                                       <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2 my-2">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                       </span>
                                       <span class="bg-secondary fs13 px-2 py3 border-radius-3 float-right mt-0 mt_lg_-4">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-trash-alt text-white"></i></a>
                                       </span>
                                   </div>
                                </div>   
                            </div>
                        </div>
                        <div class="col-12 px-0 border-bottom bg-white">
                            <div class="p-2">
                                <div class="pl-2">
                                   <div>
                                        <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                        <span class="float-right">
                                            <a href="#" class="lh25 text-decoration-none text-dark fs12">Now</a>
                                        </span>
                                   </div>
                                   <div>Sri Laxmi Narasimha Swamy Kalyanam at 7:15pm...</div>
                                   <div class="my-1">
                                       <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2 my-2 d-table d-sm-block">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp; Bob, Tim</a>
                                       </span>
                                       <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2 my-2">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                       </span>
                                       <span class="bg-secondary fs13 px-2 py3 border-radius-3 float-right mt-0 mt_lg_-4">
                                           <a href="#" class="text-dark text-decoration-none"><i class="far fa-trash-alt text-white"></i></a>
                                       </span>
                                   </div>
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 col-lg-8">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
</section>

<script src='https://cdn.jsdelivr.net/npm/moment@2.24.0/min/moment.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.js'></script>
<script  src="js/script.js"></script>

<?php include 'footer.php';?>