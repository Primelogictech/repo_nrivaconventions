<?php include 'header.php';?>
	
<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small px-sm-30 py-4 p-md-4 p40">
            	<div class="text-center float-sm-none float-md-right px-2">
            		<img src="images/Hari_Raini.jpg" class="" width="160" alt="Nagender_Aytha" />
            		<div class="py-2">
            			<div class="text-danger fs16 text-center">Hari Raini</div>
            			<div class="text-danger fs12 text-center">- President</div>
            		</div>
            	</div>
                <h4 class="mb-3 text-violet">Dear Members,</h4>
                <p>
                	It gives me immense pleasure to welcome all our extended family members to our NRIVA 5th Global Convention in Detroit. I hope that this convention will serve to ignite your passion towards our beloved organization. We have planned various programs for each and everyone one in your family. We are planning to host one of the best Convention and provide lifetime memories to cherish. Our Detroit team is looking forward to meeting you all and provide excellent hospitality.
                </p>
                <p>
					I would like to thank hundreds of volunteers working tirelessly for last one year with great passion to give best experience for all of us. I would like to extend special thanks to all the donors and sponsors for their generous contributions and support.
				</p>
				<p>
					We have some new programs in this convention like Vasavites got Talent (VGT), Miss/Mrs. NRIVA Competition, Shatamanam Bhavathi, Business Conference, Women’s Conference, Youth Conference and many more. This convention is all about Learning, Business, Networking, Family Reunions, Spiritual, Fun, Entertainment and more. I hope that you all will have quality time and enjoy your time in Detroit.
				</p>
				<p>
					The mission statement of NRIVA is “Connecting to Serve”. Our goal is to connect our community and serve underprivileged and needy people. Aligning with our Mission statement we have introduced our Vision 2020 plan during January 2018to expand the vison. We have made tremendous progress in last 18 months and introduced many programs to connect and help our community. I would like to thank all our Members, Chapter leads, Committee Chairs, Co-chairs, Executive Team, Board members and all Volunteers who have put tremendous number of hours with passion, dedication and commitment to grow NRIVA to the highest level.
				</p>
				<p>
					In closing, thank you once again for taking your time to join us. It is our sincere wish that you will have an inspiring and very enjoyable time here.
				</p>
				<div>
					<div class="text-violet font-weight-bold fs16">Yours truly,</div>
					<div class="text-black fs16">Hari Raini</div>
					<div class="text-orange fs16">President, NRIVA</div>
					<div class="fs16 py-3">Visit <a href="#" class="text-orange">convention.nriva.org</a> for updates and further details</div>
				</div>
				<div class="text-violet fs15 font-weight-bold">
					Why wait? Go “Destination Detroit!”
				</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>