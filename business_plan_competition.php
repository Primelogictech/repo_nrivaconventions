<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/business-plan-competition.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row mx-md-1">
                            <div class="col-12 col-md-6 my-1 px-0 pl-md-0 pr-md-3">
                                <div>
                                    <img src="images/businees-reg.png" class="img-fluid w-100 mx-auto d-block img-b2 bg-skyblue p5" alt="Businees Reg" />
                                </div>
                            </div>
                            <div class="col-12 col-md-6 my-1 shadow-small py-3">
                                <h5 class="text-danger">Contact Details :</h5>
                                <h6><strong class="text-violet">Chair Name:</strong> <span class="text-skyblue">Raj Sohmsetty</span></h6>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">734-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">busxxxxxxxxxx@nriva.org</a></div>
                            </div>
                        </div>
                        <div class="row my-4">
                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                <h4 class="text-violet">Where Dreams Become Reality</h4>
                            </div>
                            <div class="col-12 col-md-12 col-lg-6 my-auto">
                                <div class="text-left text-lg-right">
                                    <a href="#business-plan-competition-registration" class="btn btn-lg btn-warning text-white text-decoration-none fs-xs-12 fs-xss-10">BUSINESS PLAN COMPETITION REGISTRATION</a>
                                </div>
                            </div>
                            <div class="col-12 pt-3">
                                <p class="fs16">
                                    As a testament to NRIVA's mission to promote opportunity and entrepreneurship, a business plan competition (VOVE) is being organized to coincide with NRIVA Global Convention 2019. NRIVA envisions the
                                    business plan competition to act as a platform for aspiring entrepreneurs, including both seasoned professionals and students, to turn great ideas and dreams into high quality business plans and
                                    eventually launch into successful businesses.
                                </p>
                                <p class="fs16">
                                    Some of the industry leaders and potential investors from India and US will be attending the conference. The participants will receive feedback and recommendations on their business plans.
                                </p>
                            </div>
                            <div class="col-12">
                                <h5 class="text-skyblue">The competition is Aimed at:</h5>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12 px-0 px-md-3">
                                        <ul class="business-plan-competition-list list-unstyled pl20 pr10 pt10 fs16">
                                            <li>Promoting entrepreneurship in the Vasavi community – students; or seasoned/aspiring entrepreneurs</li>
                                            <li>Stimulating business development and consequent employment in the community</li>
                                            <li>Providing support for business ideas and plans</li>
                                            <li>Encouraging entrepreneurs with potential investments and other benefits</li>
                                            <li>Providing entrepreneurs with a platform to network with other Vasavites, business professionals and investors</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pt-2">
                                <h4 class="text-violet">The Business Plan Competition Process</h4>
                                <h5 class="text-skyblue pt-1">The Following Process Will Be Adopted For This Business Plan Competition:</h5>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12 px-0 px-md-3">
                                        <ul class="business-plan-competition-list list-unstyled pl20 pr10 pt10 fs16">
                                            <li>Interested and aspiring entrepreneurs as an individual or as a team will submit their business plans by the target date Key Dates</li>
                                            <li>The business plan should follow the recommended template <a href="#" class="text-danger text-decoration-none" target="_blank">(Click here for recommended template)</a></li>
                                            <li>The business conference judges will review the business plans and select top ten contestants to present at the convention</li>
                                            <li>The selected teams will receive convention registration fee waiver (up to a maximum of 2 people per team)</li>
                                            <li>Each participant will be given 5 minutes to present and 5 minutes of Q&A with judges</li>
                                            <li>Judges will announce the top three winners on the same day</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pt-2">
                                <h4 class="text-violet">Prizes</h4>
                                <h5 class="text-skyblue pt-1">The following prizes will be awarded to the selected teams:</h5>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12 px-0 px-md-3">
                                        <ul class="business-plan-competition-list list-unstyled pl20 pr10 pt10 fs16">
                                            <li>Each of the ten finalists will receive a certificate of recognition</li>
                                            <li>
                                                The top three winners will also be awarded prize money. In addition, they will be invited to the convention banquet that evening to have an opportunity to network with potential investors,
                                                VVIPs and other dignitaries
                                            </li>
                                            <li>
                                                Prize money for top three winners:
                                                <ul class="list-unstyled business-list pl20 pt10">
                                                    <li><strong>First Price:</strong><span>$1,500</span></li>
                                                    <li><strong>Second Prize:</strong><span>$1,000</span></li>
                                                    <li><strong>Third Prize:</strong><span>$500</span></li>
                                                </ul>
                                            </li>
                                            <li>The winners will be featured in NRIVA 2019 Global convention media releases wherever applicable</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pt-2">
                                <h4 class="text-violet">Key Dates</h4>
                                <h5 class="text-skyblue pt-1">The Key Deadlines For The Competition Are:</h5>
                                <div class="mx-auto" style="max-width: 600px;">
                                    <table class="datatable table-bordered table table-hover table-center mb-0 table-striped">
                                        <tbody>
                                            <tr>
                                                <td width="5%">1</td>
                                                <td width="65%">Deadline for submitting the business plans through website</td>
                                                <td width="30%">June 16th, 2019</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Selection and announcement of the top 10 business plans</td>
                                                <td>June 23rd, 2019</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Presentation of top 10 business plans at the NRIVA Business Conference by contestants and selection/announcement of the top THREE winners of the business plan competition</td>
                                                <td>July 5th, 2019</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-12 pt-5">
                                <h4 class="text-violet">Judging / Scoring Criteria For Business Plans</h4>
                                <h5 class="text-skyblue pt-1">The Judges Look At Many Different Aspects When Evaluating Executive Summaries And Business Plans/Presentations, Including:</h5>
                                <div class="mx-auto" style="max-width: 600px;">
                                    <table class="datatable table-bordered table table-hover table-center mb-0 table-striped">
                                        <tbody>
                                            <tr>
                                                <td class="text-center" valign="center">1</td>
                                                <td width="30%">Product or Service</td>
                                                <td width="65%">Does the plan describe the product or service completely, concisely and in a way it’s easy to understand?</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">2</td>
                                                <td>Need</td>
                                                <td>Does the product or service address a specific need or solve a problem? And is there an evidence to support it?</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">3</td>
                                                <td>Competitive Advantage</td>
                                                <td>Does the plan address competition; Provide differentiators and present evidence of why the product or service is better than the competition (e.g., features, benefits, price, etc.,)</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">4</td>
                                                <td>Market analysis</td>
                                                <td>Is the target customer base, market size, defined clearly, proven, quantified and potent?</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">5</td>
                                                <td>Sales/Marketing strategy</td>
                                                <td>Clearly defined, consistent and complete?</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">6</td>
                                                <td>Revenue model</td>
                                                <td>Clearly defined, cohesive and logical?</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">7</td>
                                                <td>Management team</td>
                                                <td>Composition, roles, strength, experience, commitment, quality coming across?</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">8</td>
                                                <td>Financial planning</td>
                                                <td>Realistic and justified?</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">9</td>
                                                <td>Resource planning</td>
                                                <td>Effective identification, allocation and utilization of resources documented?</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">10</td>
                                                <td>Documentation</td>
                                                <td>Clear, concise, complete &amp; well written?</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" valign="center">11</td>
                                                <td>Presentation, Q&amp;A</td>
                                                <td>Communication &amp; clarity is compelling?</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-12 pt-5">
                                <h5 class="text-skyblue">In Addition, The Judges May Also Consider Factors Such As:</h5>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12 px-0 px-md-3">
                                        <ul class="business-plan-competition-list list-unstyled pl20 pr10 pt10 fs16">
                                            <li>Reasonable opportunity for success of the company</li>
                                            <li>Is the business opportunity/plan as presented both attractive and realistic?</li>
                                            <li>Realistic timeframe for take-off and company growth</li>
                                            <li>Well planned use of funds</li>
                                            <li>What is the amount of up-front capital investment required?</li>
                                            <li>Does the team understand its business model?</li>
                                            <li>Has the team gone out to the market already to test its ideas?</li>
                                            <li>Why is this business going to be around and a real-world winner in the next 5 years?</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <h4 class="text-violet">Participation / Eligibility / Rules:</h4>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12 px-0 px-md-3">
                                        <ul class="business-plan-competition-list list-unstyled pl20 pr10 pt10 fs16">
                                            <li>A participant can be an individual or a team which can be comprised of up to 5 members</li>
                                            <li>Entries must be business ideas for a new business or an early stage company</li>
                                            <li>All participants will submit their entire business plan as one document by the due date</li>
                                            <li>The idea included in the business plan must be an original one</li>
                                            <li>Contestants must be 18 years or older at the time of entry &nbsp;</li>
                                            <li>
                                                There is no guarantee, expressed or implied, that any team will receive an award. Should there be no entries in the competition that meet the entry criteria, or should there be no entry that
                                                has a reasonable chance for success and should the judges determine no winner exists for the competition, no award will be declared
                                            </li>
                                            <li>Each participant must sign an online waiver of eligibility and publicity/liability release &nbsp;</li>
                                            <li>
                                                Participants that have already secured funding from any source including other competitions must disclose the amounts and sources at the time of entry, or as soon as they become aware of any
                                                funding commitment during the duration of the competition
                                            </li>
                                            <li>All entries must be original and free from any claim of copyright or other restriction relating to the competition</li>
                                            <li>No individual may be included as a "team member" on more than one business plan, and no business or team may enter more than one business plan</li>
                                            <li>
                                                Awards to the winners of the competition will be issued in the name of the company as set forth in the plan and the sponsors/judges do not assume any liability for any subsequent distribution
                                            </li>
                                            <li>The decisions of the judges and NRIVA are final</li>
                                            <li>
                                                NRIVA reserves the right to change these official rules at any time, in its sole discretion, and to suspend or cancel the Competition or any entrant's participation in the Competition should
                                                unauthorized human intervention or other causes beyond NRIVA 's control, affect the administration, security, or proper conduct of the Competition. No responsibility is assumed, and entrants
                                                waive all claims for lost, late or misdirected entries, or for any problems of technical malfunction.
                                            </li>
                                            <li>
                                                Entrants who violate these official rules, tamper with the operation of the contest or engage in any conduct that is detrimental or unfair to the Competition, or any other entrant (in each
                                                case as determined in NRIVA 's sole discretion) are subject to disqualification from entry into the Competition. NRIVA reserves the right to lock out any entrant whose eligibility is in
                                                question
                                            </li>
                                            <li>
                                                By entering, participants, without limitation, release and hold harmless NRIVA and the organizers from any and all liability for any injuries, loss, or damage of any kind in connection with
                                                the Competition
                                            </li>
                                            <li>By entering this Competition, participants agree to waive any right to claim ambiguity or error in these official rules or in the Competition itself</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12" id="business-plan-competition-registration">
                                <h4 class="text-violet text-center mb-3">Business Plan Competition Registration</h4>
                                <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                                <h6 class="font-weight-bold text-center mb-3">Please make sure that you have registered for the Convention before you register for this Event.</h6>
                                <div class="row px-3 px-lg-0">
                                    <div class="col-12 col-md-12 col-lg-10 offset-lg-1 shadow-small p-3 py-md-5 px-md-4">
                                        <form>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Business Plan Name</label><span class="text-red">*</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Business Plan Name" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6 pt33">
                                                    <div>
                                                        <input type="file" name="" id="" placeholder="" />
                                                    </div>
                                                    <div class="text-danger pt-2">( Only one PDF file – Max Size 20MB *)</div>
                                                    <h6 class="text-violet pt-2">
                                                        Template is available here for Reference
                                                        <a href="documents/2019_NRIVA_GlobalCon_BPC_Business_Plan_Template-converted.pdf" class="text-danger text-decoration-underline">Click Here</a>
                                                    </h6>
                                                </div>
                                            </div>
                                        </form>
                                        <h5 class="text-violet">Principal Applicant:</h5>
                                        <form>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Name</label><span class="text-red">*</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Name" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Email</label><span class="text-red">*</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Email" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Telephone Number</label><span class="text-red">*</span>
                                                    <div>
                                                        <input type="number" name="" id="" class="form-control" placeholder="Telephone Number" />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="font-weight-bold fs16 text-center">
                                                    <span class="text-danger">Note:</span> (Before going to submit please read Terms & Conditions & - To accept & read T&C please use below scroller).
                                                </div>
                                                <div class="border_violet-4 px-3 pb30 pt30 my-3 border-radius-5">
                                                    <div class="business-plan-competition-terms-n-conditions">
                                                        <h5 class="text-violet">Terms & Conditions:</h5>
                                                        <p class="fs16">
                                                            The protection of intellectual property and the rights to the ideas contained in business plans submitted are the ultimate responsibility of each applicant, and the organizers of
                                                            the competition are not responsible for any proprietary information and/or intellectual property included in a business plan they submit to NRIVA Business Plan Competition.
                                                        </p>
                                                        <p class="fs16">
                                                            Originality of Plan. The ideas and concepts set forth in the Plan are the original work of the applicant and that the applicant are not under any agreement or restrictions that
                                                            prohibit or restrict their ability to disclose or submit such ideas or concepts to the Competition.
                                                        </p>
                                                        <p class="fs16">
                                                            Compliance with Rules of the Competition. Each applicant has reviewed the competition rules ("the Rules") and by his/her signature below certifies that this application and the
                                                            individual or team it represents complies with the Rules and agrees to abide by the Rules.
                                                        </p>
                                                        <p class="fs16">
                                                            Waivers and Releases. Each applicant understands that the NRIVA, NRIVA.org principals of the Competition, along with their employees, directors, and agents, are under no obligation
                                                            to provide any advice or service to any applicant. Each applicant further understands that the Judges are volunteers and are under no obligation to provide any advice or service to
                                                            any applicant. The views expressed by the Judges, organizers, and the organizers’ representatives are their own and not those of any other person or entity. The Judges, organizers,
                                                            and the organizers’ representatives are held harmless by each applicant and shall not be subject to any action by the applicant arising from participation in this Competition. Each
                                                            applicant also understands and agrees that although the organizers of the Competition have taken and will take the steps described in the Rules regarding confidentiality of the
                                                            ideas and plans submitted by the applicant(s), the legal protection of the Plans submitted by the applicant(s) to the Competition is the sole responsibility of the applicant(s). In
                                                            consideration of the time, expertise, and other resources provided by Competition officials, including Judges and organizers, and their representatives, each applicant hereby
                                                            voluntarily and fully releases and further agrees to fully indemnify and hold harmless NRIVA, NRIVA.org and all its employees, directors, agents, officers, members, guests,
                                                            participants, organizers, and representatives, and &nbsp;&nbsp;each Competition official and their representatives from any liabilities, obligations, responsibilities, and
                                                            accountabilities whatsoever relating to or arising out of such Competition, and such Competition officials’ or organizer representative's participation in the Competition and 2019
                                                            NRIVA Global Convention. Each applicant grants permission to the Competition to publicize their name(s) and a description of their business plan if they are named a finalist or
                                                            winner of the Competition.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="py-1 position-relative">
                                                    <input type="checkbox" class="" />
                                                    <span class="pl15 fs16">
                                                        By clicking this check box, the named applicants and the team/business they representindicate that they have read and agreed to the terms and conditions of the competition.
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="dashed-hr" />
                                        <div class="row">
                                            <div class="col-12 col-md-8 col-lg-7 pt-2">
                                                <h5>Security Code</h5>
                                                <div class="row">
                                                    <div class="col-11">
                                                        <div class="registration-captcha-block pr-3">
                                                            <div class="row">
                                                                <div class="col-8 px-0 my-auto">
                                                                    <input type="text" class="border-0 form-control bg-transparent" name="" placeholder="Enter the Characters you see" />
                                                                </div>
                                                                <div class="col-4 px-0 my-auto pr5">
                                                                    <img src="images/ShowCaptchaImage.png" class="captcha-img float-md-right" alt="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1 px-2 my-auto">
                                                        <div>
                                                            <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-4 col-lg-5 mt40">
                                                <div class="text-center text-sm-right">
                                                    <div class="btn btn-lg btn-danger text-uppercase px-5">Submit</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 pt-3">
                                <div>
                                    <img src="images/business-conf-flyer.jpg" class="img-fluid mx-auto d-block" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
