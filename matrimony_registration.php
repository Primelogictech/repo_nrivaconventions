<?php include 'header.php';?>

<style type="text/css">
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 3px;
    }
    .card-header a::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/plus.png);
    }
    .card-header a[aria-expanded="true"]::after {
        position: absolute;
        top: 0px;
        right: 0px;
        width: 60px;
        height: 52px;
        content: "";
        background-image: url(images/minus.png);
    }
</style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/matrimony.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-md-5 col-lg-4">
                                <h5 class="text-violet">Contact For More Details</h5>
                                <h6 class="">NAGENDER CHEKKA</h6>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">(248)-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">matxxxxxxxxxx2019@tta.org</a></div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-8">
                                <div class="text-center text-md-right">
                                    <a href="#matrimony-registration" class="btn btn-danger mr-2 my-1"> MATRIMONY REG.</a>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <h4 class="text-violet mb-4">Frequently asked Questions</h4>
                                <div id="accordion" class="o-accordion">
                                    <ul class="list-unstyled">
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link fs18" data-toggle="collapse" href="#collapseOne">
                                                        What are the Matrimonial Events conducted and their Schedule?
                                                    </a>
                                                </div>
                                                <div id="collapseOne" class="collapse" data-parent="#accordion">
                                                    <div class="card-body p20">
                                                        <ul class="list-unstyled matrimony-details-list fs16">
                                                            <li>
                                                                Young Adult Social Mixer at Hyatt Place in Suburban Collection Showplace.
                                                                <span class="text-danger font-weight-bold">(July 4<span class="fs13">th</span>&nbsp;7.00pm – 10.00pm)</span>
                                                            </li>
                                                            <li>
                                                                Parents Meet up <span class="text-danger font-weight-bold">(July 5<span class="fs13">th</span>&nbsp;, 9.00 am to 2.30pm)</span>
                                                            </li>
                                                            <li>
                                                                Young Adults Meet up <span class="text-danger font-weight-bold">(July 5<span class="fs13">th</span>&nbsp;3.00pm to 7.00pm)</span>
                                                            </li>
                                                            <li>
                                                                One–On-One Sessions for Parents and Young Adults&nbsp;
                                                                <span class="text-danger font-weight-bold">(July 6th , between 9.00 am to 5.30 pm by appointment<span class="fs13">&nbsp;</span>)</span>
                                                            </li>
                                                            <li>Matrimony Networking &amp; Consulting Session&nbsp;<span class="text-danger font-weight-bold">(July 6th 2.00pm to 5.00pm)</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                                        How do you register for Matrimony Events?
                                                    </a>
                                                </div>
                                                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <ul class="list-unstyled matrimony-details-list fs16">
                                                            <li>Click on the <a class="text-danger font-weight-bold" href="#matrimony-registration">(Registration link)</a>&nbsp;on this page</li>
                                                            <li>
                                                                If you have registered in the previous years for Matrimony, use your Eedu Jodu ID to Resubmit otherwise register as New.
                                                            </li>
                                                            <li>
                                                                If your Eedu Jodu ID does not work,&nbsp; Register as New.
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                                        Do Parents need a Separate Registration?
                                                    </a>
                                                </div>
                                                <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            No. Parents Registration is linked with the Respective Groom/Bride Matrimony Convention Registration
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                                        Are Walk-ins allowed?
                                                    </a>
                                                </div>
                                                <div id="collapseFour" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            Walk-ins are Welcome, but they do not get all the Registration Privileges
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
                                                        What are the Registration Privileges?
                                                    </a>
                                                </div>
                                                <div id="collapseFive" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <ul class="list-unstyled matrimony-details-list fs16">
                                                            <li>Designated Priority Seating</li>
                                                            <li>Better reach of your Profile</li>
                                                            <li>Assigned Relationship Manager Assistance</li>
                                                            <li>Access to Printed Profile Directory</li>
                                                            <li>Opt for One-On-One Sessions</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">
                                                        Do you Charge for the Registration?
                                                    </a>
                                                </div>
                                                <div id="collapseSix" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <ul class="list-unstyled matrimony-details-list fs16">
                                                            <li>
                                                                All these Matrimony Events are Sponsored and Paid by NRIVA.
                                                            </li>
                                                            <li>
                                                                During Social Mixer Event, Liquor is available for purchase
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseSeven">
                                                        What are the Highlights of Young Adults Social Mixer Event?
                                                    </a>
                                                </div>
                                                <div id="collapseSeven" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <ul class="list-unstyled matrimony-details-list fs16">
                                                            <li>Ice Breaking Sessions/DJ</li>
                                                            <li>Social Hour</li>
                                                            <li>Fun Activities</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseEight">
                                                        How is Parents meet conducted?
                                                    </a>
                                                </div>
                                                <div id="collapseEight" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <ul class="list-unstyled matrimony-details-list fs16">
                                                            <li>
                                                                All Participating Parents are divided into small groups with similar interest.
                                                            </li>
                                                            <li>
                                                                These Small Groups are formed&nbsp; based on your Partner Prefernce
                                                            </li>
                                                            <li>
                                                                Every Family gets an Opportunity to be introduced to all the available Profiles
                                                            </li>
                                                            <li>
                                                                Printed Profile Directories of Convention attendees will be provided
                                                            </li>
                                                            <li>
                                                                Once participants identify their prospective matches, they can opt for One-On-One Session
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseNine">
                                                        How is Young Adults meet conducted?
                                                    </a>
                                                </div>
                                                <div id="collapseNine" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <ul class="list-unstyled matrimony-details-list fs16">
                                                            <li>
                                                                Default seating arrangement is made based on your partner prefernce.
                                                            </li>
                                                            <li>
                                                                Seating arrangements can be reviewed and particpants have option to change if logistics permit
                                                            </li>
                                                            <li>
                                                                Once participants identify their prospective matches, they can opt for One-On-One Session
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTen">
                                                        We are not an active profile as off now, Can we attend this event for our future requirement?
                                                    </a>
                                                </div>
                                                <div id="collapseTen" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            Yes, You can attend. You are considered as Walk-in and will be seated in the location designated for walk-ins.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Nagender Chekka.jpg" class="img-fluid rounded-circle border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Nagender Chekka</h6>
                                            <div class="pt-1">Chairman</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none text-break">nchxxxxxxxxxx@hotmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 font-weight-bold text-violet">248-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Lakshmi Anumalasetty.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Lakshmi Anumalasetty</h6>
                                            <div class="pt-1">Co-Chair</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none text-break">Lakxxxxxxxxxx@yahoo.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 font-weight-bold text-violet">630-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Kalpana Sathrasala.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Kalpana Sathrasala</h6>
                                            <div class="pt-1">Co-Chair</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none text-break">ksaxxxxxxxxxx@hotmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 font-weight-bold text-violet">734-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">JayaSree Nichanametla</h6>
                                            <div class="pt-1">Board Member</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none text-break">jayxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 font-weight-bold text-violet">586-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Chandra Sekhar Repaka</h6>
                                            <div class="pt-1">Board Member</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none text-break">chaxxxxxxxxxx@yahoo.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 font-weight-bold text-violet">704-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Kalidas Varada</h6>
                                            <div class="pt-1">Board Member</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none text-break">Kalxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 font-weight-bold text-violet">410-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 col-md-6 my-1 px-2">
                                <div class="bg-white p3 shadow-small">
                                    <div class="bg-light-violet p10">
                                        <div class="row">
                                            <div class="col-4 col-sm-3 col-md-3 pr-0 my-auto">
                                                <div class="p3 border-radius-10 bg-white">
                                                    <img src="images/SRINIVAS PEDAMALLU.jpg" class="img-fluid border-radius-10 w-100" alt="SRINIVAS PEDAMALLU" />
                                                </div>
                                            </div>
                                            <div class="col-8 col-sm-9 col-md-9 px-0 my-auto">
                                                <div class="pl25 pr10">
                                                    <h5 class="mb-1">SRINIVAS PEDAMALLU</h5>
                                                    <div class="mb-1">National-Advisor</div>
                                                    <h6 class="mb-1">
                                                        <a href="#" class="text-danger text-decoration-none text-break">spexxxxxxxxxx@hotmail.com</a>
                                                    </h6>
                                                </div>
                                                <div class="phone-stiff">
                                                    <i class="fas fa-mobile-alt text-skyblue fs18 mr-2"></i>
                                                    <span class="fs20">832-XXX-XXXX</span>
                                                </div>
                                                <div class="d-block d-lg-none pl25">
                                                    <span class="fs20">832-XXX-XXXX</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 my-1 px-2">
                                <div class="bg-white p3 shadow-small">
                                    <div class="bg-light-violet p10">
                                        <div class="row">
                                            <div class="col-4 col-sm-3 col-md-3 pr-0">
                                                <div class="p3 border-radius-10 bg-white">
                                                    <img src="images/Ravi Chandra Anantha.jpg" class="img-fluid border-radius-10 w-100" alt="Ravi Chandra Anantha" />
                                                </div>
                                            </div>
                                            <div class="col-8 col-sm-9 col-md-9 px-0">
                                                <div class="pl25 pr10">
                                                    <h5 class="mb-1">Ravi Chandra Anantha</h5>
                                                    <div class="mb-1">National-Advisor</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 my-1 px-2">
                                <div class="bg-white p3 shadow-small">
                                    <div class="bg-light-violet p10">
                                        <div class="row">
                                            <div class="col-4 col-sm-3 col-md-3 pr-0">
                                                <div class="p3 border-radius-10 bg-white">
                                                    <img src="images/no-image1.jpg" class="img-fluid border-radius-10 w-100" alt="Ramesh Vardha" />
                                                </div>
                                            </div>
                                            <div class="col-8 col-sm-9 col-md-9 px-0">
                                                <div class="pl25 pr10">
                                                    <h5 class="mb-1">Ramesh Vardha</h5>
                                                    <div class="mb-1">National-Advisor</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 my-1 px-2">
                                <div class="bg-white p3 shadow-small">
                                    <div class="bg-light-violet p10">
                                        <div class="row">
                                            <div class="col-4 col-sm-3 col-md-3 pr-0">
                                                <div class="p3 border-radius-10 bg-white">
                                                    <img src="images/SURESH CHATAKONDU.jpg" class="img-fluid border-radius-10 w-100" alt="SURESH CHATAKONDU" />
                                                </div>
                                            </div>
                                            <div class="col-8 col-sm-9 col-md-9 px-0">
                                                <div class="pl25 pr10">
                                                    <h5 class="mb-1">SURESH CHATAKONDU</h5>
                                                    <div class="mb-1">Advisor - National</div>
                                                    <h6 class="mb-1">
                                                        <a href="#" class="text-danger text-decoration-none text-break">chaxxxxxxxxxx@gmail.com</a>
                                                    </h6>
                                                </div>
                                                <div class="phone-stiff">
                                                    <i class="fas fa-mobile-alt text-skyblue fs18 mr-2"></i>
                                                    <span class="fs20">609-XXX-XXXX</span>
                                                </div>
                                                <div class="d-block d-lg-none pl25">
                                                    <span class="fs20">609-XXX-XXXX</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-12" id="matrimony-registration">
                                <h4 class="text-violet text-center mb-3">Matrimony Registration</h4>
                                <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                                <h6 class="font-weight-bold text-center mb-3">Please make sure that you have registered for the Convention before you register for this Event.</h6>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-10 offset-lg-1">
                                        <div class="shadow-small px-sm-20 p40">
                                            <div class="row">
                                                <div class="col-12 col-md-6 col-lg-6">
                                                    <label class="mb-0">Do you have Eedu Jodu ID / Con Matrimony Id ?</label>
                                                </div>
                                                <div class="col-6 col-md-3 col-lg-2"><input type="radio" class="p-radio-btn" name="eedu-jodu-id" /><span class="pl20">Yes</span></div>
                                                <div class="col-6 col-md-3 col-lg-2"><input type="radio" class="p-radio-btn" name="eedu-jodu-id" /><span class="pl20">No</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
