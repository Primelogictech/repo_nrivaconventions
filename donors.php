<?php include 'header.php';?>
    
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12" id="sapphire">
                <div class="text-violet fs22">Sapphire</div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
        </div>

        <!-- Diamond -->

        <div class="row">
            <div class="col-12 mt-4 mb-2" id="diamond">
                <div class="text-violet fs22">Diamond</div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
        </div>

        <!-- End of Diamond -->

        <!-- Platinum -->

        <div class="row">
            <div class="col-12 mt-4 mb-2" id="platinum">
                <div class="text-violet fs22">Platinum</div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
        </div>

        <!-- End of Platinum -->

        <!-- Gold -->

        <div class="row">
            <div class="col-12 mt-4 mb-2" id="gold">
                <div class="text-violet fs22">Gold</div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
        </div>

        <!-- End of Gold -->

        <!-- Silver -->

        <div class="row">
            <div class="col-12 mt-4 mb-2" id="silver">
                <div class="text-violet fs22">Silver</div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
        </div>

        <!-- End of Silver -->

        <!-- Bronze -->

        <div class="row">
            <div class="col-12 mt-4 mb-2" id="bronze">
                <div class="text-violet fs22">Bronze</div>
            </div>
           <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
        </div>

        <!-- End of Bronze -->

        <!-- Patron -->

        <div class="row">
            <div class="col-12 mt-4 mb-2" id="patron">
                <div class="text-violet fs22">Patron</div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
        </div>

        <!-- End of Patron -->

    </div>
</section>

<?php include 'footer.php';?>