<?php include 'header.php';?>

<style type="text/css">
    .card {
        background-color: transparent;
        border: 0px solid;
    }
    .card-header {
        padding: 0px;
    }
    .card-header a {
        background: #784d98;
        color: #fff;
        padding: 0.75rem 1.25rem;
        width: 100%;
        display: block;
    }
    .p-radio-btn {
        position: absolute;
        top: 6px;
    }
</style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/shathamanam-bhavathi.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-md-5 col-lg-4">
                                <h5 class="text-violet">Contact For More Details</h5>
                                <h6 class="">Sreeram Kolisetty</h6>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">(617)-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">shathamanam2019@nriva.org</a></div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-8">
                                <div class="text-center text-md-right">
                                    <a href="#shathamanam-bhavathi-registration" class="btn btn-danger mr-2 my-1 fs-xss-15"> SHATHAMANAM BHAVATHI REG.</a>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <h4 class="text-violet mb-4">Shathamanam Bhavathi - Frequently Asked Questions</h4>
                                <div id="accordion" class="">
                                    <ul class="list-unstyled">
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link fs18" data-toggle="collapse" href="#collapseOne">
                                                        Who can participate in the program?
                                                    </a>
                                                </div>
                                                <div id="collapseOne" class="collapse" data-parent="#accordion">
                                                    <div class="card-body p20">
                                                        <p class="mb-0">
                                                            All NRIVA members and/or their parents or in-laws who are above 60 are eligible to register themselves or their parents/in-laws for the event.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                                        Is there a registration fee for the event?
                                                    </a>
                                                </div>
                                                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p>
                                                            If you are a Sponsor of the event one couple is allowed per family. If you are not a sponsor of the convention event there is a registration fee of $300 per participating couple.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                                        Can I have both my parents and in-laws participate in the event?
                                                    </a>
                                                </div>
                                                <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            Yes, but note that only 102 couples are allowed on a first come first serve basis. Registration fee is $300 per couple if you are not a sponsor to the convention.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                                        What if we register but unable to participate in the event?
                                                    </a>
                                                </div>
                                                <div id="collapseFour" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            Fee paid is non-refundable and non-transferable. We encourage you to participate in the event, however if you are unable to participate, please let the Shathamanam Bhavathi
                                                            committee members about your absence. This will help another couple to participate in the event as there is a huge demand from the participants.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
                                                        Can I sponsor any other family member for the event (like Grandparents, brother, sister etc)
                                                    </a>
                                                </div>
                                                <div id="collapseFive" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            Yes, as long as you are member of NRIVA you are allowed to sponsor any of your relatives.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">
                                                        Are we expected to bring anything for this event?
                                                    </a>
                                                </div>
                                                <div id="collapseSix" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            No, you are not required to bring anything for this event. However, if you want to get new clothes for the couple you may get it, organizers will not be providing any new clothes
                                                            to the couple. All pooja related items will be provided by organizers of the event.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseSeven">
                                                        Do you take any photos and videos of the event?
                                                    </a>
                                                </div>
                                                <div id="collapseSeven" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            As a part of the event, our photographers and participants will be taking the photographs. You are allowed to take the Photographs and videos as long as it is not disturbing the
                                                            event. If the event is getting disturbed, organizers have the right to ask you to stop taking the photographs and videos.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseEight">
                                                        How will these photos / videos be used?
                                                    </a>
                                                </div>
                                                <div id="collapseEight" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            Event organizers may share these photos and videos on the social media to promote the events in the subsequent year, cultural awareness, educational activities and any other
                                                            non-profit motives. By participating in this event, you are providing the consent to share these photos and videos to the organizers.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pb15 mb15 border-bottom">
                                            <div class="card">
                                                <div class="card-header fs18">
                                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseNine">
                                                        Who can I contact for more information?
                                                    </a>
                                                </div>
                                                <div id="collapseNine" class="collapse" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <p class="mb-0">
                                                            For more information you are encouraged to visit www.nriva.org or reach out to any members of Shathamanam Bhavathi event.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-12" id="shathamanam-bhavathi-registration">
                                <h4 class="text-violet text-center mb-3">Shathamanam Bhavathi Registration</h4>
                                <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                                <h6 class="font-weight-bold text-center">Please make sure that you have registered for the Convention before you register for this Event.</h6>
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-10 offset-lg-1">
                                        <div class="shadow-small px-sm-20 p40">
                                            <form>
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Name:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Email:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Email" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-4 col-lg-4">
                                                        <label>Fathers (In-Laws) Name:</label>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Fathers (In-Laws) Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-4 col-lg-4">
                                                        <label>Mothers (In-Laws) Name:</label>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Mothers (In-Laws) Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-4 col-lg-4">
                                                        <label>Gothram:</label>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Gothram" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                            <h5 class="text-violet mt-3">Please check your Sponsorship Level:</h5>
                                            <div class="row my-1">
                                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 my-1"><input type="radio" class="p-radio-btn" name="sponsorship" /><span class="pl25 fs16">Diamond</span></div>
                                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 my-1"><input type="radio" class="p-radio-btn" name="sponsorship" /><span class="pl25 fs16">Platinum</span></div>
                                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 my-1"><input type="radio" class="p-radio-btn" name="sponsorship" /><span class="pl25 fs16">Gold</span></div>
                                            </div>
                                            <div class="row my-1">
                                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 my-1"><input type="radio" class="p-radio-btn" name="sponsorship" /><span class="pl25 fs16">Silver</span></div>
                                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 my-1"><input type="radio" class="p-radio-btn" name="sponsorship" /><span class="pl25 fs16">Bronze</span></div>
                                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 my-1"><input type="radio" class="p-radio-btn" name="sponsorship" /><span class="pl25 fs16">Sapphire</span></div>
                                            </div>
                                            <div class="row my-1">
                                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 my-1"><input type="radio" class="p-radio-btn" name="sponsorship" /><span class="pl25 fs16">Patron</span></div>
                                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 my-1"><input type="radio" class="p-radio-btn" name="sponsorship" /><span class="pl25 fs16">Not a Sponsor</span></div>
                                            </div>
                                            <hr class="dashed-hr" />

                                            <div class="row">
                                                <div class="col-12 col-md-9 col-lg-9 my-2 my-md-auto">
                                                    <h5 class="text-violet mb-0">Parents / In-Laws Coming from different country for the event:</h5>
                                                </div>
                                                <div class="col-12 col-md-3 col-lg-3 my-2 my-md-auto">
                                                    <div>
                                                        <span class="mr-3"> <input type="radio" class="p-radio-btn" name="event" /><span class="pl25 fs16">Yes</span> </span>
                                                        <span class=""> <input type="radio" class="p-radio-btn" name="event" /><span class="pl25 fs16">No</span> </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mt-3">
                                                <div class="col-12 border-green-2 border-radius-5 p20">
                                                    <h5 class="text-violet text-center mb-0">Father (In-Law) Details</h5>
                                                </div>
                                                <div class="col-12 mt-3">
                                                    <form>
                                                        <div class="form-row">
                                                            <div class="form-group col-12 col-md-6 col-lg-4">
                                                                <label>DOB:</label>
                                                                <div class="border border-radius-5">
                                                                    <input type="" name="" id="datepicker" class="dob-input border-0" placeholder="DOB" />
                                                                    <span class="input-group-addon"><i class="fas fa-times"></i></span>
                                                                    <span class="input-group-addon"><i class="fas fa-th"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-12 col-md-6 col-lg-4">
                                                                <label>Naksthram:</label>
                                                                <div>
                                                                    <input type="text" name="" id="" class="form-control" placeholder="Naksthram" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-12 col-md-6 col-lg-4">
                                                                <label>Rasi:</label>
                                                                <div>
                                                                    <input type="text" name="" id="" class="form-control" placeholder="Rasi" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="row mt-3">
                                                <div class="col-12 border-green-2 border-radius-5 p20">
                                                    <h5 class="text-violet text-center mb-0">Mother (In-Law) Details</h5>
                                                </div>
                                                <div class="col-12 mt-3">
                                                    <form>
                                                        <div class="form-row">
                                                            <div class="form-group col-12 col-md-6 col-lg-4">
                                                                <label>DOB:</label>
                                                                <div class="border border-radius-5">
                                                                    <input type="" name="" id="datepicker" class="dob-input border-0" placeholder="DOB" />
                                                                    <span class="input-group-addon"><i class="fas fa-times"></i></span>
                                                                    <span class="input-group-addon"><i class="fas fa-th"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-12 col-md-6 col-lg-4">
                                                                <label>Naksthram:</label>
                                                                <div>
                                                                    <input type="text" name="" id="" class="form-control" placeholder="Naksthram" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-12 col-md-6 col-lg-4">
                                                                <label>Rasi:</label>
                                                                <div>
                                                                    <input type="text" name="" id="" class="form-control" placeholder="Rasi" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <hr class="dashed-hr" />

                                            <div class="row">
                                                <div class="col-12 col-md-7 col-lg-6 my-1 my-md-auto">
                                                    <h5 class="text-violet">Number of Participants from your family:</h5>
                                                </div>
                                                <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto">
                                                    <div>
                                                        <input type="number" class="form-control" name="" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <div>
                                                        <label>Any Instructions to Sponsors:</label>
                                                    </div>
                                                    <textarea class="form-control w-100" rows="2"></textarea>
                                                </div>
                                            </div>

                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <div class="border_violet-4 px-3 pb30 pt30 my-3 border-radius-5">
                                                        <div class="shathamanam-bhavathi-reg-terms-n-conditions">
                                                            <h5 class="text-violet">Terms & Conditions:</h5>
                                                            <p>
                                                                <strong>1.</strong> Maximum of 102 couples are allowed for the event and purely on first come first serve basis. Anything above 102 couples is at the discretion of event
                                                                organizers.
                                                            </p>
                                                            <p><strong>2.</strong> Maximum of 8 people are allowed to sit around the participating couples (excludes kids of age less than 10).</p>
                                                            <p><strong>3.</strong> Participants are expected to be physically able to perform the rituals without assistance from another person.</p>
                                                            <p>
                                                                <strong>4.</strong> Photographs and Videos taken during the program may be shared in social and electronic media and by accepting these terms and conditions you are providing
                                                                consent to share the photos and videos.
                                                            </p>
                                                            <p><strong>5.</strong> Participants are advised to keep any medication and water as required for their needs.</p>
                                                            <p><strong>6.</strong> These terms and conditions are subject to change as required to support the smooth functioning of the ceremony.</p>
                                                            <p><strong>7.</strong> Please speak to the convention committee and chairs of the program for any questions that you may have.</p>
                                                        </div>
                                                    </div>
                                                    <div class="py-1 position-relative">
                                                        <input type="checkbox" class="" />
                                                        <span class="pl15 fs16">
                                                            By clicking this check box, you and participants are agreeing to the above terms and conditions. It is responsibility of the registering person to update about the terms and
                                                            conditions to their parents/in-laws or participants.
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mt-3">
                                                <div class="col-12 col-md-8 col-lg-7 pt-2">
                                                    <h5>Security Code</h5>
                                                    <div class="row">
                                                        <div class="col-11">
                                                            <div class="registration-captcha-block pr-3">
                                                                <div class="row">
                                                                    <div class="col-8 px-0 my-auto">
                                                                        <input type="text" class="border-0 form-control bg-transparent" name="" placeholder="Enter the Characters you see" />
                                                                    </div>
                                                                    <div class="col-4 px-0 my-auto pr5">
                                                                        <img src="images/ShowCaptchaImage.png" class="captcha-img float-md-right" alt="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-1 px-1 my-auto">
                                                            <div>
                                                                <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-4 col-lg-5 mt40">
                                                    <div class="text-center text-sm-right">
                                                        <div class="btn btn-lg btn-danger text-uppercase px-5">Submit</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $("#datepicker").datepicker();
    });
</script>

<?php include 'footer.php';?>
