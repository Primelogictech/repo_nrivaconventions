<?php include 'header.php';?>
	
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small">
        <div class="row">
            <div class="col-12 px-0">
                <div class="leadership-heading-bg p-3">
                    <h4 class="mb-0">Our Convention Convener's Corner Committee</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 py-4 p-md-4 p40">
            	<div class="row">
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Ramesh Kalwala</h6>
		            				<div class="pt-1">2011</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ramxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">571-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 pt-3">
		            				<h6 class="mb-0">Hanuman Nandanampati</h6>
		            				<div class="pt-1">2013</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">SRINIVAS AKULA</h6>
		            				<div class="pt-1">2015</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">akuxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">614-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">TP SRINIVASA RAO</h6>
		            				<div class="pt-1">2017</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">raoxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">732-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">VIJAYBHASKAR PALLERLA</h6>
		            				<div class="pt-1">2019</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">reaxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>