<?php include 'header.php';?>
    
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <div class="text-violet fs22">Sapphire</div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>