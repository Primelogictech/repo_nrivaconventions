<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/v-got-talent.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row mx-md-1">
                            <div class="col-12 col-md-6 my-1 px-0 pl-md-0 pr-md-3">
                                <div>
                                    <img src="images/v-got.png" class="img-fluid w-100 mx-auto d-block img-b2 bg-skyblue p5" alt="Businees Reg" />
                                </div>
                            </div>
                            <div class="col-12 col-md-6 shadow-small py-3 my-1">
                                <h5 class="text-danger">Contact Details :</h5>
                                <h6><strong class="text-violet">Chair Name:</strong> <span class="text-skyblue">Raj Kumar Dhubba</span></h6>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">vgt2019@nriva.org</a></div>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">678-XXX-XXXX</span></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 pt-3">
                                <p>
                                    Are you a Singer, Dancer, Mimicry artist, Juggler, Musician, Comedian, Hypnotist, or have Special skills in Education or have any other talent that you would like to present in front of a huge audience at a
                                    world class convention in Detroit during July 5th-7th 2019?
                                </p>
                                <h4 class="text-violet">
                                    There is no restriction on the Language or Skill. Age and gender is not a barrier.
                                </h4>
                                <p>
                                    We have seen your talents in Singing, Dancing and Education Competitions in our previous conventions. There is still a lot of undiscovered talent that you could amaze us with.
                                </p>
                                <p>
                                    With the introduction of<span class="text-skyblue"> V GOT TALENT</span>, we are widening the competition horizon and bringing undiscovered, unmatched talents of participants of all ages with no restriction on
                                    the type of performance, language and age.
                                </p>
                            </div>
                            <div class="col-12">
                                <h6 class="text-danger font-weight-bold">COMPETITION FORMAT:</h6>
                                <h6 class="text-skyblue">Level 1:</h6>
                                <p>
                                    You appear before the judges in your chapters and showcase your skills during NRIVA Days. Two finalists from each chapter will advance to the finals that will be held in Detroit during the NRIVA 5th Global
                                    Convention.
                                </p>
                                <ul class="list-unstyled fs16 miss-nriva-mrs-nriva-details-list pl20">
                                    <li>
                                        Both Level 1 and Level 2 will have only one round. You may or may not repeat the same performance at the nationals.
                                    </li>
                                    <li>
                                        Preference will be given to the extraordinary performances that the judges think are the most entertaining
                                    </li>
                                    <li>
                                        Interested participants can register at the below link with the description of your act.
                                    </li>
                                </ul>
                                <h6 class="text-skyblue">Level 2:</h6>
                                <p class="mb-2">
                                    Level 2 is the final competition where two finalists from each chapter will compete for a winner and runner-up positions at the national level during the Convention.
                                </p>
                                <h6 class="text-danger font-weight-bold">Rules and Guidelines:</h6>
                                <ul class="list-unstyled fs16 miss-nriva-mrs-nriva-details-list pl20">
                                    <li>
                                        There is no age, gender, language and type of act restrictions.
                                    </li>
                                    <li>
                                        All finalists will have to register for the NRIVA 5th Global Convention in Detroit.
                                    </li>
                                    <li>Length of your act can not exceed 4 minutes.</li>
                                    <li>
                                        You can bring any props, instruments etc as long as they can be accommodated and have no concerns to the organizer. Please check with the committee contacts before you plan on bringing any of them.
                                    </li>
                                    <li>
                                        Judges decision is final and no enquiries are entertained.
                                    </li>
                                    <li>
                                        Only NRIVA members residing in USA are eligible to enter in this contest
                                    </li>
                                    <li>
                                        Winner and Runner may get a chance to perform on the main stage on one of the days during the Convention. It will be intimated to them in advance.
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 pt-3">
                                <h4 class="text-violet text-center mb-3">V Got Talent Registration</h4>
                                <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                                <h6 class="font-weight-bold text-center mb-3">Please make sure that you have registered for the Convention before you register for this Event.</h6>
                                <div class="pt-3">
                                    <img src="images/V-Got-Flyer-Flyer.jpeg" class="img-fluid mx-auto d-block" alt="Spiritual Flyer" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
