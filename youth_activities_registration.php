<?php include 'header.php';?>

<section class="container-fluid my-3 px-1-2 px-md-3 my-md-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1 pb-md-5">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/youth-activities.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-md-5 col-lg-4">
                                <h5 class="text-violet">Contact For More Details</h5>
                                <h6 class="">Vikhyathi Pallerla</h6>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">248-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">youth2019@nriva.org</a></div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-8">
                                <div class="text-center text-md-right">
                                    <a href="#youth-activity-registration" class="btn btn-danger mr-2 my-1">Youth Activities Reg.</a>
                                    <a href="documents/NRIVA-YOUTH-ACTIVITIES-WEBINAR.pdf" class="btn btn-info my-1 fs-xs-15" target="_blank">Please go through the above document before registering</a>
                                </div>
                            </div>
                            <div class="col-12 pt-3">
                                <h5 class="text-violet">Paradise Park Late Night Lock-In</h5>
                                <p>
                                    A unique experience where the youth will be locked into Paradise Park for the night of their lives! Paradise Park is an arcade center with both indoor and outdoor activities and when you sign up, you get
                                    access to everything the park has to offer! This night features unlimited:
                                </p>
                                <ul class="youth-activity-registration-list list-unstyled pl20 pr10 pt10 fs16 pt-0">
                                    <li>Go-karting</li>
                                    <li>Laser tag</li>
                                    <li>Dance Dance Revolution</li>
                                    <li>Mini Golf</li>
                                    <li>Trampolines</li>
                                    <li>Refreshments and more!</li>
                                </ul>
                            </div>
                            <div class="col-12 pt-3">
                                <h5 class="text-violet">Lights Camera Action!</h5>
                                <p>
                                    A Hollywood-themed awards-styled banquet, this event will take place the night of July 5th as an alternative youth banquet. Dress code is American formal and come ready to show off your movie knowledge
                                    and meet some new people! There will be:
                                </p>
                                <ul class="youth-activity-registration-list list-unstyled pl20 pr10 pt10 fs16 pt-0">
                                    <li>Photo booth</li>
                                    <li>Awards show themed buffet</li>
                                    <li>Movie trivia</li>
                                    <li>Oscar styled awards (try to get Best Dressed!)</li>
                                </ul>
                            </div>
                            <div class="col-12 pt-3">
                                <h5 class="text-violet">Imagine, Inspire, Ignite</h5>
                                <p>
                                    This event is a professional and motivational conference geared towards the leaders of the future. It will feature talks by many accomplished people along with career building and networking activities.
                                </p>
                            </div>
                            <div class="col-12 pt-3" id="youth-activity-registration">
                                <h4 class="text-violet text-center mb-3">Youth Activities Registration</h4>
                                <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                                <h6 class="font-weight-bold text-center mb-3">Please make sure that you have registered for the Convention before you register for this Event.</h6>
                                <div class="row">
                                    <div class="col-12 col-lg-10 offset-lg-1 mt-3">
                                        <div class="shadow-small py-5 px-3 px-md-4">
                                            <form>
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>First Name:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="First Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Email:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Email" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Age:</label><span class="text-red">*</span>
                                                        <div>
                                                            <select class="form-control">
                                                                <option>Select Age</option>
                                                                <option>1</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Chapters:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Email" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div>
                                                <h5 class="text-violet">What events will you be attending?:</h5>
                                                <div class="py-1"><input type="checkbox" class="" /><span class="pl15 lh35 fs16">Imagine, Inspire, Ignite (Youth Conference)</span></div>
                                                <div class="py-1"><input type="checkbox" class="" /><span class="pl15 lh35 fs16">Lights Camera Action! (Youth Alternative Banquet)</span></div>
                                            </div>
                                            <div class="border_violet-4 my-2 pt30 pb30 px-3 border-radius-5">
                                                <h5 class="text-violet">Terms & Conditions:</h5>
                                                <div class="py-1 position-relative">
                                                    <input type="checkbox" class="" />
                                                    <span class="pl15 fs16">
                                                        By clicking this check box, you and participants are agreeing to the above terms and conditions. It is responsibility of the registering person to update about the terms and conditions
                                                        to their Activity.
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-md-8 col-lg-7 pt-2">
                                                    <h5>Security Code</h5>
                                                    <div class="row">
                                                        <div class="col-11">
                                                            <div class="registration-captcha-block pr-3">
                                                                <div class="row">
                                                                    <div class="col-8 px-0 my-auto">
                                                                        <input type="text" class="border-0 form-control bg-transparent" name="" placeholder="Enter the Characters you see" />
                                                                    </div>
                                                                    <div class="col-4 px-0 my-auto pr5">
                                                                        <img src="images/ShowCaptchaImage.png" class="captcha-img float-md-right" alt="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-1 px-1 my-auto">
                                                            <div>
                                                                <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-4 col-lg-5 mt40">
                                                    <div class="text-center text-sm-right">
                                                        <div class="btn btn-lg btn-danger text-uppercase px-5">Submit</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
