<?php include 'header.php';?>
    
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <h4 class="mb-4 text-violet">NRIVA Preferred Hotels</h4>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-skyblue">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-four-point.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Four Points by Sheraton Detroit Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://www.google.com/maps/place/Hilton+Garden+Inn+Detroit%2FNovi,+27355+Cabaret+Dr,+Novi,+MI+48377,+United+States/@42.4914694,-83.4879927,17z/data=!4m2!3m1!1s0x8824af22f08a23bf:0x4ead5d9a36895d2" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-light-orange">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-1.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Hilton Garden Inn Detroit/Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/yhXmYcoFp4xfUYF27" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-light-orange">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hyatt-place-detroit-novi.jpg" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Hyatt Place Detroit/Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/yhw7pfhQ3Lugz844A" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-12 pt-4">
                <h4 class="mb-4 text-violet">NRIVA Nearby Hotels</h4>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-skyblue">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/sheraton-detroit-novi-hotel.jpg" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Sheraton Detroit Novi Hotel</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/HGXTFBTArama79k17" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-light-orange">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/staybridge-suites-detroit-novi.jpg" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Staybridge Suites Detroit Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/53EwmgpWpxSYfXGD6" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-light-orange">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hilton-garden-inn-detroit-novi.jpg" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Hilton Garden Inn Detroit/Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/6wStDjUp3vJjkEkX6" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-skyblue">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-21.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Red Roof Inn Detroit - Farmington Hills</h6>
                    <div class="text-center fs16">
                        <a href="https://g.page/RedRoofInnDetroitFarmingtonHills?share" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
        </div>

        <div class="row pt-2">
             <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-skyblue">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/residence-inn-by-marriott-detroit-novi.jpg" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Residence Inn by Marriott Detroit Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/rGNTWzePeNiPHjrq7" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
             <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-light-orange">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-four-point.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Four Points by Sheraton Detroit Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://www.google.com/maps/place/Hilton+Garden+Inn+Detroit%2FNovi,+27355+Cabaret+Dr,+Novi,+MI+48377,+United+States/@42.4914694,-83.4879927,17z/data=!4m2!3m1!1s0x8824af22f08a23bf:0x4ead5d9a36895d2" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-light-orange">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-4.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">DoubleTree by Hilton Detroit - Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/YtBVGk7TtRnLM4S7A" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
             <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-skyblue">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-5.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Holiday Inn Express Novi Northwest Wixom</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/tfESD4QAM1EbAzqc6" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
        </div>

        <div class="row pt-2">
             <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-skyblue">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/residence-inn-by-marriott-detroit-novi.jpg" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Residence Inn by Marriott Detroit Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/rGNTWzePeNiPHjrq7" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
             <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-light-orange">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-four-point.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Four Points by Sheraton Detroit Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://www.google.com/maps/place/Hilton+Garden+Inn+Detroit%2FNovi,+27355+Cabaret+Dr,+Novi,+MI+48377,+United+States/@42.4914694,-83.4879927,17z/data=!4m2!3m1!1s0x8824af22f08a23bf:0x4ead5d9a36895d2" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-light-orange">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-4.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">DoubleTree by Hilton Detroit - Novi</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/YtBVGk7TtRnLM4S7A" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
             <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-skyblue">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-5.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Holiday Inn Express Novi Northwest Wixom</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/tfESD4QAM1EbAzqc6" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>