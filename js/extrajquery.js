﻿
$(document).ready(function () {

    //top scrolling


    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.topa').fadeIn('slow');
        } else {
            $('.topa').fadeOut('slow');
        }
    });


    $('.topa').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
	
	
	// for menu dropdowns

    $(".dropdown").hover(function () {

        $(".dropdown-menu", this).show();
        $(".dropdown-menu", this).addClass('animated fadeIn');

    }, function () {

        $(".dropdown-menu", this).hide();

    });

});