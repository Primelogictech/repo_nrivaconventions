<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/immigration-forum-speakers.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Bhanu B. Ilindra
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/bhanu-b-ilindra1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <h5 class="mb-3">
                                                <span class="text-danger">Topic :</span>
                                                <span class="text-violet">H-4 Dependent Spouse Work Authorization</span>
                                            </h5>
                                            <p class="py-3"></p>
                                            <h5 class="text-skyblue text-center">Coming Soon ....</h5>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Sweekrutha Shankar
                                        <span class="text-yellow fs16 pl10">- founder and partner of Shankar Ninan & amp; Co.</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/sweekrutha2.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <h5 class="mb-3">
                                                <span class="text-danger">Topic :</span>
                                                <span class="text-violet">H-1B denials and alternative</span>
                                            </h5>
                                            <p>
                                                Swee Shankar is a founder and partner of Shankar Ninan & Co. Ms. Shankar brings a wealth of experience in Immigration and Nationality laws. Ms. Shankar is an active member of the American
                                                Immigration Lawyers Association.
                                            </p>
                                            <p>
                                                In the field of immigration law, Ms. Shankar assists corporations seeking to hire foreign professionals or skilled workers or to transfer personnel to company branches in the United States.
                                                Ms. Shankar is a trusted adviser and a major asset to her clients, including large consulting companies for which she obtains visas, green cards and naturalization. She also advises clients in
                                                U.S. EB-5 investor visa applications. Ms. Shankar did a EB-5 program For India’s Investors podcast which has received over 15,000 views on YouTube®. She has nearly 40 5-star client reviews
                                                online and is the recipient of the SuperLawyers Rising Stars award for 2018-2019.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Joseph Kallabat
                                        <span class="text-yellow fs16 pl10">- Founded Joseph Kallabat & Associates</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/joy1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <h5 class="mb-3">
                                                <span class="text-danger">Topic :</span>
                                                <span class="text-violet">Green Card Processing delays</span>
                                            </h5>
                                            <p>
                                                Joseph Kallabat has been practicing with in the field of Immigration Law for over 23 years. In 1997, he founded Joseph Kallabat & Associates, P.C. in West Bloomfield, Michigan, which focuses
                                                on serving its client’s business and professional needs in the areas of Employment-Based Immigration. Mr. Kallabat is an active member of the Michigan Bar Association and the American
                                                Immigration Lawyers Association (AILA). His clientele include small start-up companies to large multinational publicly traded companies within the I.T. Staffing, Engineering, Pharmaceutical,
                                                Medical and Financial industries. His firm utilizes an entrepreneurial approach to its client’s immigration and business needs to provide a comprehensive solution to a variety of legal issues.
                                                Mr. Kallabat will typically partner with human resource personnel, global mobility specialist, in-house legal counsel, and recruiters to provide training and insight into the rapidly changing
                                                and evolving field of U.S. Immigration Law. Mr. Kallabat’s firm also handles Department of Labor and Department of Homeland Security audits, compliance reviews, and investigations.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Rajaguru Nalliah
                                        <span class="text-yellow fs16 pl10">- Managing Attorney of IMMLAWS</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/raj-guru1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <h5 class="mb-3">
                                                <span class="text-danger">Topic :</span>
                                                <span class="text-violet">Master Students Second masters Day 1 CPT issues</span>
                                            </h5>
                                            <p>
                                                Raj Nalliah, managing attorney of IMMLAWS, is admitted to practice law in Ohio and Michigan, and member of bar of U.S. Federal Court. He has 23 years of law practice and 15 years in the U.S.
                                                Immigration law. He worked for an IT Consulting Company in their immigration department for 7 years, managing the company's H1B, L1, and Green Card requirements. In his practice, he has
                                                successfully filed thousands of H1B/L1, and Green Card petitions. After starting his private law practice in 2008, he is practicing exclusively Immigration law and assisting hundreds of
                                                Information Technology firms all over the United States.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<script>
    $(".speakers-more-details").click(function () {
        $(this).parent().parent().parent().find(".speakers-content").toggleClass("speakers-details-height");

        if ($(this).text() == "Show More +") {
            $(this).text("Show Less - ");
        } else {
            $(this).text("Show More +");
        }
    });
</script>

<?php include 'footer.php';?>
