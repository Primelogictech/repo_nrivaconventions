<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/youth-conference-speakers.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Past, Present & Future: Journey of a Silicon Valley Entrepreneur
                                        <span class="text-yellow fs16 pl10">- By Vijay Pradeep</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/Vijay1Pradeep1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <p>
                                                Vijay Pradeep is a Silicon Valley robotics engineer and angel investor, and has been involved in the robotics sector for over a decade. He was a Co-Founder of hiDOF, Inc., a Silicon Valley
                                                robotics consulting firm, that has worked with Fortune 500 companies and startups in a variety of areas, including VR/AR, vehicle autonomy, medical imaging, drones, personal aviation, and
                                                industrial automation. His team joined Google’s Daydream VR effort, where Vijay led the sensor characterization and factory calibration teams, helping to launch Daydream View, Tango,
                                                &amp;ARCore.
                                            </p>
                                            <p>
                                                Vijay received his B.S. in Computer Systems, focusing on Robotics and Mechatronics, and his M.S. in Mechanical Engineering focusing on Control Systems, both from Stanford University. His
                                                academic contributions have been showcased in a variety of robotics conferences, journals, and books. Vijay’s contributions to ROS (Robot Operating System) and the PR2 robot platform are now
                                                used widely in the open source robotics community.
                                            </p>
                                            <p>
                                                Vijay splits his time between San Francisco, California, USA and St Augustine, Trinidad &amp; Tobago.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        The Influence of Spiritual Connection on Happiness
                                        <span class="text-yellow fs16 pl10">- By Vaishnavi Vura</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/Vaishnavi-Vura1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <p>
                                                Vaishnavi Vura is going to be a senior at South Fayette High School. This year she discovered her passion for public speaking. Prior moving to Pennsylvania, she lived in Hyderabad,India
                                                where she learnt things about her culture and traditions. After coming to Pennsylvania she started attending Chinmaya Mission and discovered her love for Hindu scriptures. Vaishnavi took
                                                this love for her scripture and presented a TEDx talk at her school. Her talk was how to use a principle called "karma yoga" to discover the inner happiness within a person.
                                            </p>
                                            <p>
                                                Vaishnavi is very active and started many initiatives in her school. She is the founder of the environmental club- which is a club making the school more eco friendly and she started an
                                                organ donation project. Organ donation is something she is very passionate about and she has organized a school wide assembly and delivered a 45 minute talk to the entire student body on
                                                why they should be an organ donor. She has hosted a booth at a local event and convinced many people to become donors. So far Vaishnavi has convinced 10 people to become donors- saving
                                                over 80 lives indirectly.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Music to Politics, Igniting the World
                                        <span class="text-yellow fs16 pl10">- By Vaishnavi Vura</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/Hemanth-Tadepalli1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <p>
                                                Hemanth Tadepalli is from Troy. He attends Kettering University and will be studying Cyber Defense. Hemanth has been a local government official for the City of Troy for a long time. He
                                                serves as the President of the Troy Public Library’s advisory Board, Vice President of the Police Department’s Advisory Board and now serves as a Planning Commission Representative for the
                                                City of Troy. Hemanth has been in charge/part of many campaigns across the state and country for a long time. He has worked with our very own Governor Whitmer, Congresswoman Haley Stevens,
                                                Barack Obama, Ethan Baker and many more. Hemanth’s vision is to inspire the youth in being part of local movements and show that activism plays a role in the community you live in.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Efficient Ways to Follow A Unique Passion
                                        <span class="text-yellow fs16 pl10">- By Manideep Telukuntla</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/manideep-telukuntla1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <p>
                                                ManideepTelukuntla is a Game Development freelance consultant. His previously worked on titles include: SMITE, Tribes Ascend, &DayZ. Notable achievements include: Making Awesome
                                                Sandwiches, Draining a One handed, backwards, half-court basketball shot and Winning a staring match against a Squirrel. He is a 3rd year Cyber Security Undergrad at George Mason
                                                University. Currently works as a Combat Animator for Phoenix Labs on the game Dauntless.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        The Never-Ending Story
                                        <span class="text-yellow fs16 pl10"> - By Alekhya Sure</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/Alekhya-Sure1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <p>
                                                Alekhya finds joy in helping others help themselves. During her day job, she empowers analysts and community partners through recruiting reform, internal mentoring, and personal
                                                branding/performance improvement workshops. In her community, she loves diving into the stories of high school students and helping them sift through the noise to 'pitch' their whole
                                                selves to colleges, jobs, and the ever-nosy aunties & uncles. She currently works for Accenture as a Management Consultant within the Risk & Financial Crime space, manages the corporate
                                                citizenship relationship with Dress for Success, and is a facilitator for design thinking/ human-centered design.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<script>
    $(".speakers-more-details").click(function () {
        $(this).parent().parent().parent().find(".speakers-content").toggleClass("speakers-details-height");

        if ($(this).text() == "Show More +") {
            $(this).text("Show Less - ");
        } else {
            $(this).text("Show More +");
        }
    });
</script>

<?php include 'footer.php';?>
        
