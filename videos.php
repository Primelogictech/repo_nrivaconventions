<?php include 'header.php';?>
    
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/fLAs8dNQRZ8" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image1.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Logo Launch</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/gGrfODuTcQc" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image2.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Invitation from President and Chairman</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/DtoQ1sW9JY4" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image3.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Promo</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/fLAs8dNQRZ8" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image1.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Logo Launch</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/gGrfODuTcQc" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image2.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Invitation from President and Chairman</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/DtoQ1sW9JY4" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image3.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Promo</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/fLAs8dNQRZ8" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image1.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Logo Launch</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/gGrfODuTcQc" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image2.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Invitation from President and Chairman</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/DtoQ1sW9JY4" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image3.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Promo</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>