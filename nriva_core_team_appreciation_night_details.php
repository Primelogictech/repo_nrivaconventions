    <?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/nriva-core-team-appreciation-night.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <p>
                                    Thank you all for your hard work and support to our beloved NRIVA. What we are today is because of you all and your selfless service to the organization. An organization that started with ten people now
                                    is an organization of more than 10000 members. With the introduction of Vision 2020 committees, we have achieved and benefited quite a bit with series of webinars, suggestions and exchanging ideas on
                                    financial, real-estate and business groups and helping and supporting one another as needed.
                                </p>
                                <p>
                                    This event includes Awards & Recolonization for EC, Board, Chapter Leads, Standing Committee Members, Cultural Programs by Talented Vasavites followed by a Special Dinner and DJ.
                                </p>
                                <p><strong>Date & Time :</strong> Jul 4th 6 PM Onward</p>
                            </div>
                            <div class="col-12">
                                <h5 class="text-violet">Address</h5>
                                <p>
                                    Room - CRYSTAL / SAPPHIRE / RUBY,<br />
                                    Suburban Collection Show Place,<br />
                                    46100 Grand River Ave, Novi, MI 48374, USA.
                                </p>
                            </div>
                            <div class="col-12">
                                <h5 class="text-violet">Contact Details:</h5>
                                <div class="text-danger font-weight-bold mb-1">SREEDHAR AIETHA</div>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">(248)-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i>
                                    <a href="#" class="icon-positions text-black text-decoration-none fs16 lh16 mail">srexxxxxxxxxx@nriva.org</a>
                                </div>
                            </div>
                            <div class="col-12 pt20">
                                <div class="text-danger font-weight-bold mb-1">SANTOSH VELLORE</div>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">(217)-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i>
                                    <a href="#" class="icon-positions text-black text-decoration-none fs16 lh16 mail">sanxxxxxxxxxx@gmail.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
