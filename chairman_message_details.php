<?php include 'header.php';?>
	
<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small px-sm-30 py-4 p-md-4 p40">
            	<div class="text-center float-sm-none float-md-right px-2">
            		<img src="images/jayasimha_sunku.jpg" class="" width="160" alt="Nagender_Aytha" />
            		<div class="py-2">
            			<div class="text-danger fs16 text-center">Mr. Jayasimha Sunku</div>
            			<div class="text-danger fs12 text-center">- Chairman</div>	
            		</div>
            	</div>
                <h4 class="mb-3 text-violet">Dear Members,</h5>
                <p>
					I am so grateful to serve as the Chairman of the Board of Directors of the NRI Vasavi Association (“NRIVA”) for 2018-19. As you know, we celebrate our great heritage from the Vasavi Matha that originated in Penugonda with clear guiding principles on how to lead our lives with Duty, Integrity and Non-Violence.
				</p>
				<p>
					Now in our 12th year, the NRIVA maintains its very powerful vision “Connecting to Serve.” While the chapters help build relationships among our local communities, the interactions nationally are limited without our national arm.
				</p>
				<p>
					I would like to humbly welcome each of the members to our 5 th Global Convention in Novi, Michigan this year. We are slated to host between 3000 to 5000 Vasavaites globally for this remarkable convention, representing 100+ cities around the United States and globally.
				</p>
				<p>
					NRIVA was founded in 2007 by Anand Garlapati and Vijay Chavva, with a simple but very powerful vision to “Connecting to Serve”. Many of us were connected with each other in our own local communities through the annual Vasavi Pooja but with very limited set of interactions. Since the early days of immigration we estimate that there are about 10,000 Vasavite families live in USA representing approximately 1% of NRIs.
				</p>
				<p>
					While our heritage is from the business community, we have a much more diversified set of professionals now who have excelled in Academics, Arts, Business, Entrepreneurship, Films, Infrastructure, Literature, Medicine, Music, Politics, Science, Spirituality, Sports and many more fields. It is our privilege to not only serve as a platform to connect all of our members for the convention, but to showcase their very special talents and accomplishments at the Convention.
				</p>
				<p>
					While the NRIVA has humble beginnings, we strive to pay it forward to the underprivileged while annually funding programs for the needy.
				</p>
				<p>
					I extend my appreciation to all the Detroit local chapter, President, Conveners, Board of Directors, Executive Committee and all of the hundreds of volunteers who have worked tirelessly to put this convention to fruition. I encourage you to invite additional members to our NRIVA events so we can continue to grow our vision and plans for the future generation. We will continue to strive to build for the future by fulfilling original mission for “Connecting to Serve.” Once again, thank you all and welcome!
				</p>
				<div>
					<div class="text-violet font-weight-bold fs16">Yours truly,</div>
					<div class="text-black fs16">Mr. Jayasimha Sunku</div>
					<div class="text-orange fs16">Chairman, NRIVA</div>
					<div class="text-orange fs16">Board Of Directors Of The NRIVA</div>
					<div class="fs16 py-3">Visit <a href="#" class="text-orange">convention.nriva.org</a> for updates and further details</div>
				</div>
				<div class="text-violet fs15 font-weight-bold">
					Why wait? Go “Destination Detroit!”
				</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>