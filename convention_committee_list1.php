<?php include 'header.php';?>

<style type="text/css">
   
    .carosel {
        bottom: -30px;
        margin-left: 42%;
        margin-right: 42%;
    }
    .carosel_slider-btn {
        position: absolute;
    }
    .carousel-control-prev {
        left: auto;
        right: 60px !important;
    }
    .carousel-control-next {
        right: 0px;
    }
    .carousel-control-next,
    .carousel-control-prev {
        position: absolute;
        top: 100%;
        width: auto;
        opacity: 1;
    }
    .carosel-inner:before {
        content: "";
        position: absolute;
        width: 77%;
        height: 100%;
        border-bottom: 2px solid #e9ebf0;
        left: 130px;
        right: 130px;
    }
    @media (max-width: 576px) {
        .carosel {
            background: transparent !important;
            bottom: 5x !important;
        }
    }
</style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/convention-committees.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="col-12 px-0">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="5000">
                                <ol class="carousel-indicators carosel">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                </ol>
                                <div class="carousel-inner carosel-inner">
                                    <div class="carousel-item active">
                                        <div class="col-12">
                                            <div class="row pb-5">
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-1 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/av.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">AV</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-2 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/awards.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">Awards</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-3 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/banquet.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">Banquet</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-4 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/bi_group.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Business Interest Group
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-5 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/cme.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    CME
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-12 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/Cultural.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Cultural
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-6 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/decoration.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Decoration
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-7 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/exhibit_booth.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Exhibit booths
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-8 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/event_registration.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Event Registration
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-9 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/finance.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Finance/Accounts
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-10 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/food.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Food
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-11 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/fund_raising.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Fund Raising
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-13 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/hospitality.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Hospitality
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-14 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/accomodations.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Hotels & Accomodations
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-15 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/immigration.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Immigration
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-16 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/information_desk.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Information Desk
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="col-12">
                                            <div class="row pb-5">
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-1 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/photography.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Photography and Video
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-2 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/matrimony.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Matrimony
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-3 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/real_estate.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Real Estate
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-4 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/security.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Security and Safety
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-5 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/shathamanam_bhavathi.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Shathamanam Bhavathi
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-6 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/souvenir.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Souvenir
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-7 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/social_media.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Social Media
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-8 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/spiritual.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Spiritual
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-9 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/investments.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Stocks and Investments
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-10 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/transportation.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Travel and Transportation
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-11 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/vgt.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Vasavites Got Talent (VGT)
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-12 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/venue.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Venue & Logistics
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-13 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/women.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Women
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-14 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/flyers.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Web & Flyers
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                                    <div class="gc-15 border-radius-10">
                                                        <div class="convention-cc-bg">
                                                            <div class="p-3">
                                                                <div>
                                                                    <img src="images/cc-icons/youth.png" class="img-fluid mx-auto d-block" alt="" />
                                                                </div>
                                                                <h6 class="text-white text-center pt-2">
                                                                    Youth
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carosel_slider-btn">
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carosel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carosel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
