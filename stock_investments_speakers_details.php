<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/stock-investments-speakers.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Rajendra Prasad
                                        <span class="text-yellow fs16 pl10">- MD, FRCPC</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/raja.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <h5 class="mb-3">
                                                <span class="text-danger">Topic :</span>
                                                <span class="text-violet">Wallstreet Strategies</span>
                                            </h5>
                                            <p>
                                                Rajendra Prasad, MD, FRCPC’s family name is Grandhe. His family migrated from Andhra 1000 years ago to Udumalpet, near Coimbatore.
                                            </p>
                                            <p>
                                                In 1992 he passed series 7, 63 and 65 and registered with SEC as a registered investment advisor. In 1990s he published a newsletter called “The Mutual Funds Leader”. It was a market timing
                                                newsletter. He was one of the top timers in Timer Digest, a publication from Connecticut in the 90s for many years. In the late 1998 he started a mutual fund called “Prasad Growth Fund” which
                                                was one of the best funds in the USA on and off for several years. Starting in 2009 it did not do well and he sold the fund in 2014. He was a regular guest on TV, KWHY Business Channel in Los
                                                Angeles to talk about stocks and mutual funds. He was quoted many times in the Wall Street Journal and Investors Business Daily. He has published two e-books under the pen name of “Mr.
                                                Market”. The first one was published through Booktango.com, called “Wall Street Dos and Don’ts/ Formulas to Make Money”. The second e-book was for people of India. It was “Become a Crorepati/
                                                Invest in Indian Mutual Funds”.
                                            </p>
                                            <p>
                                                God;s work: He has donated his house in India; built three class rooms and an auditorium in Sri Kannika Parameswari School in Udumalpet, Tamilanadu (his native place). His grandfather had
                                                started this school 108 years ago and now it has 3,500 students. He is a donor in crematorium, blood bank, Sri SKP wedding chapel; He pays for 75 cataract extractions a year to poor people. In
                                                the USA he is a founding member of Sri Venkateswara Temple in Calabasas, CA. In 1991 he started “USA Match for Life”, an organization to recruit donors for bone marrow/stem cell donation which
                                                later merged with Asians for Miracle Marrow Matches in Los Angeles, CA.
                                            </p>
                                            <p>
                                                He is an ex-associate professor of oncology, hematology and internal medicine and spent 20 years teaching in University of California, Irvine on a voluntary basis. He is triple board certified
                                                and a Fellow of the Royal College of Physicians of Canada. In 2007 the Congress of the United States House of Representatives gave him and his wife a certificate of Special Congressional
                                                Recognition for outstanding and dedicated services to the community.
                                            </p>
                                            <p>
                                                Please visit GRANDHE.ORG for all the information which will be presented in NRIVA meet. Also you can download his book, “Wall Street Dos and Don’ts/ Formulas to Make Money” free of charge from
                                                Grandhe.org website.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Jay Krishna Devaki
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/jaya-krishna-devaki1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <h5 class="mb-3">
                                                <span class="text-danger">Topic :</span>
                                                <span class="text-violet">Trading and Investment Insights</span>
                                            </h5>
                                            <p>
                                                A resident of NRIVA Tampa and a IIT Madras grad, has 18 years of experience in Stocks, Options, Futures, Commodities, and Forex trading. A certified Investment advisor and Fund manager, worked
                                                for Barclays earlier in their investment division.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Vamshi Yelakanti
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/vamshi-yelakanti.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <h5 class="mb-3">
                                                <span class="text-danger">Topic :</span>
                                                <span class="text-violet">Hedge Funds - Behind the Insights the way algos work</span>
                                            </h5>
                                            <p>
                                                A NRIVA DC resident, with 20+ years of experience as investor, trader, and advisor in stock market investments. During his collage days in India, he helped his family business and parents at
                                                Hyderabad stock exchange and National Stock exchange (NSE). He underwent training at Bombay stock exchange and was a professional trader and market maker for more than 3 years before moving to
                                                USA. Currently he advises multiple investor groups on a daily basis with main goal of not to loose money in stock market.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<script>
    $(".speakers-more-details").click(function () {
        $(this).parent().parent().parent().find(".speakers-content").toggleClass("speakers-details-height");

        if ($(this).text() == "Show More +") {
            $(this).text("Show Less - ");
        } else {
            $(this).text("Show More +");
        }
    });
</script>

<?php include 'footer.php';?>
