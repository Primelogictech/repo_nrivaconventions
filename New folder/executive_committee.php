<?php include 'header.php';?>
	
<section class="container-fluid my-5">
    <div class="container shadow-small">
        <div class="row">
            <div class="col-12 px-0">
                <div class="leadership-heading-bg p-3">
                    <h4 class="mb-0">Executive Committee</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 p-4 p-md-4 p40">
            	<div class="row">
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harionweb@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">847-942-9562</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">pandiri@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">314-239-1104</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellendular@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">248-894-7284</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harionweb@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">847-942-9562</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">pandiri@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">314-239-1104</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellendular@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">248-894-7284</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harionweb@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">847-942-9562</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">pandiri@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">314-239-1104</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellendular@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">248-894-7284</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harionweb@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">847-942-9562</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">pandiri@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">314-239-1104</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellendular@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">248-894-7284</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harionweb@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">847-942-9562</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">pandiri@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">314-239-1104</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-2 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellendular@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 font-weight-bold">248-894-7284</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>