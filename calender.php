    <?php include 'header.php';?>

    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.css'>
    <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/cupertino/jquery-ui.css'><link rel="stylesheet" href="css/style.css">

    <style type="text/css">
        body{
            font-family: 'Kanit', sans-serif;
        }
        span.fc-title {
            white-space: normal;
        }
        .fc-view-container{
            overflow-x: scroll;
        }
        /*table tr td, table tr th, td.fc-day-top.fc-tue.fc-future{
            width: 150px !important;
        }*/
    </style>

    <section class="container-fluid py-5">
        <div class="row">
            <div class="col-12">
                <div class="main-heading">
                    <div>
                        Convention Events
                    </div>
                </div>
            </div>
        </div>
    </section>
        
    <section class="container-fluid mt-2 mt-lg-4 mb-5">
        <div class="shadow-small px-sm-20 py-4 px-md-3 py-md-4 p20">
            <div class="row px-2 px-md-3">
                <div class="col-12 col-md-12 col-lg-4 shadow-xs py-3">
                    <div class="row">
                        <div class="col-12 my-auto">
                            <div class="fs20 font-weight-bold">Events Schedule</div>
                        </div>
                        <!-- <div class="col-6 my-auto">
                            <div class=" text-right">
                                <button class="btn btn-info"><i class="fas fa-plus fs14 pr-2"></i> New</button>
                            </div>
                        </div>   -->
                    </div>
                    <div class="col-12 shadow-xs mt-3 calender-events-schedule">
                        <div class="row">
                            <div class="col-12 px-0 border-bottom bg-light">
                                <div class="px-2 pt30 pb30">
                                    <div class="border-left-2-violet  pl-2">
                                       <div>
                                            <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                       </div>
                                       <div>Sri Laxmi Narasimha Swamy Kalyanam at 7:15pm. The Lord’s Kalyanam is performed for ‘loka ksheman’ and ‘loka kalyanam’.</div>
                                       <div class="my-2">
                                           <!-- <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp;Tim</a>
                                           </span> -->
                                           <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Thu Jul 4th, 10am</a>
                                           </span>
                                       </div>
                                    </div>   
                                </div>
                            </div>
                            <div class="col-12 px-0 border-bottom bg-light">
                                <div class="px-2 pt30 pb30">
                                    <div class="border-left-2-violet  pl-2">
                                       <div>
                                            <span class="text-violet font-weight-bold fs16">Breakfast and Matrimony</span>
                                       </div>
                                       <div>Breakfast at 7:15am...</div>
                                       <div class="my-2">
                                           <!-- <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp;Tim</a>
                                           </span> -->
                                           <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                           </span>
                                       </div>
                                    </div>   
                                </div>
                            </div>
                            <div class="col-12 px-0 border-bottom bg-white">
                                <div class="px-2 pt30 pb30">
                                    <div class="pl-2">
                                       <div>
                                            <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                       </div>
                                       <div>Sri Laxmi Narasimha Swamy Kalyanam at 7:15pm. The Lord’s Kalyanam is performed for ‘loka ksheman’ and ‘loka kalyanam’.</div>
                                       <div class="my-2">
                                           <!-- <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp;Tim</a>
                                           </span> -->
                                           <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                           </span>
                                       </div>
                                    </div>   
                                </div>
                            </div>
                            <div class="col-12 px-0 border-bottom bg-white">
                                <div class="px-2 pt30 pb30">
                                    <div class="pl-2">
                                       <div>
                                            <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                       </div>
                                       <div>Sri Laxmi Narasimha Swamy Kalyanam at 7:15pm. The Lord’s Kalyanam is performed for ‘loka ksheman’ and ‘loka kalyanam’.</div>
                                       <div class="my-2">
                                           <!-- <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp;Tim</a>
                                           </span> -->
                                           <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                           </span>
                                       </div>
                                    </div>   
                                </div>
                            </div>
                            <div class="col-12 px-0 border-bottom bg-white">
                                <div class="px-2 pt30 pb30">
                                    <div class="pl-2">
                                       <div>
                                            <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                       </div>
                                       <div>Sri Laxmi Narasimha Swamy Kalyanam  at 7:15pm. The Lord’s Kalyanam is performed for ‘loka ksheman’ and ‘loka kalyanam’.</div>
                                       <div class="my-2">
                                           <!-- <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp;Tim</a>
                                           </span> -->
                                           <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                           </span>
                                       </div>
                                    </div>   
                                </div>
                            </div>
                            <div class="col-12 px-0 border-bottom bg-white">
                                <div class="px-2 pt30 pb30">
                                    <div class="pl-2">
                                       <div>
                                            <span class="text-violet font-weight-bold fs16">Sri Laxmi Narasimha Swamy Kalyanam..</span>
                                       </div> 
                                       <div>Sri Laxmi Narasimha Swamy Kalyanam at 7:15pm. The Lord’s Kalyanam is performed for ‘loka ksheman’ and ‘loka kalyanam’.</div>
                                       <div class="my-2">
                                           <!-- <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp;Tim</a>
                                           </span> -->
                                           <span class="bg-white border fs13 px-2 py3 border-radius-3 mr-2">
                                               <a href="#" class="text-dark text-decoration-none"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; Wed Aug 04, 10am</a>
                                           </span>
                                       </div>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-8 px-0 pl-md-3 pr-md-0">
                    <div id='calendar' class="shadow-xs p-3 mt-0"></div>
                </div>
            </div>
        </div>
    </section>

    <script src='https://cdn.jsdelivr.net/npm/moment@2.24.0/min/moment.min.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.min.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.js'></script>
    <script  src="js/script.js"></script>

    <?php include 'footer.php';?>