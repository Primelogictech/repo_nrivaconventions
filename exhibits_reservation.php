<?php include 'header.php';?>

<style type="text/css">
    th {
        border-color: #dadada !important;
    }
</style>

<section class="container-fluid my-3">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/exhibits-registration.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-md-5 col-lg-4">
                                <h5 class="text-violet">Contact For More Details</h5>
                                <h6 class="">Jayaprakash Madhamshetty</h6>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">(313)-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">exhxxxxxxxxxx@nriva.org</a></div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-8 my-2 my-md-0">
                                <div class="text-md-right">
                                    <a href="#exhibit-registration" class="btn btn-danger btn-lg text-uppercase mr-2 my-1 fs-xs-15">Exhibit Registration</a>
                                    <a href="documents/2019-Detroit-Convention-Exhibit_Booths_Package_RE.pdf" target="_blank" class="btn btn-danger btn-lg text-uppercase my-1 fs-xs-15">Exhibits Package Details</a>
                                </div>
                            </div>
                            <div class="col-12 mt-3">
                                <h5 class="text-orange">Timings</h5>
                                <h6 class="text-skyblue">Booths Setup</h6>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>Thursday July 4th, 2019 – 9:00 A.M. to 5:00 P.M EST</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h6 class="text-skyblue">Hours</h6>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>Friday - July 5th, 2019 - 10:00 A.M. to 11:00 P.M. EST</li>
                                    <li>Saturday - July 6th, 2019 - 10:00 A.M. to 11:00 P.M. EST</li>
                                    <li>Sunday - July 7th, 2019 - 10:00 A.M. to 02:00 P.M. EST</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h6 class="text-skyblue">Tear Down</h6>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>Sunday – July 7th, 2019 - 04:00 p.m. to 06:00 p.m. EST</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h6 class="text-skyblue">Questions?</h6>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>
                                        Call JP Madhamsetty at +1 (313)-XXX-XXXX (or) email:
                                        <a class="text-black text-decoration-none" href="#"> exhxxxxxxxxxx@nriva.org</a>
                                    </li>
                                    <li>
                                        Call Suresh Cholaveti at +1 (614)-XXX-XXXX (or) email:
                                        <a class="text-black text-decoration-none" href="#"> exhxxxxxxxxxx@nriva.org</a>
                                    </li>
                                    <li>
                                        <a class="text-black text-decoration-none" href="#">Call Srinivas Chittaluri at +1 (848)-XXX-XXXX (or) email:</a>
                                        <a class="text-black text-decoration-none" href="#"> exhxxxxxxxxxx@nriva.org</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h6 class="text-skyblue">What is Included</h6>
                                <p class="mb-0 fs16">
                                    All booths are of 10' x 10' size and include the following. For a bigger booth of 20' x 10', you will get two sets of these items.
                                </p>
                                <ul class="list-unstyled exhibits-list pl20 pt10 fs16">
                                    <li>8’ High Back Wall.</li>
                                    <li>3’ High Side Drape.</li>
                                    <li>7" x 44" One-line ID Sign.</li>
                                    <li>6’ Long Skirted Table - 1 No.</li>
                                    <li>Side Chairs - 2 Nos.</li>
                                    <li>Waste Basket/Trash Can - 1.</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h5 class="text-violet">Optional Accessories</h5>
                                <p class="mb-0 fs16">
                                    Optional Accessories/Items are available for additional pricing. Please contact us for pricing.
                                </p>
                            </div>
                            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 my-3">
                                <h5 class="text-violet text-center">Exhibit Booths Pricing</h5>
                                <div class="card-body px-0">
                                    <div class="table-responsive">
                                        <table class="datatable table-bordered table table-hover table-center mb-0">
                                            <tr>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center">S.No</th>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center"><strong>Exhibitor Type</strong></th>
                                                <th align="center" rowspan="2" class="bg-violet text-white text-center"><strong>Size</strong></th>
                                                <th align="center" class="bg-violet text-white text-center"><strong>Before</strong></th>
                                                <th align="center" class="bg-violet text-white text-center"><strong>After</strong></th>
                                            </tr>
                                            <tr>
                                                <th align="center" class="bg-violet text-white text-center">
                                                    <strong>4/19/2019</strong>
                                                </th>
                                                <th align="center" class="bg-violet text-white text-center">
                                                    <strong>4/19/2019</strong>
                                                </th>
                                            </tr>
                                            <tbody>
                                                <tr>
                                                    <td align="center" rowspan="2">1</td>
                                                    <td align="left" rowspan="2" class="bg-gold text-white">
                                                        <strong>Jewelry</strong>
                                                    </td>
                                                    <td align="center">10’ x 20’</td>
                                                    <td align="center">$7,500.00</td>
                                                    <td align="center">$8,000.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">10’ x 10’</td>
                                                    <td align="center">$4,000.00</td>
                                                    <td align="center">$5,000.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" rowspan="2">2</td>
                                                    <td align="left" rowspan="2" class="bg-gold text-white">
                                                        <strong>Real Estate/Corporate</strong>
                                                    </td>
                                                    <td align="center">10’ x 20’</td>
                                                    <td align="center">$4,500.00</td>
                                                    <td align="center">$5,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">10’ x 10’</td>
                                                    <td align="center">$3,000.00</td>
                                                    <td align="center">$4,000.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" rowspan="2">3</td>
                                                    <td align="left" rowspan="2" class="bg-gold text-white">
                                                        <strong>
                                                            Pearls/Gems/<br />
                                                            Artificial Jewelry
                                                        </strong>
                                                    </td>
                                                    <td align="center">10’ x 20’</td>
                                                    <td align="center">$3,500.00</td>
                                                    <td align="center">$4,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">10’ x 10’</td>
                                                    <td align="center">$2,000.00</td>
                                                    <td align="center">$3,000.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" rowspan="2">4</td>
                                                    <td align="left" rowspan="2" class="bg-gold text-white">
                                                        <strong>
                                                            Apparel/Clothing<br />
                                                            Boutiques
                                                        </strong>
                                                    </td>
                                                    <td align="center">10’ x 20’</td>
                                                    <td align="center">$2,500.00</td>
                                                    <td align="center">$3,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">10’ x 10’</td>
                                                    <td align="center">$1,500.00</td>
                                                    <td align="center">$2,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" rowspan="2">5</td>
                                                    <td align="left" rowspan="2" class="bg-gold text-white">
                                                        <strong>Insurance/Telecom</strong>
                                                    </td>
                                                    <td align="center">10’ x 20’</td>
                                                    <td align="center">$4,500.00</td>
                                                    <td align="center">$5,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">10’ x 10’</td>
                                                    <td align="center">$2,500.00</td>
                                                    <td align="center">$3,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" rowspan="2">6</td>
                                                    <td align="left" rowspan="2" class="bg-gold text-white">
                                                        <strong>
                                                            Financial/Web<br />
                                                            Education/Professional
                                                        </strong>
                                                    </td>
                                                    <td align="center">10’ x 20’</td>
                                                    <td align="center">$3,500.00</td>
                                                    <td align="center">$4,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">10’ x 10’</td>
                                                    <td align="center">$2,500.00</td>
                                                    <td align="center">$3,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" rowspan="2">7</td>
                                                    <td align="left" rowspan="2" class="bg-gold text-white">
                                                        <strong>Arts/Crafts</strong>
                                                    </td>
                                                    <td align="center">10’ x 20’</td>
                                                    <td align="center">$2,500.00</td>
                                                    <td align="center">$3,000.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        10’ x 10’
                                                    </td>
                                                    <td align="center">
                                                        $1,500.00
                                                    </td>
                                                    <td align="center">
                                                        $2,000.00
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" rowspan="2">8</td>
                                                    <td align="left" rowspan="2" class="bg-gold text-white">
                                                        <strong>Booth – General</strong>
                                                    </td>
                                                    <td align="center">10’ x 20’</td>
                                                    <td align="center">$2,500.00</td>
                                                    <td align="center">$3,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">10’ x 10’</td>
                                                    <td align="center">$1,500.00</td>
                                                    <td align="center">$2,500.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" rowspan="2">9</td>
                                                    <td align="left" class="bg-gold text-white">
                                                        <strong>Home Based Businesses</strong>
                                                    </td>
                                                    <td align="center">10’ x 10’</td>
                                                    <td align="center">$1,500.00</td>
                                                    <td align="center">$2,000.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="bg-gold text-white" width="30%">
                                                        <strong>Non-Profit Organizations</strong>
                                                    </td>
                                                    <td align="center">10’ x 10’</td>
                                                    <td align="center">$1,500.00</td>
                                                    <td align="center">$2,000.00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <h6 class="text-danger">Note:</h6>
                                <div class="fs16">Premium (outside) booths are extra. Please contact for pricing</div>
                                <div class="fs16">Cancellation Policy: <strong>50% Refund before June 01, 2019</strong> &amp; Nonrefundable thereafter.</div>
                            </div>
                            <div class="col-12 mt-5" id="exhibit-registration">
                                <h4 class="text-violet text-center mb-3">Exhibits Registration</h4>
                                <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                                <h6 class="font-weight-bold text-center mb-3">Please make sure that you have registered for the Convention before you register for this Event.</h6>

                                <div class="row px-3 px-lg-0">
                                    <div class="col-12 col-md-12 col-lg-10 offset-lg-1 shadow-small p-3 py-md-5 px-md-4 my-3">
                                        <form>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Full Name:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Full Name" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Company Name:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Company Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Category:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Category" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Tax ID:</label>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Tax ID" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Mailing Address:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Mailing Address" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Phone No:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="number" name="" id="" class="form-control" placeholder="Phone No" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Email Id:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Email Id" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Fax: </label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Fax" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Name of your Website:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Name of your Website" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-md-6 col-lg-6">
                                                    <label>Booth Type:</label><span class="text-red"> *</span>
                                                    <div>
                                                        <input type="text" name="" id="" class="form-control" placeholder="Booth Type" />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-6 my-1">
                                                <div class="border-green-2 border-radius-5 bg-white">
                                                    <h5 class="text-violet py-3 text-center">Payment by using PayPal</h5>
                                                    <img src="images/paypal.jpg" class="img-fluid mx-auto d-block" alt="" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-6 my-1">
                                                <div class="border-green-2 border-radius-5 bg-white py55">
                                                    <h5 class="text-center">Enter your Exhibit Amount</h5>
                                                    <div class="mx-auto" style="max-width: 250px;">
                                                        <div class="border border-radius-3 p10 mt10">
                                                            <div class="row">
                                                                <div class="col-2 p10 my-auto">
                                                                    <div class="border-right">
                                                                        <h4 class="text-orange text-center mb-0">$</h4>
                                                                    </div>
                                                                </div>
                                                                <div class="col-7 my-auto px-0">
                                                                    <input type="text" class="p-0 pl10 pr10 text-right box-shadow-none form-control donatein border-0" name="" />
                                                                </div>
                                                                <div class="col-3 my-auto px-0">
                                                                    <div class="text-orange text-left fs28">.00</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-8 col-lg-7 pt-2">
                                                <h5>Security Code</h5>
                                                <div class="row">
                                                    <div class="col-11">
                                                        <div class="registration-captcha-block pr-3">
                                                            <div class="row">
                                                                <div class="col-8 px-0 my-auto">
                                                                    <input type="text" class="border-0 form-control bg-transparent" name="" placeholder="Enter the Characters you see" />
                                                                </div>
                                                                <div class="col-4 px-0 my-auto pr5">
                                                                    <img src="images/ShowCaptchaImage.png" class="captcha-img float-md-right" alt="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1 px-1 my-auto">
                                                        <div>
                                                            <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-5 mt40">
                                                <div class="text-center text-sm-right">
                                                    <div class="btn btn-lg btn-danger text-uppercase px-3 px-lg-5 fs-md-18">PROCEED TO PAY</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 mt-3">
                                <div>
                                    <img src="images/Exhibits-Flyer.jpeg" class="img-fluid mx-auto d-block" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
