<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/venue.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-lg-12">
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-9 mb-2 mb-lg-0 px-0">
                                        <div>
                                            <img src="images/venue.png" class="img-fluid w-100 img-b p7" alt="Venue" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-12 col-lg-3 bg-violet py-3">
                                        <div class="row pt10">
                                            <div class="col-12 col-md-6 col-lg-12">
                                                <a href="hotels_details.php">
                                                    <div class="venue-btns">
                                                        <img src="images/hotel1.png" class="img-fluid mr-2" width="30" alt="" />
                                                        <span class="text-white fs16">Hotels</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-12">
                                                <a href="#">
                                                    <div class="venue-btns">
                                                        <img src="images/parking1.png" class="img-fluid mr-2" width="30" alt="" />
                                                        <span class="text-white fs16">Parking</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-12">
                                                <a href="#">
                                                    <div class="venue-btns">
                                                        <img src="images/floor-plan1.png" class="img-fluid mr-2" width="30" alt="" />
                                                        <span class="text-white fs16">Transportation</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-12">
                                                <a href="#">
                                                    <div class="venue-btns">
                                                        <img src="images/directions1.png" class="img-fluid mr-2" width="30" alt="" />
                                                        <span class="text-white fs16">Hotels</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 pt-4">
                                <h4 class="text-violet">About Us</h4>
                                <h5 class="text-orange">Suburban Collection Showplace</h5>
                                <h5 class="text-skyblue">Background</h5>
                                <p>
                                    Suburban Collection Showplace is the largest, privately-owned exposition, conference and banquet center in Michigan. The brand new, state-of-the-art facility sits on 55 acres of land directly off of the
                                    I-96 expressway in Novi, Michigan. Since construction was completed on the 320,000 square foot facility in August 2005, Suburban Collection Showplace has been host to a variety of events including: public
                                    expositions, trade shows, association and corporate conferences, and social events.
                                </p>
                                <h5 class="text-skyblue">What's Special</h5>
                                <p>
                                    Suburban Collection Showplace is Southeastern Michigan’s premier exposition, conference, and banquet center. Each year 1.5 million people enter through a grand atrium style entrance to over 25,000 square
                                    feet of pre-function, lobby space. The exposition space has a total of 214,000 square feet of multipurpose exhibit floor. The space is dividable into three exposition halls, and features 5 additional
                                    meeting rooms. The facility was carefully designed to meet all event needs. There are a total of only 45 columns throughout the entire exposition space. Each hall boasts a 28' ceiling with 24' true clear
                                    throughout, dedicated loading docks, 480 120/280 three phase power, and both wired and wireless internet connectivity.
                                </p>
                                <h5 class="text-skyblue">Extra Special</h5>
                                <p>
                                    The elegantly appointed Diamond Banquet and Conference Center welcomes all guests through a 25,000 square foot lobby. The 20,000 square foot Diamond Ballroom can accommodate up to 1,300 people at a
                                    banquet style setting. In addition, the entire column-free space is dividable into 5 rooms to accommodate smaller events. The Diamond Ballroom ceiling is a soaring 18 feet high and has been structurally
                                    designed for hanging light trusses and audio-visual equipment platforms.
                                </p>
                                <h5 class="text-skyblue">Details</h5>
                                <p>
                                    The Suburban Collection Showplace is owned by TBON, LLC., a privately owned corporation located in Novi, Michigan. In December 2010 Suburban Collection agreed to become the facility’s naming rights
                                    sponsor. Suburban Collection Showplace is located at 46100 Grand River Avenue in Novi, Michigan.
                                </p>
                            </div>

                            <div class="col-12">
                                <div class="img-thumbnail pb-0">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d47073.81420462556!2d-83.502688!3d42.489142!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8824af29586cce13%3A0xa3b4c3c8d7225925!2s46100%20Grand%20River%20Ave%2C%20Novi%2C%20MI%2048374%2C%20USA!5e0!3m2!1sen!2sin!4v1628860174784!5m2!1sen!2sin"
                                        width="100%"
                                        height="350"
                                        class=""
                                        style="border: 0;"
                                        allowfullscreen=""
                                        loading="lazy"
                                    ></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
