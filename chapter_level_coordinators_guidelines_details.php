<?php include 'header.php';?>
    
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 py-3">
                <h4 class="text-violet mb-4">Chapter Level Coordinators Guidelines</h4>
                <div class="pl30">
                    <p>
                        <strong>1.</strong> Chapter Level Miss and Mrs. NRIVA coordinator must decide on a date, time and place for the pageant and secure all the necessary reservations and permits. Although largely unseen and unnoticed, other aspects, such as audio, video and music, must also be lined up and scripted by the coordinator. Additionally, the coordinator is in charge of hiring an emcee and putting together a script for the event.
                    </p>
                    <p>
                        <strong>2.</strong> They are required to get all the competition related information from the national coordinators and work hand-in-hand all the time.<b>( Coordinators will send the questions half hour before the competitions)</b>
                    </p>
                    <p>
                        <strong>3.</strong><b> Chapter Level coordinators or/and organizers are not allowed to participate in the competition.</b>
                    </p>
                    <p>
                        <strong>4.</strong> They are responsible to find <b>two</b> neutral judges from their chapters for the Chapter Level competition who has Prior experience in judging.( They should be NON NRIVA)
                    </p>
                    <p>
                        <strong>5.</strong> They are required to send the judging sheets and results back to the national coordinators upon completion of the competition.
                    </p>
                    <p>
                        <strong>6.</strong> They are required to be impartial and not to favor or support any participants.
                    </p> 
                </div>
                <p>
                    We have read and here by agree to the all the above rules and competition guidelines.
                </p>       
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>