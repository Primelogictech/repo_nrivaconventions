<?php include 'header.php';?>
	
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small">
        <div class="row">
            <div class="col-12 px-0">
                <div class="leadership-heading-bg p-3">
                    <h4 class="mb-0">Board Of Directors</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 py-4 p-md-4 p40">
            	<div class="row">
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">panxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellxxxxxxxxxx@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">panxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellxxxxxxxxxx@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">panxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellxxxxxxxxxx@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">panxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellxxxxxxxxxx@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Hari Raini</h6>
		            				<div class="pt-1">President Elect</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">harxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Srinivasa Rao Pandiri</h6>
		            				<div class="pt-1">General Secretary</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">panxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Ravi Ellendula</h6>
		            				<div class="pt-1">Co-Convener</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ellxxxxxxxxxx@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>