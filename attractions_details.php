<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/attractions.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-center text-violet">Things to Cover in Around Detroit During your July-2019 Trip</h4>
                                <div>
                                    <h5 class="text-danger">In and Around Detroit</h5>
                                    <h6 class="text-skyblue mb15">Museums</h6>
                                    <ul class="list-unstyled fs16 attractions-details-list pl20">
                                        <li>Detroit Institute of Arts</li>
                                        <li>Charles H. Wright Museum of African American History</li>
                                        <li>Rouge Factory Tour(Ford F-150 Truck Assembly)</li>
                                        <li>Henry Ford Museum(Collection artifacts, Cars etc.)</li>
                                        <li>Motown Museum</li>
                                        <li>Detroit Historical Museum</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12">
                                <div>
                                    <h5 class="text-danger">City Riverfront Walk and Cover Detroit Downtown</h5>
                                    <h6 class="text-skyblue mb15">1 Day Trips:</h6>
                                    <ul class="list-unstyled fs16 attractions-details-list pl20">
                                        <li>1. Niagara Falls <b>(4 to 5 Hour Drive from Detroit)</b>, Lion Safari <b>(Canada VISA Required)</b></li>
                                        <li>2. Putin Bay (Ohio) <a class="text-danger" href="#">(Click Here)</a></li>
                                        <li>3. Mackinac island Michigan<a class="text-danger" href="#"> (Click Here)</a></li>
                                        <li>4. Cleveland</li>
                                        <li>5. Pittsburgh First Sri Venkateswara Temple in the western Hemisphere<a href="#" class="text-danger text-decoration-none"> (Click Here)</a></li>
                                    </ul>

                                    <h6 class="text-skyblue mb15">2 Day Trips:</h6>
                                    <ul class="list-unstyled fs16 attractions-details-list pl20">
                                        <li>Niagara Falls and Toronto (4 to 5 Hour Drive from Detroit), Lion Safari(Canada VISA Required)</li>
                                        <li>Chicago</li>
                                    </ul>

                                    <h6 class="text-skyblue mb15">3 Day Trips:</h6>
                                    <ul class="list-unstyled fs16 attractions-details-list pl20">
                                        <li>Pictured Rocks Michigan<a href="#" class="text-danger text-decoration-none"> (Click Here)</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
