<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/cultural-registration.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row px-3 px-lg-0">
                    <div class="col-12 p40 px-sm-30">
                        <h4 class="text-violet text-center mb-3">Cultural Registration</h4>
                        <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                        <h6 class="font-weight-bold text-center mb-3">Please make sure that you have registered for the Convention before you register for this Event.</h6>

                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-10 offset-lg-1 shadow-small p-3 py-md-5 px-md-4 mt-3">
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Chapter Name</label><span class="text-red"> *</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Chapter Name" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Program Name</label><span class="text-red"> *</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Program Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Type of Program</label>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Type of Program" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Number of Participants (Minimum 10)</label>
                                            <div>
                                                <input type="number" name="" id="" class="form-control" placeholder="Number of Participants" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Name of Choreographer</label>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Name of Choreographer" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Name of Contact Person</label><span class="text-red"> *</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Name of Contact Person" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Mobile number of Contact Person</label><span class="text-red"> *</span>
                                            <div>
                                                <input type="number" name="" id="" class="form-control" placeholder="Mobile number of Contact Person " />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Email of Contact Person </label>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Email of Contact Person" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Link of Practice session (teaser)</label>
                                            <div>
                                                <input type="number" name="" id="" class="form-control" placeholder="Link of Practice session (teaser) " />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="border_violet-4 px-3 pb30 pt30 my-3 border-radius-5">
                                            <h5 class="text-violet">Terms &amp; Conditions:</h5>
                                            <div class="py-1 position-relative">
                                                <input type="checkbox" class="" />
                                                <span class="pl15 fs16">
                                                    By clicking this check box, the named applicants and the team/business they representindicate that they have read and agreed to the terms and conditions of the competition.
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                                <div class="row">
                                    <div class="col-12 col-md-8 col-lg-7 pt-2">
                                        <h5>Security Code</h5>
                                        <div class="row">
                                            <div class="col-11">
                                                <div class="registration-captcha-block pr-3">
                                                    <div class="row">
                                                        <div class="col-8 px-0 my-auto">
                                                            <input type="text" class="border-0 form-control bg-transparent" name="" placeholder="Enter the Characters you see" />
                                                        </div>
                                                        <div class="col-4 px-0 my-auto pr5">
                                                            <img src="images/ShowCaptchaImage.png" class="captcha-img float-md-right" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1 px-1 my-auto">
                                                <div>
                                                    <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-5 mt40">
                                        <div class="text-center text-sm-right">
                                            <div class="btn btn-lg btn-danger text-uppercase px-5">Submit</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
