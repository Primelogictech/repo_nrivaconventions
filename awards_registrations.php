<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5 px-1-2 px-md-3">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/awards.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-md-5 col-lg-4">
                                <h5 class="text-violet">Contact For More Details</h5>
                                <h6 class="">UDAY SINGARAPU</h6>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">(248)-XXX-XXXX</span></div>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">awaxxxxxxxxxx@nriva.org</a></div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-8 my-2 my-md-0">
                                <div class="text-md-right">
                                    <a href="#awards-registration" class="btn btn-danger btn-lg text-uppercase mr-2 my-1">Awards Registration</a>
                                </div>
                            </div>
                            <div class="col-12 pt-3">
                                <h5 class="text-violet">Rules and Guidelines</h5>
                                <ul class="youth-activity-registration-list list-unstyled pl20 pr10 pt10 fs16 pt-0 pt-3">
                                    <li>There is no age, gender, language restriction for the nomination for most categories.</li>
                                    <li>All contestants need to be members of NRIVA. However, we encourage all the members to become Life members of NRIVA.</li>
                                    <li>Nominees who already received the NRIVA award in any category, during prior Conventions, will not be considered for nominating in the same awards category, with same credentials as prior.</li>
                                    <li>
                                        The Awards Committee Chair will submit the committee’s report to the Board for ratification. It is the NRIVA Board Executives’ responsibility to ensure that the Awards committee has followed the
                                        proper procedures.
                                    </li>
                                    <li>NRIVA Awards nominations must be received via the online submission process.. (through NRIVA website).</li>
                                    <li>The awards committee will review the submissions and the CVs or papers and prepare a short-list of candidates for each Award.</li>
                                    <li>All NRIVA members residing in USA are eligible to enter in this contest</li>
                                    <li>Deadline to submit for nominations should be submitted by 15th June, 2019 11:59 pm EST.</li>
                                </ul>
                            </div>
                            <div class="col-12 py-5" id="awards-registration">
                                <h4 class="text-center text-danger">Registrations Closed...</h4>
                            </div>
                            <div class="col-12 pt-3">
                                <div>
                                    <img src="images/awards-flyer.jpg" class="img-fluid mx-auto d-block" alt="Awards Flyer" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
