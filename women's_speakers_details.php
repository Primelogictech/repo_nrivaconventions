<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/women's-speakers.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Padma Kuppa
                                        <span class="text-yellow fs16 pl10">- State Representative</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/padma-kuppa1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <p class="Helvetica-Condensed dk-gray-t font14 l-h24 t-p20 tabhorizontal-t-p10 mobile-t-p0 tablet-r-p15">
                                                State Rep. Padma Kuppa is serving her first term representing Michigan’s 41st House District, which encompasses the cities of Troy and Clawson.
                                            </p>
                                            <p>
                                                Kuppa came to the U.S. from India at the age of four and grew up in America before moving to India as a teenager. After receiving her bachelors degree in mechanical engineering from the
                                                National Institute of Technology Warangal, Kuppa moved home to the U.S. for graduate school.
                                            </p>
                                            <p>
                                                Kuppa moved to the 41st district more than 20 years ago. Since then, she has worked as an engineer and project manager with a career that spans the automotive, financial, and IT industries.
                                                Throughout her life, Kuppa has sought to bring people together to solve problems both in the professional world and in the community.
                                            </p>
                                            <p>
                                                Kuppa has been a civic leader, volunteering with the PTA throughout her children’s K-12 education, founding the Troy-area Interfaith Group, created to bring people of all faiths together, and
                                                serving on Troy’s Planning Commission. She is currently President of the Troy Historic Society and a board member of the Michigan Roundtable for Diversity and Inclusion. As the first Indian
                                                immigrant and Hindu in the State Legislature, she hopes to bring a fresh perspective to the State House.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Anshu Varma
                                        <span class="text-yellow fs16 pl10">- Finance/management Professional</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/anshu-varma1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <p>
                                                Anshu Varma is a resident of East Lansing. She is a naturalized US citizen, originally from India, and has lived in Michigan since 1985. She is a finance/management professional and is a
                                                Certified Public Accountant (CPA).
                                            </p>
                                            <h5 class="text-violet mb-3">Community Engagement and Public Service:</h5>
                                            <p>
                                                She serves on the Board of the Ingham Community Health Centers as Finance Chair and is a member of its Executive Committee. This is an oversight board, routinely stretching the available
                                                federal/state dollars to meet client needs, increase access to care, and work on provider recruitment and retention.
                                            </p>
                                            <p>
                                                She also serves on the advisory council for the MSU museum andis actively involved with engaging middle school students with mission of the MSU Museum.
                                            </p>
                                            <p>
                                                Since 2003, she has worked with the Capital Area District Libraries to promote South Asian/Indian art formsas part of their Teen Programs.Specifically, she teaches Henna (Indian Asian
                                                temporary body art) at the local libraries, empowering teens through this creative art of self-expression. These events present a setting to talk about India – its culture, food, festivals and
                                                socio- economy issues. She is also engaged in several leadership activities and cultural event management for the Indian American Community in Michigan.
                                            </p>
                                            <p>
                                                Professionally, she has worked in budgeting and senior financial management roles for over 30 years with the State of Michigan.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Anu Prakash
                                        <span class="text-yellow fs16 pl10">- Anchor/Reporter</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/Anu-Prakash.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <p>
                                                Anu Prakash is an Emmy award winning journalist who is an anchor and reporter at WXYZ Channel 7.
                                            </p>
                                            <p>
                                                Since joining WXYZ, Anu Prakash has been reporting on the tough and touching stories of Metro Detroit. Anu feels it is a big responsibility and honor to share people’s stories with viewers.
                                                She is inspired by the courage and strength she has seen in the people she's interviewed over the years.
                                            </p>
                                            <p>
                                                As a first generation Indian American, Anu has always looked up to her parents who taught her the value of hard work and a kind heart. They have always been her biggest cheerleaders.
                                            </p>
                                            <p>
                                                Even as a kid, Anu watched the World News and developed an interest in what was going on in the world. While studying Business at Ohio State University, Anu explored her interest in
                                                broadcasting and reporting. While in school, she first worked as an intern at the ABC affiliate and got her big on-air break right out of college at that station. She's been in the industry
                                                ever since.
                                            </p>
                                            <p>
                                                When Anu is not working, you will find her spending time with friends and family. She loves photography and enjoys cooking .. especially trying to recreate her mom's delicious recipes! She
                                                loves to travel and watch basketball and college football, but we won’t say who she roots for.
                                            </p>
                                            <p>
                                                Born and raised in Northeast Ohio, Anu now resides in Oakland County and is actively involved in the surrounding community.
                                            </p>
                                            <p>
                                                If you have any stories or questions for Anu email her at aprakash@wxyz.com .
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Kappu Raghunathan
                                        <span class="text-yellow fs16 pl10">- Co-founder of Mi Therapy Clinic</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/Kappu-Raghunathan-1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <p>
                                                Kappu Raghunathan is the co-founder of Mi Therapy Clinic in Southfield. Kappu graduated from Dr. MGR Medical University in India in 1992. Due to her interest in women's health, she trained to
                                                be a pelvic floor therapist, and treats patients with pelvic issues using a holistic and integrated approach.
                                            </p>
                                            <p>
                                                She has a unique skill set which allows her to treat both the internal and external pelvic structures. This allows her to treat the pelvic unit as a whole and thus achieve more successful
                                                outcomes. She is also a vestibular therapist treating patients with vertigo and loss of balance. A self taught Chef,she was also the anchor of a popular Cooking Show on Zee TV called
                                                “Anjaraipetty”, where she dished out more than 350 recipes a year.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<script>
    $(".speakers-more-details").click(function () {
        $(this).parent().parent().parent().find(".speakers-content").toggleClass("speakers-details-height");

        if ($(this).text() == "Show More +") {
            $(this).text("Show Less - ");
        } else {
            $(this).text("Show More +");
        }
    });
</script>

<?php include 'footer.php';?>
