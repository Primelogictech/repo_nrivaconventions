<?php include 'header.php';?>

<style type="text/css">
    p {
        font-size: 16px;
        margin-bottom: 20px;
    }
</style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/lakshmi-narasimha-swami-kalyanam.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row mx-md-1">
                            <div class="col-12 col-md-5 col-lg-4 my-1 px-0  pl-md-0 pr-md-3">
                                <div class="p5 border">
                                    <img src="images/lakshmi-kalayanam3.jpg" class="img-fluid w-100" alt="lakshmi kalayanam" />
                                </div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-8 shadow-small px-4 py-3 my-1">
                                <h5 class="text-danger">Contact Details:</h5>
                                <div class="fs16">
                                    <label>Chair Name:</label>
                                    <span class="text-skyblue">Prashanth Kaparthi</span>
                                </div>
                                <div class="fs16">
                                    <label><i class="fas fa-mobile-alt"></i> :</label>
                                    <span class="text-skyblue">313-XXX-XXXX</span>
                                </div>
                                <div class="fs16">
                                    <label>Co-Chair Name:</label>
                                    <span class="text-skyblue">Swapna Ellendula</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 py-4">
                                <h4 class="mb-3 text-violet">Importance of performing god’s kalyanam</h4>
                                <div class="text-center float-sm-none float-md-right bg-white p5 border">
                                    <img src="images/lakshmi-kalayanam2.jpg" class="" width="265" alt="Nagender_Aytha" />
                                </div>
                                <p>
                                    Celebrating the wedding/kalyanam of the Gods as part of the Hindu culture is an important and auspicious community event. Kalyana Utsava celebration catalyzes the applications of Vedic traditions and
                                    shaping the spiritual learnings of the communities involved.
                                </p>
                                <p>
                                    Kalyanam means marriage, but doesn’t imply marriage alone, it means well-being of the community. The Lord’s Kalyanam is performed for ‘loka ksheman’ and ‘loka kalyanam’.
                                </p>
                                <p>
                                    May Lord Narasimha Swamy and His Divine Consort Shri Lakshmi Devi, who are always full of auspiciousness, bless the communities involved in this kalyAna mahOtsavam
                                </p>
                                <p><strong>Note:</strong> We encourage all the devotees to register for kalyanam event.</p>
                            </div>
                            <div class="col-12">
                                <h4 class="mb-3 text-violet">FAQ’s</h4>
                                <ul class="list-unstyled">
                                    <li class="py-1 fs16"><strong>Q.</strong> Who can participate in the event?</li>
                                    <li class="py-1 fs16"><strong>A.</strong> All NRIVA members are welcomed to participate the event.</li>
                                    <li class="py-1 fs16"><strong>Q.</strong> Is there a registration fee for the event?</li>
                                    <li class="py-1 fs16"><strong>A.</strong> If you are a sponsor of the event one couple is allowed per family. If not, please use the register link to participate the event.</li>
                                    <li class="py-1 fs16"><strong>Q.</strong> Are we expected to bring anything for this event?</li>
                                    <li class="py-1 fs16"><strong>A.</strong> All the required items will be provided by the event organizers.</li>
                                </ul>
                            </div>
                            <div class="col-12 pt-3">
                                <div>
                                    <img src="images/Spiritual-Flyer.jpeg" class="img-fluid mx-auto d-block" alt="Spiritual Flyer" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
