<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/real-estate.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Nagesh
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/nages-2.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height pt-4">
                                            <h4 class="my-5 text-center text-skyblue">Coming Soon ...</h4>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<script>
    $(".speakers-more-details").click(function () {
        $(this).parent().parent().parent().find(".speakers-content").toggleClass("speakers-details-height");

        if ($(this).text() == "Show More +") {
            $(this).text("Show Less - ");
        } else {
            $(this).text("Show More +");
        }
    });
</script>

<?php include 'footer.php';?>
