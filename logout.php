<?php include 'header1.php';?>

<!-- Carousel Banners -->

<section class="container-fluid my-0 my-md-1">
    <div class="row">
        <div class="col-12 py-0 px-1 p-lg-0">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="images/banner.jpg" alt="First slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="images/banner.jpg" alt="Second slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="images/banner.jpg" alt="Third slide" />
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- End of Carousel Banners -->

<!-- Location section -->

<section class="container-fluid violet-gradient-bg mt-1 px-4 px-md-5">
    <div class="row">
        <div class="col-12 col-lg-3 text-white border-right border-right-md-0 px-0 px-lg-2 my-auto">
            <a href="calender.php" class="text-white text-decoration-none">
                <div class="d-flex my-auto text-center py-4 py-lg-5">
                    <div>
                        <i class="far fa-calendar-alt date-icon"></i>
                    </div>
                    <div class="my-auto">
                        <span class="fs25 text-white">Jul 1<sup>st</sup> to 3<sup>rd</sup>, 2022<br /></span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-12 col-lg-6 text-white border-right border-right-md-0 px-0 px-lg-4 my-auto">
            <div class="d-flex text-center py-4 py-lg-5">
                <div class="my-auto">
                    <i class="fas fa-map-marker-alt location-point-icon"></i>
                </div>
                <div class="my-auto">
                    <h4 class="text-yellow text-left text-lg-center fs23">Renaissance Schaumburg Convention Center Hotel</h4>
                    <div class="text-left text-lg-center fs16">1551 Thoreau Dr N, Schaumburg, IL 60173</div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-3 text-white pt-3 pb-4 py-lg-0 my-auto">
            <div class="text-center text-lg-right text-xl-center">
                <a href="registration.php" class="btn register-today-btn">REGISTER TODAY</a>
            </div>
        </div>
    </div>
</section>

<!-- Location section -->

<!-- Crackers -->

<!-- <div class="container-fluid">
    <div class="row">
        <canvas id="canvas"></canvas>
    </div>
</div> -->

<!-- Crackers -->

<!-- Messages -->

<section class="container-fluid mt-1 messages-bg px-4 px-md-5" id="messages">
    <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-4 py-lg-5 msg_bg" data-aos="fade-left" data-aos-duration="1000">
            <div>
                <h6 class="president-message pl-3 mb-3">President Message</h6>
            </div>
            <div class="border-top-violet bg-white pt-4 shadow-sm">
                <div class="px-3">
                    <div class="borders">
                        <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs16 font-weight-bold">Hari Raini</div>
                        <div class="text-center text-muted fs14">President</div>
                    </div>
                </div>
                <div class="text-white px-3 pb-4" style="background: #a63cf5;">
                    <p class="py-3 mb-0 text-center description">
                        For the past two months what I have learned from each and every one of you is, “We can come together in need of time and we care for.......
                    </p>
                    <div class="text-center pt-3">
                        <a href="president_message_details.php">
                            <!-- <img src="images/right-arrow.png" class="img-fluid" alt="" /> -->
                            <i class="fas fa-chevron-circle-right text-white fs20"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-4 py-lg-5 msg_bg" data-aos="fade-left" data-aos-duration="2000">
            <div>
                <h6 class="chairman-message pl-3 mb-3">Chairman Message</h6>
            </div>
            <div class="border-top-green bg-white pt-4 shadow-sm">
                <div class="px-3">
                    <div class="borders">
                        <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs16 font-weight-bold">Dr. Jayasimha Sunku</div>
                        <div class="text-center text-muted fs14">Chairman</div>
                    </div>
                </div>
                <div class="text-white px-3 pb-4" style="background: #f0a309;">
                    <p class="py-3 mb-0 text-center description">
                        Namasthy, I feel honored to represent the NRIVA as the Chairman for the 2020-2021 term. It is a true privilege to be among.......
                    </p>
                    <div class="text-center pt-3">
                        <a href="chairman_message_details.php">
                            <!-- <img src="images/right-arrow.png" class="img-fluid" alt="" /> -->
                            <i class="fas fa-chevron-circle-right text-white fs20"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-4 py-lg-5 msg_bg" data-aos="fade-left" data-aos-duration="3000">
            <div>
                <h6 class="convenor-message pl-3 mb-3">Convenor Message</h6>
            </div>
            <div class="border-top-light-grey bg-white pt-4 shadow-sm">
                <div class="px-3"> 
                    <div class="borders">
                        <img src="images/user.png" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs16 font-weight-bold">Convenor</div>
                        <div class="text-center text-muted fs14">Convenor</div>
                    </div>
                </div>
                <div class="text-white px-3 pb-4" style="background: #399679;">
                    <p class="py-3 mb-0 text-center text-white description px-3">
                        I feel delighted and honored to have been appointed as the Convener for the NRIVA 6th Global Convention which is going to be held from.......
                    </p>
                    <div class="text-center pt-3">
                        <a href="convenor_message_details.php">
                            <!-- <img src="images/right-arrow.png" class="img-fluid" alt="" /> -->
                            <i class="fas fa-chevron-circle-right text-white fs20"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-4 py-lg-5 msg_bg" data-aos="fade-left" data-aos-duration="4000">
            <div>
                <h6 class="co-convenor-message pl-3 mb-3">Co-Convenor Message</h6>
            </div>
            <div class="border-top-warning bg-white pt-4 shadow-sm">
                <div class="px-3">
                    <div class="borders">
                        <img src="images/user.png" class="mx-auto d-block rounded-circle" alt="" />
                    </div>
                    <div class="pt-0 pb-2">
                        <div class="text-center mb-1 fs16 font-weight-bold">Co-Convenor</div>
                        <div class="text-center text-muted fs14">Co-Convenor</div>
                    </div>
                </div>
                <div class="text-white px-3 pb-4" style="background: #da605f;">
                    <p class="py-3 mb-0 text-center text-white description px-3">
                        I could not be more thankful to be a part of such a wonderful organization and serve as Co-Convener for NRIVA.......
                    </p>
                    <div class="text-center pt-3">
                        <a href="co-convenor_message_details.php">
                            <!-- <img src="images/right-arrow.png" class="img-fluid" alt="" /> -->
                            <i class="fas fa-chevron-circle-right text-white fs20"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Messages -->

<!-- Pillars Of Our Success Leadership -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12 pb-md-4 leadership-heading-bottom">
            <div class="main-heading">
                <div>
                    Pillars Of Our Success Leadership
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid leadership-bg">
    <div class="row pr-3">
        <div class="col-12 col-lg-2 py-0 py-lg-5">
            <div>
                <ul class="list-unstyled leadership-tabs-ul mb-md-0">
                    <li class="py-4 leadership-nav-item" data-aos="fade-right">
                        <a class="leadership-nav-link text-white text-decoration-none leadership_active_tab_btn" target-id="#convention-leadership">
                            <span>Convention Leadership</span> <i class="fas fa-long-arrow-alt-right fs22 align-middle leadership-navbar-right-arrow"></i>
                        </a>
                    </li>
                    <li class="py-4 leadership-nav-item" data-aos="fade-right">
                        <a class="leadership-nav-link text-white text-decoration-none" target-id="#convention-committees">
                            <span>Convention Committee</span> <i class="fas fa-long-arrow-alt-right fs22 align-middle leadership-navbar-right-arrow"></i>
                        </a>
                    </li>
                    <li class="py-4 leadership-nav-item" data-aos="fade-right">
                        <a class="leadership-nav-link text-white text-decoration-none" target-id="#executive-committee">
                            <span>Executive Committee</span><i class="fas fa-long-arrow-alt-right fs22 align-middle leadership-navbar-right-arrow"></i>
                        </a>
                    </li>
                    <li class="py-4 leadership-nav-item" data-aos="fade-right">
                        <a class="leadership-nav-link text-white text-decoration-none" target-id="#founders-n-bod">
                            <span>Founders & BOD'S</span> <i class="fas fa-long-arrow-alt-right fs22 align-middle leadership-navbar-right-arrow"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-12 col-lg-9 leaders-bg pt20 border-radius-15 ml-2 px-4 px-md-5 leadership-bg-top">
            <div class="row leadership_active_tab" id="convention-leadership">
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
                        </div>
                        <div class="text-center mb10 text-white">Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Ravi Ellendula</a>
                        </div>
                        <div class="text-center mb10 text-white">General Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Gangadhar_Upalla.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Gangadhar Vuppala</a>
                        </div>
                        <div class="text-center mb10 text-white">Joint Treasurer</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
                        </div>
                        <div class="text-center mb10 text-white">Past President</div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="pb50 text-right">
                        <a href="convention_leadership_committee.php" class="text-decoration-none text-white font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
                    </div>
                </div>
            </div>

            <div class="row" id="convention-committees" style="display: none;">
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
                        </div>
                        <div class="text-center mb10 text-white">Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Ravi Ellendula</a>
                        </div>
                        <div class="text-center mb10 text-white">General Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Gangadhar_Upalla.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Gangadhar Vuppala</a>
                        </div>
                        <div class="text-center mb10 text-white">Joint Treasurer</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="pb50 text-right">
                        <a href="convention_leadership_committee.php" class="text-decoration-none text-white font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
                    </div>
                </div>
            </div>

            <div class="row" id="executive-committee" style="display: none;">
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
                        </div>
                        <div class="text-center mb10 text-white">Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Ravi Ellendula</a>
                        </div>
                        <div class="text-center mb10 text-white">General Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Gangadhar_Upalla.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Gangadhar Vuppala</a>
                        </div>
                        <div class="text-center mb10 text-white">Joint Treasurer</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                 <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="pb50 text-right">
                        <a href="convention_leadership_committee.php" class="text-decoration-none text-white font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
                    </div>
                </div>
            </div>

            <div class="row" id="founders-n-bod" style="display: none;">
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Gangadhar_Upalla.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Gangadhar Vuppala</a>
                        </div>
                        <div class="text-center mb10 text-white">Joint Treasurer</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
                        </div>
                        <div class="text-center mb10 text-white">Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Ravi Ellendula</a>
                        </div>
                        <div class="text-center mb10 text-white">General Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Gangadhar_Upalla.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Gangadhar Vuppala</a>
                        </div>
                        <div class="text-center mb10 text-white">Joint Treasurer</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="pb50 text-right">
                        <a href="convention_leadership_committee.php" class="text-decoration-none text-white font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Pillars Of Our Success Leadership -->

<!-- Convention Programs -->

<section class="container-fluid mt-lg-90">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Convention Programs
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 my-3" data-aos="fade-down">
                <a href="lakshmi_narasimha_swami_kalyanam_details.php">
                    <div>
                        <img src="images/NRIVA convention programs/lakshmi-narasimha-swamy-kalyanam.jpg" class="img-fluid mx-auto d-block border-radius-10" alt="" />
                    </div>
                </a>
                <div class="pt-3">
                    <h5 class="text-left text-sm-center text-md-left">Lakshmi Narasimha Swamy Kalyanam</h5>
                </div>
            </div>
            <div class="col-12 col-md-4 my-3" data-aos="fade-down">
                <a href="live_musical_concert_details.php">
                    <div>
                        <img src="images/NRIVA convention programs/live-musical-concert.jpg" class="img-fluid mx-auto d-block border-radius-10" alt="" />
                    </div>
                </a>
                <div class="pt-3">
                    <h5 class="text-left text-sm-center text-md-left">Live Musical Concert</h5>
                </div>
            </div>
            <div class="col-12 col-md-4 my-3" data-aos="fade-down">
                <a href="stocks_investments_session_details.php">
                    <div>
                        <img src="images/NRIVA convention programs/stocks-investments-session.jpg" class="img-fluid mx-auto d-block border-radius-10" alt="" />
                    </div>
                </a>
                <div class="pt-3">
                    <h5 class="text-left text-sm-center text-md-left">Stocks & Investments Session</h5>
                </div>
            </div>
            <div class="col-12 col-md-4 my-3" data-aos="fade-down">
                <a href="#">
                    <div>
                        <img src="images/NRIVA convention programs/real-estate-sessions.jpg" class="img-fluid mx-auto d-block border-radius-10" alt="" />
                    </div>
                </a>
                <div class="pt-3">
                    <h5 class="text-left text-sm-center text-md-left">Real Estate Sessions</h5>
                </div>
            </div>
            <div class="col-12 col-md-4 my-3" data-aos="fade-down">
                <a href="youth_activities_registration.php">
                    <div>
                        <img src="images/NRIVA convention programs/youth-lock-In-at-paradise-park.jpg" class="img-fluid mx-auto d-block border-radius-10" alt="" />
                    </div>
                </a>
                <div class="pt-3">
                    <h5 class="text-left text-sm-center text-md-left">Youth Lock-In at Paradise Park</h5>
                </div>
            </div>
            <div class="col-12 col-md-4 my-3" data-aos="fade-down">
                <a href="nriva_core_team_appreciation_night_details.php">
                    <div>
                        <img src="images/NRIVA convention programs/core-teamappreciation-night.jpg" class="img-fluid mx-auto d-block border-radius-10" alt="" />
                    </div>
                </a>
                <div class="pt-3">
                    <h5 class="text-left text-sm-center text-md-left">NRIVA Core Team Appreciation Night</h5>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Convention Programs -->

<!-- Convention Invitees -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Convention Invitees
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid invitees_bg py-5">
    <div class="row pr-3">
        <div class="col-12 col-md-3 col-lg-2">
            <div>
                <ul class="list-unstyled invitees-tabs-ul mb-0 pt-0 pt-md-5">
                    <li class="py-4 invitees-nav-item" data-aos="fade-right">
                        <a class="invitees-nav-link text-white text-decoration-none fs18 invitees_active_tab_btn" target-id="#all-invitees"><span>All</span> <i class="fas fa-long-arrow-alt-right fs22 align-middle invitees-navbar-right-arrow"></i></a>
                    </li>
                    <li class="py-4 invitees-nav-item" data-aos="fade-right">
                        <a class="invitees-nav-link text-white text-decoration-none fs18" target-id="#dignitaries"><span>Dignitaries</span> <i class="fas fa-long-arrow-alt-right fs22 align-middle invitees-navbar-right-arrow"></i></a>
                    </li>
                    <li class="py-4 invitees-nav-item" data-aos="fade-right">
                        <a class="invitees-nav-link text-white text-decoration-none fs18" target-id="#entertainment"><span>Entertainment</span> <i class="fas fa-long-arrow-alt-right fs22 align-middle invitees-navbar-right-arrow"></i></a>
                    </li>
                    <li class="py-4 invitees-nav-item" data-aos="fade-right">
                        <a class="invitees-nav-link text-white text-decoration-none fs18" target-id="#speakers"><span>Speakers</span> <i class="fas fa-long-arrow-alt-right fs22 align-middle invitees-navbar-right-arrow"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-12 col-md-8 offset-md-1 col-lg-9 offset-lg-1 invitees-left-spacing">
            <div class="row invitees_active_tab" id="all-invitees">
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">MR. HARI RAINI</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Member Of Rajya Sabha">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/jayasimha_sunku.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Dr. Jayasimha Sunku</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Minister for Environment Telangana">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Nagender_Aytha.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Nagender Aytha</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Police Housing Corporation Chairman - TS">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Ravi Ellendula</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Chairman of GMR Group">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Gangadhar_Upalla.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Gangadhar Vuppala</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="MLA">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">MR. HARI RAINI</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Member Of Rajya Sabha">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/jayasimha_sunku.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Dr. Jayasimha Sunku</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Minister for Environment Telangana">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Nagender_Aytha.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Nagender Aytha</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Police Housing Corporation Chairman - TS">Member of XYZ</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="dignitaries" style="display: none;">
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/jayasimha_sunku.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Dr. Jayasimha Sunku</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Minister for Environment Telangana">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Nagender_Aytha.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Nagender Aytha</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Police Housing Corporation Chairman - TS">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Ravi Ellendula</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Chairman of GMR Group">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Gangadhar_Upalla.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Gangadhar Vuppala</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="MLA">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">MR. HARI RAINI</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Member Of Rajya Sabha">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/jayasimha_sunku.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Dr. Jayasimha Sunku</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Minister for Environment Telangana">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Nagender_Aytha.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Nagender Aytha</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Police Housing Corporation Chairman - TS">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Hari Raini</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Member Of Rajya Sabha">Member of XYZ </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="entertainment" style="display: none;
            ">
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Nagender_Aytha.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Nagender Aytha</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Police Housing Corporation Chairman - TS">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Ravi Ellendula</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Chairman of GMR Group">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Gangadhar_Upalla.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Gangadhar Vuppala</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="MLA">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">MR. HARI RAINI</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Member Of Rajya Sabha">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/jayasimha_sunku.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Dr. Jayasimha Sunku</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Minister for Environment Telangana">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Nagender_Aytha.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Nagender Aytha</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Police Housing Corporation Chairman - TS">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Hari Raini</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Member Of Rajya Sabha">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/jayasimha_sunku.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Dr. Jayasimha Sunku</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Minister for Environment Telangana">Member of XYZ</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="speakers" style="display: none;">
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">ijkl</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Member Of Rajya Sabha">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/jayasimha_sunku.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Dr. Jayasimha Sunku</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Minister for Environment Telangana">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Nagender_Aytha.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Nagender Aytha</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Police Housing Corporation Chairman - TS">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Ravi Ellendula</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Chairman of GMR Group">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Gangadhar_Upalla.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Gangadhar Vuppala</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="MLA">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">MR. HARI RAINI</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Member Of Rajya Sabha">Member of XYZ </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/jayasimha_sunku.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Dr. Jayasimha Sunku</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Minister for Environment Telangana">Member of XYZ</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Nagender_Aytha.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">Mr. Nagender Aytha</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Police Housing Corporation Chairman - TS">Member of XYZ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Convention Invitees -->

<!-- Our Donors -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Our Donors
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid donors-bg">
    <section class="container py-3">
        <div class="row">
            <div class="col-12">
                <div>
                    <ul class="list-unstyled text-center horizontal-scroll">
                        <li class="donors-nav-item d-inline-flex">
                            <a class="donors-nav-link text-dark fs18 pt10 pb10 text-decoration-none donors_active_tab_btn" target-id="#sapphire">Sapphire</a>
                        </li>
                        <li class="donors-nav-item d-inline-flex">
                            <a class="donors-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#diamond">Diamond</a>
                        </li>
                        <li class="donors-nav-item d-inline-flex">
                            <a class="donors-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#platinum">Platinum</a>
                        </li>
                        <li class="donors-nav-item d-inline-flex">
                            <a class="donors-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#gold">Gold</a>
                        </li>
                        <li class="donors-nav-item d-inline-flex">
                            <a class="donors-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#silver">Silver</a>
                        </li>
                        <li class="donors-nav-item d-inline-flex">
                            <a class="donors-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#bronze">Bronze</a>
                        </li>
                        <li class="donors-nav-item d-inline-flex">
                            <a class="donors-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#patron">Patron</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row donors_active_tab" id="sapphire">
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 py-2">
                <div class="text-center">
                    <a href="donors_more_details.php" class="btn px-3 bg-info text-white text-decoration-none border-radius-20">See More</a>
                </div>
            </div>
        </div>

        <div class="row" id="diamond" style="display: none;">
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 py-2">
                <div class="text-center">
                    <a href="donors_more_details.php" class="btn px-3 bg-info text-white text-decoration-none border-radius-20">See More</a>
                </div>
            </div>
        </div>

        <div class="row" id="platinum" style="display: none;">
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 py-2">
                <div class="text-center">
                    <a href="donors_more_details.php" class="btn px-3 bg-info text-white text-decoration-none border-radius-20">See More</a>
                </div>
            </div>
        </div>

        <div class="row" id="gold" style="display: none;">
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 py-2">
                <div class="text-center">
                    <a href="donors_more_details.php" class="btn px-3 bg-info text-white text-decoration-none border-radius-20">See More</a>
                </div>
            </div>
        </div>

        <div class="row" id="silver" style="display: none;">
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 py-2">
                <div class="text-center">
                    <a href="donors_more_details.php" class="btn px-3 bg-info text-white text-decoration-none border-radius-20">See More</a>
                </div>
            </div>
        </div>

        <div class="row" id="bronze" style="display: none;">
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 py-2">
                <div class="text-center">
                    <a href="donors_more_details.php" class="btn px-3 bg-info text-white text-decoration-none border-radius-20">See More</a>
                </div>
            </div>
        </div>

        <div class="row" id="patron" style="display: none;">
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Ravi_Ellendula.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Ravi Ellendula</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Gangadhar_Upalla.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Gangadhar Vuppala
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Hari_Raini.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Hari Raini</h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/jayasimha_sunku.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">
                        Mr. Jayasimha Sunku
                    </h6>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-20 my-3 px-5 px-sm-4 px-md-3 px-lg-4">
                <div>
                    <img src="images/Nagender_Aytha.jpg" class="img-fluid w-100 border-radius-top-right-15 donor-image-height" alt="" />
                </div>
                <div class="home-donor-name py-2">
                    <h6 class="text-uppercase px-2 text-white text-center mb-0">Mr. Nagender Aytha</h6>
                </div>
            </div>
            <div class="col-12 py-2">
                <div class="text-center">
                    <a href="donors_more_details.php" class="btn px-3 bg-info text-white text-decoration-none border-radius-20">See More</a>
                </div>
            </div>
        </div>
    </section>
</section>

<!-- End of Our Donors -->

<!-- Events & Schedule -->

<section class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Events & Schedule
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                <div class="event-details ed-card ed-card1">
                    <div>
                        <img src="images/Business-and-womens_conference3.jpg" class="img-fluid w-100 border-radius-top-right-15" alt="" />
                    </div>
                    <div class="p-3 event-date1">
                        <div class="fs18 text-white text-left py-2">
                            <span>Thursday</span>
                            <span>July 4th 2021</span>
                        </div>
                        <div class="text-white text-center event-schedule">
                            <span class="text-white">DATE:</span> Sunday, July 7th 2021 <br />
                            Sri Laxmi Narasimha Swamy Kalyanam &amp; Shathamanam Bhavathi
                            <a href="convention_schedule_details.php" class="text-white">more... </a>
                        </div>
                    </div>
                    <div class="row event-time-n-kn px15">
                        <div class="col-6 px-0">
                            <div class="event-time-block">
                                <div class="event-time">
                                    <span><i class="far fa-clock pr-2"></i></span>
                                    <span>8:00 AM to 4:00 PM</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 px-0">
                            <div class="event-km">
                                <a href="convention_schedule_details.php" class="text-decoration-none text-white">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                <div class="event-details ed-card ed-card2">
                    <div>
                        <img src="images/Leadership-Appreciation-Dinner.jpg" class="img-fluid w-100 border-radius-top-right-15" alt="" />
                    </div>
                    <div class="p-3 event-date2">
                        <div class="fs18 text-white text-left py-2">
                            <span>Thursday</span>
                            <span>July 4th 2021</span>
                        </div>
                        <div class="text-white text-center event-schedule">
                            <span class="text-white">DATE:</span> Sunday, July 7th 2021 <br />
                            Sri Laxmi Narasimha Swamy Kalyanam &amp; Shathamanam Bhavathi
                            <a href="convention_schedule_details.php" class="text-white">more... </a>
                        </div>
                    </div>
                    <div class="row event-time-n-kn px15">
                        <div class="col-6 px-0 event-time-block">
                            <div class="event-time">
                                <span><i class="far fa-clock pr-2"></i></span>
                                <span>8:00 AM to 4:00 PM</span>
                            </div>
                        </div>
                        <div class="col-6 px-0">
                            <div class="event-km">
                                <a href="convention_schedule_details.php" class="text-decoration-none text-white">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                <div class="event-details ed-card ed-card3">
                    <div>
                        <img src="images/Breakfast-and-matrimony.jpg" class="img-fluid w-100 border-radius-top-right-15" alt="" />
                    </div>
                    <div class="p-3 event-date3">
                        <div class="fs18 text-white text-left py-2">
                            <span>Thursday</span>
                            <span>July 4th 2021</span>
                        </div>
                        <div class="text-white text-center event-schedule">
                            <span class="text-white">DATE:</span> Sunday, July 7th 2021 <br />
                            Sri Laxmi Narasimha Swamy Kalyanam &amp; Shathamanam Bhavathi
                            <a href="convention_schedule_details.php" class="text-white">more... </a>
                        </div>
                    </div>
                    <div class="row event-time-n-kn px15">
                        <div class="col-6 px-0 event-time-block">
                            <div class="event-time">
                                <span><i class="far fa-clock pr-2"></i></span>
                                <span>8:00 AM to 4:00 PM</span>
                            </div>
                        </div>
                        <div class="col-6 px-0">
                            <div class="event-km">
                                <a href="convention_schedule_details.php" class="text-decoration-none text-white">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                <div class="event-details ed-card ed-card4">
                    <div>
                        <img src="images/Laxmi-narsimhaswamy-kalyanam2.jpg" class="img-fluid w-100 border-radius-top-right-15" alt="" />
                    </div>
                    <div class="p-3 event-date4">
                        <div class="fs18 text-white text-left py-2">
                            <span>Thursday</span>
                            <span>July 4th 2021</span>
                        </div>
                        <div class="text-white text-center event-schedule">
                            <span class="text-white">DATE:</span> Sunday, July 7th 2021 <br />
                            Sri Laxmi Narasimha Swamy Kalyanam &amp; Shathamanam Bhavathi
                            <a href="convention_schedule_details.php" class="text-white">more... </a>
                        </div>
                    </div>
                    <div class="row event-time-n-kn px15">
                        <div class="col-6 px-0 event-time-block">
                            <div class="event-time">
                                <span><i class="far fa-clock pr-2"></i></span>
                                <span>8:00 AM to 4:00 PM</span>
                            </div>
                        </div>
                        <div class="col-6 px-0">
                            <div class="event-km">
                                <a href="convention_schedule_details.php" class="text-decoration-none text-white">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                <div class="event-details ed-card ed-card5">
                    <div>
                        <img src="images/yoga-for-schedule.jpg" class="img-fluid w-100 border-radius-top-right-15" alt="" />
                    </div>
                    <div class="p-3 event-date5">
                        <div class="fs18 text-white text-left py-2">
                            <span>Thursday</span>
                            <span>July 4th 2021</span>
                        </div>
                        <div class="text-white text-center event-schedule">
                            <span class="text-white">DATE:</span> Sunday, July 7th 2021 <br />
                            Sri Laxmi Narasimha Swamy Kalyanam &amp; Shathamanam Bhavathi
                            <a href="convention_schedule_details.php" class="text-white">more... </a>
                        </div>
                    </div>
                    <div class="row event-time-n-kn px15">
                        <div class="col-6 px-0 event-time-block">
                            <div class="event-time">
                                <span><i class="far fa-clock pr-2"></i></span>
                                <span>8:00 AM to 4:00 PM</span>
                            </div>
                        </div>
                        <div class="col-6 px-0">
                            <div class="event-km">
                                <a href="convention_schedule_details.php" class="text-decoration-none text-white">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Events & Schedule -->

<!-- Youtube Vedios -->
<section class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                   Convention Video Gallery
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home-vedio-bg pt50 pb50">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/fLAs8dNQRZ8" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image1.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Logo Launch</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/gGrfODuTcQc" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image2.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Invitation from President and Chairman</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/DtoQ1sW9JY4" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image3.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Promo</div>
            </div>
        </div>
        <div class="col-12 pt-3 px-0">
            <div class="text-right">
                <a href="videos.php" class="text-decoration-none font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
            </div>
        </div>
    </div>
</section>

<!-- End of Youtube Vedios -->

<!-- Donors Section -->

<section class="container-fluid bg-white pb-5">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    More About Convention Event
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg1">
                    <div class="my-auto">
                        <span class="fs18">323</span>
                        <span class="float-right"><img src="images/counting-icons/donors.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Donors</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg2">
                    <div class="my-auto">
                        <span class="fs18">8228</span>
                        <span class="float-right"><img src="images/counting-icons/visitors.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Site Visitors</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg3">
                    <div class="my-auto">
                        <span class="fs18">57759</span>
                        <span class="float-right"><img src="images/counting-icons/registrations.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Registrations</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg4">
                    <div class="my-auto">
                        <span class="fs18">50</span>
                        <span class="float-right"><img src="images/counting-icons/invitees.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Invitees</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2 counter-block-5">
                <div class="counter-width counter-card-bg5">
                    <div class="my-auto">
                        <span class="fs18">0</span>
                        <span class="float-right"><img src="images/counting-icons/performance.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Performance</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Donors Section -->

<?php include 'footer.php';?>

<script>
    $(".leadership-nav-link").click(function () {
        //active_tab
        $(".leadership_active_tab_btn").removeClass("leadership-nav-link");
        $(".leadership_active_tab_btn").removeClass("leadership_active_tab_btn");
        $(this).addClass("leadership-nav-link");
        $(this).addClass("leadership_active_tab_btn");

        $(".leadership_active_tab").hide();
        $(".leadership_active_tab").removeClass("leadership_active_tab");
        $($(this).attr("target-id")).addClass("leadership_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

<script>
    $(".invitees-nav-link").click(function () {
        //active_tab
        $(".invitees_active_tab_btn").removeClass("invitees-nav-link");
        $(".invitees_active_tab_btn").removeClass("invitees_active_tab_btn");
        $(this).addClass("invitees-nav-link");
        $(this).addClass("invitees_active_tab_btn");

        $(".invitees_active_tab").hide();
        $(".invitees_active_tab").removeClass("invitees_active_tab");
        $($(this).attr("target-id")).addClass("invitees_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

<script>
    $(".donors-nav-link").click(function () {
        //active_tab
        $(".donors_active_tab_btn").removeClass("donors-nav-link");
        $(".donors_active_tab_btn").removeClass("donors_active_tab_btn");
        $(this).addClass("donors-nav-link");
        $(this).addClass("donors_active_tab_btn");

        $(".donors_active_tab").hide();
        $(".donors_active_tab").removeClass("donors_active_tab");
        $($(this).attr("target-id")).addClass("donors_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

