<?php include 'header.php';?>
	
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small">
        <div class="row">
            <div class="col-12 px-0">
                <div class="leadership-heading-bg p-3">
                    <h4 class="mb-0">Convention Leadership</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 py-4 p-md-4 p40">
            	<div class="row">
            		<div class="col-12 mb-5">
                        <div>
                            <img src="images/banners/convention-leadership.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
            		<div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
	                    <div class="hexagon">
	                        <div>
	                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
	                        </div>
	                        <div class="text-center my-2">
	                            <a href="#" class="leader-name">Mr. Hari Raini</a>
	                        </div>
	                        <div class="text-center mb10 text-white">President</div>
	                    </div>
	                </div>
	                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
	                    <div class="hexagon">
	                        <div>
	                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
	                        </div>
	                        <div class="text-center my-2">
	                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
	                        </div>
	                        <div class="text-center mb10 text-white">Chairman</div>
	                    </div>
	                </div>
	                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
	                    <div class="hexagon">
	                        <div>
	                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
	                        </div>
	                        <div class="text-center my-2">
	                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
	                        </div>
	                        <div class="text-center mb10 text-white">Past President</div>
	                    </div>
	                </div>
	                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
	                    <div class="hexagon">
	                        <div>
	                            <img src="images/Ravi_Ellendula.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
	                        </div>
	                        <div class="text-center my-2">
	                            <a href="#" class="leader-name">Mr. Ravi Ellendula</a>
	                        </div>
	                        <div class="text-center mb10 text-white">General Secretary</div>
	                    </div>
	                </div>
	                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
                        </div>
                        <div class="text-center mb10 text-white">Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Ravi Ellendula</a>
                        </div>
                        <div class="text-center mb10 text-white">General Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
                        </div>
                        <div class="text-center mb10 text-white">Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Ravi Ellendula</a>
                        </div>
                        <div class="text-center mb10 text-white">General Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
                        </div>
                        <div class="text-center mb10 text-white">Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Ravi Ellendula</a>
                        </div>
                        <div class="text-center mb10 text-white">General Secretary</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/jayasimha_sunku.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Dr. Jayasimha Sunku</a>
                        </div>
                        <div class="text-center mb10 text-white">Chairman</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Nagender_Aytha.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Nagender Aytha</a>
                        </div>
                        <div class="text-center mb10 text-white">Past President</div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Ravi_Ellendula.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Ravi Ellendula</a>
                        </div>
                        <div class="text-center mb10 text-white">General Secretary</div>
                    </div>
                </div>
            	</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>