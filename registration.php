<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12 pb-5">
                        <div>
                            <img src="images/banners/convention-registration.jpg" class="img-fluid w-100" alt="">
                        </div>
                    </div>
                </div>
                <div class="row px-4 px-lg-0">
                    <div class="col-12 col-lg-10 offset-lg-1 shadow-small px-sm-30 p40 p-md-4 mb-5">
                        <div class="row">

                            <!-- Personal Information -->

                            <div class="col-12">
                                <h5 class="text-violet py-2">Personal Information</h5>
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>First Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="First Name" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Last Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Last Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Spouse First Name:</label>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Spouse First Name" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Spouse Last Name:</label>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Spouse Last Name" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- End of Personal Information -->

                            <!-- Contact Information -->

                            <div class="col-12">
                                <h5 class="py-2 text-violet">Contact Information</h5>
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Email Id:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Email Id" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Mobile:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Mobile" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Country:</label><span class="text-red">*</span>
                                            <div>
                                                <select class="form-control">
                                                    <option>Select Country</option>
                                                    <option>USA</option>
                                                    <option>India</option>
                                                    <option>Dubai</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>State:</label><span class="text-red">*</span>
                                            <div>
                                                <select class="form-control">
                                                    <option>Select State</option>
                                                    <option>Ohio</option>
                                                    <option>Florida</option>
                                                    <option>Texas</option>
                                                    <option>Columbia</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>City:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="City" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Zip Code:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Zip Code" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- end of Contact Information -->

                        <hr class="dashed-hr" />

                        <div class="row">
                            <div class="col-12 pt-2">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-5">
                                        <h5 class="text-violet">Registration / Sponsor Category</h5>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-4 px-3 px-md-0 px-lg-3">
                                        <div><input type="radio" class="" name="category" /><span class="fs16 mx-3">Family / Individual</span></div>
                                    </div>
                                    <div class="col-12 col-md-2 col-lg-3 px-3 px-md-0 px-lg-3">
                                        <div><input type="radio" class="" name="category" /><span class="fs16 mx-3">Donor's</span></div>
                                    </div>
                                </div>

                                <!-- Family / Individual Table -->

                                <div class="row my-3">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table class="datatable table table-bordered table-center mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Registration Category</th>
                                                        <th>Price</th>
                                                        <th style="width: 120px;">Count</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Family of 4 Admission ( 2 Adults + 2 Children)</td>
                                                        <td class="text-orange font-weight-bold">$ 250.00</td>
                                                        <td>
                                                            <input type="text" class="form-control" name="" />
                                                        </td>
                                                        <td>$ 20,000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <!-- End of Family / Individual Table -->

                                <!-- Donor's Table -->

                                <div class="row my-3">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table class="datatable table table-bordered table-center mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Sponsor Category</th>
                                                        <th>Amount</th>
                                                        <th style="min-width: 300px;">Benefits</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <input type="radio" name="sponsor_category" />
                                                            </div>
                                                        </td>
                                                        <td>Sapphire</td>
                                                        <td class="text-orange font-weight-bold">$25000</td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-6">
                                                                    <ul class="flower-list fs16">
                                                                        <li>2 Vendor booth spaces</li>
                                                                        <li>3 Hotel Rooms(3 nights)</li>
                                                                        <li>2 Shathamanam Bhavathi</li>
                                                                        <li>Admission Tickets (20)</li>
                                                                        <li>
                                                                            Banquet Admission (20)
                                                                        </li>
                                                                        <li>Kalyanam Tickets (12)</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-12 col-md-12 col-lg-6">
                                                                    <ul class="flower-list fs16">
                                                                        <li>Banner on the Website</li>
                                                                        <li>Recognition on main Statge</li>
                                                                        <li>Reserved Seating at the Convention</li>
                                                                        <li>
                                                                            Full page advertisement in Souvenir
                                                                        </li>
                                                                        <li>Lunch/Dinner in VIP Area</li>
                                                                        <li>Digital Banner on the Main Stage</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <input type="radio" name="sponsor_category"/>
                                                            </div>
                                                        </td>
                                                        <td>Diamond</td>
                                                        <td class="text-orange font-weight-bold">$20000</td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-6">
                                                                    <ul class="flower-list fs16">
                                                                       <li>1 Vendor booth space</li>
                                                                       <li>3 Hotel Rooms (3 nights)</li>
                                                                       <li>2 Shathamanam Bhavathi</li>
                                                                       <li>Admission Tickets (15)</li>
                                                                       <li>Banquet Admission (15)</li>
                                                                       <li>10 Kalyanam Tickets</li>
                                                                   </ul>
                                                               </div>
                                                               <div class="col-12 col-md-12 col-lg-6">
                                                                <ul class="flower-list fs16">
                                                                 <li>Banner on the Website</li>
                                                                 <li>Recognition on main Statge</li>
                                                                 <li>Reserved Seating at the Convention</li>
                                                                 <li>Full page advertisement in Souvenir</li>
                                                                 <li>Lunch/Dinner in VIP Area</li>
                                                                 <li>Digital Banner on the Main Stage</li>
                                                             </ul>
                                                         </div>
                                                     </div>
                                                 </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                    <div>
                                                        <input type="radio" name="sponsor_category"/>
                                                    </div>
                                                </td>
                                                <td>Platinum</td>
                                                <td class="text-orange font-weight-bold">$15000</td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-12 col-md-12 col-lg-6">
                                                            <ul class="flower-list fs16">
                                                               <li>1 Vendor booth space</li>
                                                               <li>2 Hotel Rooms (3 nights)</li>
                                                               <li>2 Shathamanam Bhavathi</li>
                                                               <li>Admission Pack (12 Tickets)</li>
                                                               <li>Banquet Admission (12)</li>
                                                               <li>Kalyanam Tickets (8)</li>
                                                           </ul>
                                                       </div>
                                                       <div class="col-12 col-md-12 col-lg-6">
                                                        <ul class="flower-list fs16">
                                                         <li>Banner on the Website</li>
                                                         <li>Recognition on main Statge</li>
                                                         <li>Reserved Seating at the Convention</li>
                                                         <li>Full page advertisement in Souvenir</li>
                                                         <li>Lunch/Dinner in VIP Area</li>
                                                         <li>Digital Banner on the Main Stage</li>
                                                     </ul>
                                                 </div>
                                             </div>
                                         </td>
                                     </tr>
                                     <tr>
                                        <td>
                                            <div>
                                                <input type="radio" name="sponsor_category"/>
                                            </div>
                                        </td>
                                        <td>Gold</td>
                                        <td class="text-orange font-weight-bold">$10000</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-12 col-md-12 col-lg-6">
                                                    <ul class="flower-list fs16">
                                                       <li>1 Vendor booth space(10x10)</li>
                                                       <li>2 Hotel Rooms (3 nights)</li>
                                                       <li>2 Shathamanam Bhavathi</li>
                                                       <li>Admission Tickets (10)</li>
                                                       <li>Banquet Tickets (10)</li>
                                                       <li>6 Kalyanam Tickets</li>
                                                   </ul>
                                               </div>
                                               <div class="col-12 col-md-12 col-lg-6">
                                                <ul class="flower-list fs16">
                                                 <li>Banner on the Website</li>
                                                 <li>Recognition on main Statge</li>
                                                 <li>Reserved Seating at the Convention</li>
                                                 <li>Full page advertisement in Souvenir</li>
                                                 <li>Lunch/Dinner in VIP Area</li>
                                                 <li>Digital Banner on the Main Stage</li>
                                             </ul>
                                         </div>
                                     </div>
                                 </td>
                             </tr>
                             <tr>
                                <td>
                                    <div>
                                        <input type="radio" name="sponsor_category"/>
                                    </div>
                                </td>
                                <td>Silver</td>
                                <td class="text-orange font-weight-bold">$5,000</td>
                                <td>
                                    <div class="row">
                                        <div class="col-12">
                                            <ul class="flower-list fs16">
                                                <li>Banner on the Website</li>
                                                <li>8 Admission, Banquet Tickets (8)</li>
                                                <li>Kalyanam Tickets (4)</li>
                                                <li>1 Hotel Rooms (3 nights)</li>
                                                <li>Reserved Seating</li>
                                                <li>Half page advertisement in Souvenir</li>
                                                <li>Shathamanam Bhavathi ticket (1)</li>
                                            </ul>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <input type="radio" name="sponsor_category"/>
                                        </div>
                                    </td>
                                    <td>Bronze</td>
                                    <td class="text-orange font-weight-bold">$2,500</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-12">
                                                <ul class="flower-list fs16">
                                                    <li>6Admission,6 Banquet Tickets</li>
                                                    <li>Kalyanam Tickets (2)</li>
                                                    <li>1 Hotel Rooms (3 nights)</li>
                                                    <li>Half page advertisement in Souvenir</li>
                                                </ul>
                                            </div>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <input type="radio" name="sponsor_category"/>
                                            </div>
                                        </td>
                                        <td>Patron</td>
                                        <td class="text-orange font-weight-bold">$1,500</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-12">
                                                    <ul class="flower-list fs16">
                                                        <li>Admission Tickets (4)</li>
                                                        <li>Banquet Tickets (2)</li>
                                                        <li>Kalyanam Tickets (1)</li>
                                                        <li>1 Hotel Rooms (2 nights)</li>
                                                        <li>Listing in Souvenir</li>
                                                    </ul>
                                                </div>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- End of Donor's Table -->

                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <h5 class="text-violet py-2">Additional DONOR Benefits (Admission to)</h5>
                    <ul class="flower-list ml-3 fs16">
                        <li>Business Conference</li>
                        <li>Women's Conference</li>
                        <li>Youth Conference</li>
                        <li>Youth Late Night Lockin</li>
                        <li>CME</li>
                        <li>Matrimonial Admission</li>
                        <li>Immigration Seminar</li>
                    </ul>
                </div>
            </div>
            <div class="row px-3">
                <div class="col-12 col-sm-10 col-md-6 col-lg-5 shadow-small py-4">
                    <h6 class="text-center">Upload Photo</h6>
                    <div class="browse-button py-2">
                        <input type="file" class="form-control browse" name="">
                    </div>
                    <div class="pt-3 text-center fs15">( File Size could not be more than 5MB )</div>
                </div>
            </div>
            <div class="row my-2 px-3">
                <div class="col-12 shadow-small p-4">
                    <div>
                        <div class="text-orange fs16">Note:-</div>
                        <ul class="list-unstyled pt-2 mb-0 fs16">
                            <li class="py-1">1. All registrations are on first come first serve basis and are final</li>
                            <li class="pt-1">2. *Copy of ID / Student ID / Visitors Visa must be presented when collecting the registration kit.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-12">
                    <h5 class="text-violet py-2">Payment Types</h5>
                    <div>
                        <span class="mr-1 mr-md-5 position-relative pr-3"><input type="radio" class="p-radio-btn" name="payment_type" /><span class="fs16 pl-4 pl-md-4">PayPal</span></span>
                        <span class="position-relative pr-3"><input type="radio" class="p-radio-btn" name="payment_type" /><span class="fs16 pl-4 pl-md-4">Cheque/Cash</span></span>
                    </div>
                </div>
            </div>
            <hr class="dashed-hr">
            <div class="row">
                <div class="col-12">
                    <h5 class="text-violet mb-3">Payment by using Cheque</h5>
                    <form>
                        <div class="form-row">
                            <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                <label>Cheque Number: </label><span class="text-red">*</span>
                                <div>
                                    <input type="text" name="" id="" class="form-control" placeholder="Cheque Number">
                                </div>
                            </div>
                            <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                <label>Handed over to:</label><span class="text-red">*</span>
                                <div>
                                    <input type="text" name="" id="" class="form-control" placeholder="Handed over to">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <hr class="dashed-hr">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6 pt-2">
                    <h5>Security Code</h5>
                    <div class="row">
                        <div class="col-11">
                            <div class="registration-captcha-block">
                                <div class="row">
                                    <div class="col-8 px-0 my-auto">
                                        <input type="text" class="border-0 form-control bg-transparent" name="" placeholder="Enter the Characters you see">
                                    </div>
                                    <div class="col-4 px-0 my-auto">
                                        <img src="images/ShowCaptchaImage.png" class="captcha-img" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1 px-2 my-auto">
                            <div>
                                <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 offset-md-1 col-md-5 pt-2">
                    <div class="row my-1">
                        <div class="col-12 px-3 px-md-0">
                            <div class="row">
                                <div class="col-7 col-md-8 my-auto">
                                    <label class="mb-0">Registration Amount</label>
                                </div>
                                <div class="col-1 my-auto">
                                    <label class="mb-0">:</label>
                                </div>
                                <div class="col-3 my-auto">
                                    <div class="payment">$ 0</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row my-1">
                        <div class="col-12 px-3 px-md-0 border-bottom">
                            <div class="row mb-2">
                                <div class="col-7 col-md-8 my-auto">
                                    <label class="mb-0">Donation Amount</label>
                                </div>
                                <div class="col-1 my-auto">
                                    <label class="mb-0">:</label>
                                </div>
                                <div class="col-3 my-auto">
                                    <div class="payment">$ 0</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row my-1">
                        <div class="col-12 px-3 px-md-0">
                            <div class="row mb-2">
                                <div class="col-7 col-md-8 my-auto">
                                    <label class="mb-0">Total Amount</label>
                                </div>
                                <div class="col-1 my-auto">
                                    <label class="mb-0">:</label>
                                </div>
                                <div class="col-3 my-auto">
                                    <div class="payment">$ 0</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="dashed-hr">
            <div class="row py-2">
                <div class="col-12 col-sm-6 col-md-6 my-auto">
                    <div>
                        <input type="checkbox" class="input-checkbox" name=""><span class="pl25 fs16">I accept Terms and Conditions</span>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 my-2 my-md-auto">
                    <div class="text-center text-sm-right">
                        <div class="btn btn-lg btn-danger text-uppercase px-5">Check out</div>
                    </div>
                </div>
            </div>
            <div class="row pt-3 pb-2">
                <div class="col-12 fs16">
                    <div class="">
                        <span class="text-uppercase font-weight-bold">MAKE CHECKS PAYABLE TO :</span>
                        <span>NRIVA</span>
                    </div>
                    <div>Cheque Mailing Address:-</div>
                    <div>NRIVA 5th Global Convention</div>
                    <div>24328 Amanda Ln,</div>
                    <div>Novi, MI 48375</div>
                    <div>
                        <span><b>For online registration</b></span>
                        <span>and Convention programs & updates, please visit</span>
                        <a href="#" class="text-orange">convention.nriva.org</a>
                    </div>
                    <div>
                        <span><b>For Registration Information, email,</b></span>
                        <a href="#" class="text-orange">registration2019@nriva.org</a>
                        <span>or reach</span>
                        <a href="event_registration_committee.php" class="text-orange">Registration Committee.</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</section>

<?php include 'footer.php';?>