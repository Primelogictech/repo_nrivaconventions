<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Add Manage Sponsor Category</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="manage_sponsor_category.php" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Registration / Sponsor Category <span class="mandatory">*</span></label>
						<div class="col-md-4 my-auto">
							<input type="radio" class="radio-btn" name="">
							<span class="pl25">Donar</span>
						</div>
						<div class="col-md-4 my-auto">
							<input type="radio" class="radio-btn" name="">
							<span class="pl25">Family / Individual</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Enter Sponsor Category<span class="mandatory">*</span></label>
						<div class="col-12 col-md-8 my-auto">
							<input type="text" id="" name="" value="" class="form-control" placeholder="Enter Sponsor Category" required="">
						</div>
						<div class="col-md-4"></div><div class="col-md-8 mandatory" id="email_error"></div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4">Enter Amount<span class="mandatory">*</span></label>
						<div class="col-12 col-md-8">
							<input type="text" id="" name="" value="" class="form-control" placeholder="Enter Amount" required="">
						</div>
						<div class="col-md-4"></div><div class="col-md-8 mandatory" id="email_error"></div>
					</div>
					<div class="form-group row mb-2">
						<label class="col-form-label col-md-4 my-auto">Add Benifits<span class="mandatory">*</span></label>
						<div class="col-12 col-md-8 my-auto">
							<div class="row mb-2">
								<div class="col-10 col-md-11 my-auto">
									<input type="text" id="" name="" value="" class="form-control" placeholder="" required="">
								</div>
								<div class="col-2 col-md-1 my-auto">
									<div>
										<span class="plus_n_minus_icons"><i class="fas fa-plus"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="row mb-2">
								<div class="col-10 col-md-11 my-auto">
									<input type="text" id="" name="" value="" class="form-control" placeholder="" required="">
								</div>
								<div class="col-2 col-md-1 my-auto">
									<div>
										<span class="plus_n_minus_icons"><i class="fas fa-minus"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4"></div><div class="col-md-7 mandatory" id="email_error"></div>
					</div>
					<div class="form-group row">
						<div class="col-12 col-md-4 offset-md-4">
							<div class="btn btn-primary w-100">Create</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4">Enter Registration Category Name<span class="mandatory">*</span></label>
						<div class="col-12 col-md-4 my-auto">
							<input type="text" id="" name="" value="" class="form-control" placeholder="" required="">
						</div>
						<div class="col-10 col-md-3 my-auto">
							<input type="text" id="" name="" value="" class="form-control" placeholder="Enter Price" required="">
						</div>
						<div class="col-2 col-md-1 my-auto">
							<div>
								<span class="plus_n_minus_icons"><i class="fas fa-plus"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4"></div><div class="col-md-8 mandatory" id="email_error"></div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4">Enter Registration Category Name<span class="mandatory">*</span></label>
						<div class="col-12 col-md-4 my-auto">
							<input type="text" id="" name="" value="" class="form-control" placeholder="" required="">
						</div>
						<div class="col-10 col-md-3 my-auto">
							<input type="text" id="" name="" value="" class="form-control" placeholder="Enter Price" required="">
						</div>
						<div class="col-2 col-md-1 my-auto">
							<div>
								<span class="plus_n_minus_icons"><i class="fas fa-minus"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4"></div><div class="col-md-8 mandatory" id="email_error"></div>
					</div>
					<div class="form-group row">
						<div class="col-12 col-md-4 offset-md-4">
							<div class="btn btn-primary w-100">Create</div>
						</div>
					</div>
					<div class="text-right">
						<input class="btn btn-primary" type="submit" name="submit" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php';?>