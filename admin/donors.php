<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Donors</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="add_donors.php" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>Sl NO.</th>
								<th>Donor Name</th>
								<th>Image</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Nagendar Aytha</td>
								<td>
									<div>
										<img src="images/Nagender Aytha.jpg" alt="Nagender Aytha.jpg" class="img-fluid">
									</div>
								</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">View</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
									<a href="assign_roles.php" class="btn btn-sm btn-success my-1 mx-1">Assign Roles</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Praveen Thadakamalla</td>
								<td>
									<div>
										<img src="images/Nagender Aytha.jpg" alt="Nagender Aytha.jpg" class="img-fluid">
									</div>
								</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">View</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
									<a href="assign_roles.php" class="btn btn-sm btn-success my-1 mx-1">Assign Roles</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php';?>
