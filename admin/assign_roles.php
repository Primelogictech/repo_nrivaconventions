<?php include 'header.php';?>

<style type="text/css">

.plus-iconn{
	/*border: 1px solid;
    padding: 7px;
    border-radius: 20px;*/
    padding: 3px 8px;
}
</style>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Assign roles</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="leadership.php" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group row">
						<div class="col-12 pt-2">
							<div class="row">
								<div class="col-12 col-lg-3">
									<h6 class="mb-3 font-weight-bold">Type</h6>
								</div>
								<div class="col-12 col-lg-4">
									<h6 class="mb-3 font-weight-bold">Sub-Type</h6>
								</div>
								<div class="col-12 col-lg-4">
									<h6 class="mb-3 font-weight-bold">Designation</h6>
								</div>
							</div>
							<div class="row my-2">
								<div class="col-12 col-sm-6 col-md-3 my-1 my-sm-auto">
									<input type="checkbox" class="checbox cursor-pointer" name=""><span class="pl25">Success Leadership</span>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
									<div>
										<select class="form-control">
											<option>-- Select --</option>
											<option>Convention Leadership</option>
											<option>Convention Committee</option>
											<option>Executive Committee</option>
											<option>Founders & BOD'S</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
									<div>
										<select class="form-control">
											<option>-- Select --</option>
											<option>President</option>
											<option>Chairman</option>
											<option>Convenor</option>
											<option>Co-Convenor</option>
										</select>
									</div>
								</div>
								<div class="col-lg-1 my-auto">
									<!-- <div class="">
										<i class="fas fa-plus plus-iconn"></i>
									</div> -->
									<div class="btn btn-primary cursor-pointer plus-iconn" id="btn"><i class="fa fa-plus"></i></div>
								</div>
							</div>
							<div class="row" id="add_subtype_n_designation" style="display: none;">
								<div class="col-lg-3 my-1 my-sm-auto">
									
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
									<div>
										<select class="form-control">
											<option>-- Select --</option>
											<option>Convention Leadership</option>
											<option>Convention Committee</option>
											<option>Executive Committee</option>
											<option>Founders & BOD'S</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
									<div>
										<select class="form-control">
											<option>-- Select --</option>
											<option>President</option>
											<option>Chairman</option>
											<option>Convenor</option>
											<option>Co-Convenor</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row my-2">
								<!-- <div class="col-12">
									<h6 class="mb-2">Sub-Type</h6>
								</div> -->
								<div class="col-12 col-sm-6 col-md-3 my-1 my-sm-auto">
									<input type="checkbox" class="checbox cursor-pointer" name=""><span class="pl25">Convention Invitees</span>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
									<div>
										<select class="form-control">
											<option>-- Select --</option>
											<option>Dignitaries</option>
											<option>Entertainment</option>
											<option>Speakers</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
									<div>
										<select class="form-control">
											<option>-- Select --</option>
											<option>President</option>
											<option>Chairman</option>
											<option>Convenor</option>
											<option>Co-Convenor</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row my-2">
								<div class="col-12 col-sm-6 col-md-3 my-1 my-sm-auto">
									<input type="checkbox" class="checbox cursor-pointer" name=""><span class="pl25">Convention Donors</span>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
									<div>
										<select class="form-control">
											<option>-- Select --</option>
											<option>Sapphire</option>
											<option>Diamond</option>
											<option>Platinum</option>
											<option>Gold</option>
											<option>Silver</option>
											<option>Bronze</option>
											<option>Patron</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
									<div>
										<select class="form-control">
											<option>-- Select --</option>
											<option>President</option>
											<option>Chairman</option>
											<option>Convenor</option>
											<option>Co-Convenor</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="text-right">
						<input class="btn btn-primary" type="submit" name="submit" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script type="text/javascript">
	$(document).ready(function () {
	    $(btn).click(function () {
	        $(add_subtype_n_designation).show();
	    });
	});
</script>

<?php include 'footer.php';?>