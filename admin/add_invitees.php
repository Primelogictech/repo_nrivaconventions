<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Add Invitees</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="invitees.php" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Invitees Type<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<select class="form-control">
								<option>-- Select --</option>
								<option>Dignitaries</option>
								<option>Entertainment</option>
								<option>Speakers</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Invitees Name<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<input type="text" class="form-control" name="" placeholder="Invitees Name">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Designation<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<select class="form-control">
								<option>-- Select --</option>
								<option>President</option>
								<option>Chairman</option>
								<option>Convenor</option>
								<option>Co-conevnor</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Description<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<textarea name="editor1"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Upload Photo<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<input type="file" class="form-control" name="" >
						</div>
					</div>
					<div class="text-right">
						<input class="btn btn-primary" type="submit" name="submit" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<script>
	CKEDITOR.replace( 'editor1' );
</script>

<?php include 'footer.php';?>