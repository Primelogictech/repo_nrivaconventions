<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Convention NRIVA</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
		
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!-- Custom CSS -->
        <link rel="stylesheet" href="assets/css/custom.css">

        <link rel="stylesheet" href="assets/css/custom1.css">

        <!-- Font Family -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css" />

    </head>
    <style type="text/css">
    	.users-icon{
    		border-color: #00d0f1 !important;
		    border: 3px solid;
		    border-radius: 100%;
		    width: 60px;
    	}
    	.sellers-icon{
    		border-color: #7bb13c !important;
		    border: 3px solid;
		    border-radius: 100%;
		    width: 60px;
    	}	
    	.properties-icon{ 
    		border-color: #e84646 !important;
		    border: 3px solid;
		    border-radius: 100%;
		    width: 60px;
    	}
    	.service-seller-icon{
    		border-color: #ffbc34 !important;
		    border: 3px solid;
		    border-radius: 100%;
		    width: 60px;
    	}	
    	.service-list-icon{
    		border-color: #007bff !important;
		    border: 3px solid;
		    border-radius: 100%;
		    width: 60px;
    	}	
    </style>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
		
			<!-- Header -->
            <div class="header">
			
				<!-- Logo -->
                <div class="header-left">
                    <a href="index.php" class="logo">
						<!-- <img src="assets/img/logo.png" alt="Logo"> -->
						<h5 class="font-weight-bold mb-0 lh60">NRIVA</h5>
					</a> 
					<a href="index.php" class="logo logo-small">
						<!-- <img src="assets/img/logo-small.png" alt="Logo" width="30" height="30"> -->
						<h6 class="font-weight-bold mb-0 lh60">NRIVA</h6>
					</a>
                </div>
				<!-- /Logo -->
				
				<a href="javascript:void(0);" id="toggle_btn">
					<i class="fe fe-text-align-left"></i>
				</a>
				
				<!-- <div class="top-nav-search">
					<form>
						<input type="text" class="form-control" placeholder="Search here">
						<button class="btn" type="submit"><i class="fa fa-search"></i></button>
					</form>
				</div> -->
				
				<!-- Mobile Menu Toggle -->
				<a class="mobile_btn" id="mobile_btn">
					<i class="fa fa-bars"></i>
				</a>
				<!-- /Mobile Menu Toggle -->
				
				<!-- Header Right Menu -->
				<ul class="nav user-menu">

					<!-- Notifications -->
					<li class="nav-item dropdown noti-dropdown">
						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
							<i class="fe fe-bell"></i> <span class="badge badge-pill">3</span>
						</a>
						<div class="dropdown-menu notifications">
							<div class="topnav-dropdown-header">
								<span class="notification-title text-center">No Notifications</span>
							</div>
						
						</div>
					</li>
					<!-- /Notifications -->
					
					<!-- User Menu -->
					<li class="nav-item dropdown has-arrow">
						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
							<span class="user-img"><img class="rounded-circle" src="assets/img/profiles/avatar-01.jpg" width="31" alt="Ryan Taylor"></span>
						</a>
						<div class="dropdown-menu">
							<div class="user-header">
								<div class="avatar avatar-sm">
									<img src="assets/img/profiles/avatar-01.jpg" alt="User Image" class="avatar-img rounded-circle">
								</div>
								<div class="user-text my-auto">
									<h6>Admin</h6>
									<!-- <p class="text-muted mb-0">Administrator</p> -->
								</div>
							</div>
							<a class="dropdown-item" href="profile.php">My Profile</a>
							<a class="dropdown-item" href="settings.php">Settings</a>
							<a class="dropdown-item" href="login.php">Logout</a>
						</div>
					</li>
					<!-- /User Menu -->
					
				</ul>
				<!-- /Header Right Menu -->
				
            </div>
			<!-- /Header -->
			
			<!-- Sidebar -->
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
					<div id="sidebar-menu" class="sidebar-menu">
						<ul>
							<li class="active"> 
								<a href="index.php"><i class="fe fe-home"></i> <span>Dashboard</span></a>
							</li>
							<li class=""> 
								<a href="registrations.php"><i class="fe fe-document"></i> <span>Registrations</span></a>
							</li>
							<li class=""> 
								<a href="exhibit_registrations.php"><i class="fe fe-document"></i> <span>Exhibit Registrations</span></a>
							</li>
							<li class="submenu">
								<a href="#"><i class="fe fe-document"></i> <span> Home Page </span> <span class="menu-arrow"></span></a>
								<ul style="display: none;">
									<li><a href="upload_banners.php">Upload Banners</a></li>
									<li><a href="venue_date_n_time.php">Change Venue Date & Time</a></li>
									<li><a href="messages.php">Add Messages</a></li>
									<li><a href="leadership.php">Pillars Of Our Success Leadership</a></li>
									<li><a href="programs.php">Programs</a></li>
									<li><a href="invitees.php">Invitees</a></li>
									<li><a href="donors.php">Donors</a></li>
									<li><a href="events_n_schedule.php">Events & Schedule</a></li>
									<li><a href="youtube_videos.php">Youtube Videos</a></li>
								</ul>
							</li>
							<li class="submenu">
								<a href="#"><i class="fe fe-document"></i> <span> Content Mangement </span> <span class="menu-arrow"></span></a>
								<ul style="display: none;">
									<li><a href="menu_management.php">Menu Mangement</a></li>
									<li><a href="menu_items_management.php">Menu Items Mangement</a></li>
									<li><a href="pages.php">Manage Pages</a></li>
								</ul>
							</li>
							<li> 
								<a href="manage_logistics.php"><i class="fe fe-home"></i> <span>Manage Logistics</span></a>
							</li>
							<li class="submenu">
								<a href="#"><i class="fe fe-document"></i> <span> Master </span> <span class="menu-arrow"></span></a>
								<ul style="display: none;">
									<li><a href="manage_sponsor_category.php">Manage Sponsor Category</a></li>
									<li><a href="sponsor_category_type.php">Sponsor Category Type</a></li>
									<li><a href="upload_right_side_logo.php">Upload Right Side Logo</a></li>
									<li><a href="upload_left_side_logo.php">Upload Left Side Logo</a></li>
									<li><a href="leadership_type.php">Leadership Type</a></li>
									<li><a href="invitees_type.php">Invitees Type</a></li>
									<li><a href="donor_type.php">Donor Type</a></li>
									<li><a href="designation_type.php">Designation Type</a></li>
									<li><a href="exhibit_type.php">Exhibit Type</a></li>
								</ul>
							</li>
						</ul>
					</div>
                </div>
            </div>
			<!-- /Sidebar -->

			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

