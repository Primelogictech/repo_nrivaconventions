<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Add Menu Item</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="menu_items_management.php" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Select Menu<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<select class="form-control">
								<option>-- Select --</option>
								<option>Home</option>
								<option>Registration</option>
								<option>Team</option>
								<option>Speakers</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Menu Item Name<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<input type="text" class="form-control" name="" placeholder="Menu Item Name">
						</div>
					</div>
					<div class="text-right">
						<input class="btn btn-primary" type="submit" name="submit" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php';?>