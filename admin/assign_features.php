<?php include 'header.php';?>

<style type="text/css">
    input.form-control {
        height: 30px;
        width: 300px;
    }
</style>

<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Features</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="registrations.php" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>
                                    <div>Sponsor Category</div>
                                    <div>(Amount)</div>
                                </th>
                                <th style="min-width: 300px;">Benefits</th>
                                <th style="min-width: 500px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <div>Sapphire</div>
                                    <div class="text-orange font-weight-bold pt-2">$25000</div>
                                </td>
                                <td>
                                    <ul class="flower-list mb-0">
                                        <li>1.&nbsp;&nbsp;&nbsp;&nbsp;2 Vendor booth spaces</li>
                                        <li>2.&nbsp;&nbsp;&nbsp;&nbsp;3 Hotel Rooms(3 nights)</li>
                                        <li>3.&nbsp;&nbsp;&nbsp;&nbsp;2 Shathamanam Bhavathi</li>
                                        <li>4.&nbsp;&nbsp;&nbsp;&nbsp;Admission Tickets (20)</li>
                                        <li>
                                            5.&nbsp;&nbsp;&nbsp;&nbsp;Banquet Admission (20)
                                        </li>
                                        <li>6.&nbsp;&nbsp;&nbsp;&nbsp;Kalyanam Tickets (12)</li>
                                        <li>7.&nbsp;&nbsp;&nbsp;&nbsp;Banner on the Website</li>
                                        <li>8.&nbsp;&nbsp;&nbsp;&nbsp;Recognition on main Statge</li>
                                        <li>9.&nbsp;&nbsp;&nbsp;&nbsp;Reserved Seating at the Convention</li>
                                        <li>
                                            10.&nbsp;&nbsp;&nbsp;&nbsp;Full page advertisement in Souvenir
                                        </li>
                                        <li>11.&nbsp;&nbsp;&nbsp;&nbsp;Lunch/Dinner in VIP Area</li>
                                        <li>12.&nbsp;&nbsp;&nbsp;&nbsp;Digital Banner on the Main Stage</li>
                                    </ul>
                                </td>
                                <td>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control" height="32"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control" height="32"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                    <div class="d-flex"> 
                                        <span class="my-1 mr-1"><input type="text" name="" class="form-control"></span>
                                        <a href="#" class="btn btn-sm btn-success my-auto mx-1">Save</a>
                                        <a href="#" class="btn btn-sm btn-success text-white my-auto mx-1">Edit</a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php';?>
