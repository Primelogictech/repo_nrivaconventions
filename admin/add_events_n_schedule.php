<?php include 'header.php';?>

<!-- Page Header -->
<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Venue Date & Time</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="events_n_schedule.php" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
			</div>
		</div>
	</div>
</div>
<!-- /Page Header -->

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<form>
					<div class="form-group row">
						<label class="col-form-label col-md-3 my-auto">Name<span class="mandatory">*</span></label>
						<div class="col-md-9 my-auto">
							<input type="text" class="form-control" name="" placeholder="Name">
						</div>
					</div>
				</form>
				<form action="" method="post">
					<div class="form-group row">
						<label class="col-form-label col-md-3 my-auto">Enter Day<span class="mandatory">*</span></label>
						<div class="col-md-9 my-auto">
							<input type="text" class="form-control" name="" placeholder="Enter Day">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-3 my-auto">Enter Schedules<span class="mandatory">*</span></label>
						<div class="col-md-9 my-auto">
							<textarea name="editor1"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-3 my-auto">Description<span class="mandatory">*</span></label>
						<div class="col-md-9 my-auto">
							<textarea name="editor2"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-3 my-auto">Add More Days<span class="mandatory">*</span></label>
						<div class="col-md-9 my-auto">
							<div class="btn btn-primary cursor-pointer" id="btn"><i class="fa fa-plus"></i></div>
						</div>
					</div>
				</form>
				<form action="" id="add_onemore_day" style="display: none;">
					<div class="form-group row">
						<label class="col-form-label col-md-3 my-auto">Enter Day<span class="mandatory">*</span></label>
						<div class="col-md-9 my-auto">
							<input type="text" class="form-control" name="" placeholder="Enter Day">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-3 my-auto">Enter Schedules<span class="mandatory">*</span></label>
						<div class="col-md-9 my-auto">
							<textarea name="editor3"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-3 my-auto">Description<span class="mandatory">*</span></label>
						<div class="col-md-9 my-auto">
							<textarea name="editor4"></textarea>
						</div>
					</div>
				</form>
				<div class="col-12 text-right">
					<input class="btn btn-primary" type="submit" name="submit" value="Submit">
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<?php include 'footer.php';?>

<script>
	CKEDITOR.replace( 'editor1' );
	CKEDITOR.replace( 'editor2' );
	CKEDITOR.replace( 'editor3' );
	CKEDITOR.replace( 'editor4' );
</script>

<script type="text/javascript">
	$(document).ready(function () {
		$(btn).click(function () {
			$(add_onemore_day).show();

		});
	});
</script>

