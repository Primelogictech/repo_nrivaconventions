<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Manage Sponsor Category</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="add_manage_sponsor_category.php" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-md-12">

		<!-- Recent Orders -->
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>ID</th>
								<th>Sponsor Category Type</th>
								<th>Sponsor Category Name</th>
								<th>Amount</th>
								<th>Benefits</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Donor</td>
								<td>Sapphire</td>
								<td>$ 2,00,000</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">View</a>
								</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Donor</td>
								<td>Diamond</td>
								<td>$ 1,00,000</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">View</a>
								</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Donor</td>
								<td>Platinum</td>
								<td>$ 50,000</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">View</a>
								</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php';?>
