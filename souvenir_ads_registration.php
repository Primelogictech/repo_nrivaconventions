<?php include 'header.php';?>

<style type="text/css">
    .p-radio-btn {
        position: absolute;
        top: 4px;
    }
</style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/souvenir-ads.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <h6 class="font-weight-bold text-center mb-3">Convention Registration is a must for any of the Event Registration.</h6>
                                <h6 class="font-weight-bold text-center mb-3">Please make sure that you have registered for the Convention before you register for this Event.</h6>
                                <div class="row">
                                    <div class="col-12 col-lg-10 offset-lg-1 mt-3">
                                        <div class="shadow-small py-5 px-3 px-md-4">
                                            <form>
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Full Name:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Full Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Company Name:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Company Name" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Email:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Email" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Phone No:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="number" name="" id="" class="form-control" placeholder="Phone No" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Name of your Website:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Name of your Website" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Address:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="number" name="" id="" class="form-control" placeholder="Address" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                                        <label>Referred By:</label><span class="text-red">*</span>
                                                        <div>
                                                            <input type="text" name="" id="" class="form-control" placeholder="Referred By" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <h5 class="mt-2 mb-3 text-violet">Sponsor Type:</h5>
                                            <h6 class="position-relative font-weight-bold mb-3">
                                                <input type="radio" class="p-radio-btn" name="sponsor type" /><span class="pl-4">Front Cover Page Inside Full Page:</span><span class="text-danger"> $5000</span>
                                            </h6>
                                            <h6 class="position-relative font-weight-bold mb-3">
                                                <input type="radio" class="p-radio-btn" name="sponsor type" /><span class="pl-4">Back Cover Page Inside Full Page:</span><span class="text-danger"> $3000</span>
                                            </h6>
                                            <h6 class="position-relative font-weight-bold mb-3">
                                                <input type="radio" class="p-radio-btn" name="sponsor type" /><span class="pl-4">Full Page Souvenir Ad:</span><span class="text-danger"> $2000</span>
                                            </h6>
                                            <h6 class="position-relative font-weight-bold mb-3">
                                                <input type="radio" class="p-radio-btn" name="sponsor type" /><span class="pl-4">Half Page Souvenir Ad:</span><span class="text-danger"> $1000</span>
                                            </h6>
                                            <h6 class="position-relative font-weight-bold mb-3">
                                                <input type="radio" class="p-radio-btn" name="sponsor type" /><span class="pl-4">Quarter Page Souvenir Ad:</span><span class="text-danger"> $500</span>
                                            </h6>
                                            <h6 class="position-relative font-weight-bold mb-3"><input type="radio" class="p-radio-btn" name="sponsor type" /><span class="pl-4">Other:</span><span class="text-danger"> $0</span></h6>
                                            <div class="row px-3">
                                                <div class="col-12 col-sm-10 col-md-6 col-lg-5 shadow-small py-4">
                                                    <h6 class="text-center">Upload Photo</h6>
                                                    <div class="browse-button py-2">
                                                        <input type="file" class="form-control browse" name="" />
                                                    </div>
                                                    <div class="pt-3 text-center fs15">( File Size could not be more than 5MB )</div>
                                                </div>
                                            </div>
                                            <div class="row pt-3">
                                                <div class="col-12 col-md-12 col-lg-6 my-1">
                                                    <div class="border-green-2 border-radius-5 bg-white">
                                                        <h5 class="text-violet py-3 text-center">Payment by using PayPal</h5>
                                                        <img src="images/paypal.jpg" class="img-fluid mx-auto d-block" alt="" />
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-12 col-lg-6 my-1">
                                                    <div class="border-green-2 border-radius-5 bg-white py55">
                                                        <h5 class="text-center">Enter your Exhibit Amount</h5>
                                                        <div class="mx-auto" style="max-width: 250px;">
                                                            <div class="border-0 bg-light border-radius-3 p10 mt10">
                                                                <div class="row">
                                                                    <div class="col-2 p10 my-auto">
                                                                        <div class="border-right">
                                                                            <h4 class="text-orange text-center mb-0">$</h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-7 my-auto px-0">
                                                                        <input type="text" class="p-0 text-right box-shadow-none form-control donatein border-0 bg-light" name="" value="1000" />
                                                                    </div>
                                                                    <div class="col-3 my-auto px-0">
                                                                        <div class="text-orange text-left fs28">.00</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-md-8 col-lg-7 pt-2">
                                                    <h5>Security Code</h5>
                                                    <div class="row">
                                                        <div class="col-11">
                                                            <div class="registration-captcha-block pr-3">
                                                                <div class="row">
                                                                    <div class="col-8 px-0 my-auto">
                                                                        <input type="text" class="border-0 form-control bg-transparent" name="" placeholder="Enter the Characters you see" />
                                                                    </div>
                                                                    <div class="col-4 px-0 my-auto pr5">
                                                                        <img src="images/ShowCaptchaImage.png" class="captcha-img float-md-right" alt="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-1 px-1 my-auto">
                                                            <div>
                                                                <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-4 col-lg-5 mt40">
                                                    <div class="text-center text-sm-right">
                                                        <div class="btn btn-lg btn-danger text-uppercase px-3 px-lg-5 fs-xs-15 fs-md-18">PROCEED TO PAY</div>
                                                    </div>
                                                </div>
                                                <div class="col-12 pt-4">
                                                    <h5 class="text-violet">Payments can also made by Money Order or a Cashier’s Check payable to NRIVA, Inc</h5>
                                                    <div class="fs16 mb-1">Cheque Mailing Address:-</div>
                                                    <div class="fs16 mb-1">NRIVA 5th Global Convention</div>
                                                    <div class="fs16 mb-1">24328 Amanda Ln,</div>
                                                    <div class="fs16 mb-1">Novi, MI 48375.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
