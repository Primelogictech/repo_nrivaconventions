<?php include 'header1.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
        	<div class="col-12">
        		<h5>My Dashboard</h5>
        	</div>
        	<div class="col-12">
        		<div class="card-body px-0">
                    <div class="table-responsive">
                        <table class="datatable table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
									<th>Member ID</th>
									<th>Member Details</th>
									<th>Sponsorship Type</th>
									<th>Sponsorship Category</th>
									<th>Payment Details</th>
									<th>Action</th>
								</tr>
                            </thead>
                            <tbody>
                                <tr>
									<td>12345</td>
									<td>
										<div class="py-1">
											<span>Name:</span><span> Arun</span>
										</div>
										<div class="py-1">
											<span>Email:</span><span> arun@gmail.com</span>
										</div>
										<div class="py-1">
											<span>Mobile:</span><span> 9052304477</span>
										</div>
									</td>
									<td>Donor</td>
									<td>Bronze ($3000)</td>
									<td>
										<div>Paid Amount : $1500</div>
										<div>Due : $1500</div>
										<div>Partial Payment</div>
									</td>
									<td>
										<div>
											<a href="registration.php" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
											<a href="view_features.php" class="btn btn-sm btn-success text-white my-1 mx-1">View Features</a>
										</div>
										<div>
											
										</div>
										<div>
											<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Pay Pending Amount</a>
										</div>
										<div>
											<a href="registration.php" class="btn btn-sm btn-success text-white my-1 mx-1">Upgrade</a>
										</div>
										<div>
											<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">View / Print Ticket</a>
										</div>
									</td>
								</tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>