<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 py-2 ">
            	<div class="shadow-small border-radius-5 p-4">
            		<h4 class="text-center">Login</h4>
            		<div class="text-center fs16">Please use your NRIVA login details</div>
            		<div class="col-12 pt-3">
            			<div class="form-group">
	                        <label>Email:</label><span class="text-red">*</span>
	                        <div>
	                            <input type="text" name="" id="" class="form-control" placeholder="Enter Your Email" />
	                        </div>
	                    </div>
            		</div>
            		<div class="col-12">
            			<div class="form-group">
	                        <label>Password</label><span class="text-red">*</span>
	                        <div>
	                            <input type="text" name="" id="" class="form-control" placeholder="Password" />
	                        </div>
	                    </div>
            		</div>
            		<div class="col-12">
            			<div>
	                        <input type="checkbox" class="input-checkbox" name=""><span class="pl25 fs16">Keep me logged in</span>
	                    </div>
            		</div>
            		<div class="col-12 pt-3">
            			<div>
            				<a href="#" class="btn btn-violet w-100 border-radius-5 signin-btn">Login</a>
            			</div>
            			<!-- <div class="text-center pt-3 fs16">Don't have an account ? <a href="#" class="text-violet font-weight-bold">Sign Up</a></div>
            			<div class="fs16 text-center py-2">
            				<a href="#" class="text-violet font-weight-bold">Forgot Password ?</a>
            			</div> -->
            		</div>
            	</div>
        	</div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>