<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/stocks-investments-session.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 py-2">
                                <h5 class="text-violet">Overview</h5>
                                <p>A 2-hour information loaded stocks&investment session with presentations from market experts followed by open panel discussion and Q&A.</p>
                            </div>
                            <div class="col-12 pl30 pr30">
                                <h5 class="text-danger text-center">SPEAKERS</h5>
                                <div class="row py-2">
                                    <div class="col-12 p3 border shadow-small">
                                        <div class="bg-light-violet p10">
                                            <div class="row">
                                                <div class="col-12 col-md-4 col-lg-4 my-auto">
                                                    <h6 class="mb-0 py-1">1)&nbsp;&nbsp;Rajendra Prasad</h6>
                                                </div>
                                                <div class="col-12 col-md-4 col-lg-4 my-auto">
                                                    <h6 class="mb-0 py-1">2)&nbsp;&nbsp;Vamshi Yelakanti</h6>
                                                </div>
                                                <div class="col-12 col-md-4 col-lg-4 my-auto">
                                                    <h6 class="mb-0 py-1">3)&nbsp;&nbsp;Jaya Krishna Devaki</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pl30 pr30">
                                <h5 class="text-danger text-center">AGENDA</h5>
                                <div class="row py-2">
                                    <div class="col-12 p3 border shadow-small">
                                        <div class="bg-light-violet p10">
                                            <div class="text-center fs18 mb-1">RAJENDRA PRASAD PRESENTATION</div>
                                            <p class="text-center fs16"><strong>TOPIC :</strong> Wallstreet Strategies</p>
                                            <div class="row">
                                                <div class="col-12 col-md-12 col-lg-6 px-0 px-md-3">
                                                    <div class="text-center fs18 mb-1 text-violet">VAMSHI’S PRESENTATION</div>
                                                    <ul class="stock-investment-list list-unstyled pl20 pr10 pt10 fs16">
                                                        <li>Why stocks fall immediately after we investors buy stocks.</li>
                                                        <li>How algorithms work and how hedge funds use them.</li>
                                                        <li>How manipulation occurs even with strict rules from FINRA.</li>
                                                        <li>How to buy stocks to get the best price.</li>
                                                        <li>How to earn interest in your brokerage account.</li>
                                                        <li>How to earn royalty on your stock you are holding for longterm.</li>
                                                    </ul>
                                                </div>
                                                <div class="col-12 col-md-12 col-lg-6 px-0 px-md-3">
                                                    <div class="text-center fs18 mb-1 text-violet">JAYA KRISHNA PRESENTATION</div>
                                                    <ul class="stock-investment-list list-unstyled pl20 pr10 pt10 fs16">
                                                        <li>How to be consistently profitable in trading or investments?</li>
                                                        <li>How to manage risk?</li>
                                                        <li>How do I manage my portfolio by hedging?</li>
                                                        <li>Investment and trading styles?</li>
                                                        <li>Income generating strategies on stocks and options?</li>
                                                        <li>I need more help and someone to review my portfolio, risk profile. Who can help?</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 py-2">
                                <h5 class="text-violet">About Speakers:</h5>
                                <div class="text-skyblue fs16">RAJENDRA PRASAD:</div>
                                <p>
                                    Mr. Market is his pen name. In 1992 he became a registered investment advisor. He published a leading market timing newsletter for investing in mutual funds. He was interviewed numerous times on
                                    television. He has been quoted in Investors Business Daily and in magazines. He has published two books on stock market successfully. He is a retired associate professor of medicine. He has donated a lot
                                    to charity.
                                </p>
                                <div class="text-skyblue fs16">Vamshi Yelakanti</div>
                                <p>
                                    A NRIVA DC resident, with 20+ years of experience as investor, trader, and advisor in stock market investments. During his collage days in India, he helped his family business and parents at Hyderabad
                                    stock exchange and National Stock exchange (NSE). He underwent training at Bombay stock exchange and was a professional trader and market maker for more than 3 years before moving to USA. Currently he
                                    advises multiple investor groups on a daily basis with main goal of not to loose money in stock market.
                                </p>
                                <div class="text-skyblue fs16">JayaKrishna Devaki</div>
                                <p>
                                    A resident of NRIVA Tampa and a IIT Madras grad, has 18 years of experience in Stocks, Options, Futures, Commodities, and Forex trading. A certified Investment advisor and Fund manager, worked for
                                    Barclays earlier in their investment division.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
