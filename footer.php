<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-6">
                <div class="row registration-block">
                    <div class="col-12">
                        <h5 class="pb-3">REGISTER</h5>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#"><span>Convention</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Cultural</span></a>
                            </li>
                            <li>
                                <a href="#"><span>CME</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Leadership Appreciation Dinner</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Matrimony</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#"><span>Awards</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Vassavites Got Talent</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Women Conference</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Lakshmi Narasimha Swami Kalyanam</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#"><span>Exhibits</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Business Conference</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Shathamanam Bhavathi</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Youth Activities</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-6">
                <div class="row leadership-block">
                    <div class="col-12 col-sm-6 col-md-6">
                        <h5 class="pb-3">LEADERSHIP</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#"><span>Convention Leadership</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Convention Committee</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Executive Committee</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Board Of Directors</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6">
                        <h5 class="pb-3">CONVENTION</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#"><span>About NRIVA</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Our Donors</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Contact Us</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Flyers</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Souvenir Sponsorship Package</span></a>
                            </li>
                            <li>
                                <a href="#"><span>Exhibits Package Details</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </footer>
    <section class="copy-right-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-10 col-md-8 col-lg-6 my-2 my-sm-auto">
                <div>
                    &copy; 2021 NRI Vasavi Association, Inc.. All rights reserved.
                </div>
            </div>
            <div class="col-12 col-sm-2 col-md-4 col-lg-6 my-2 my-sm-auto">
                <div class="float-center float-sm-right">
                    <ul class="social-media-icons mb-0">
                        <li>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-youtube"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </section>

    <!-- All scripts -->

    <!-- jQuery library -->

    <script src="js/jquery.js"></script>

    <!-- Bootstrap JavaScript -->

    <script src="js/bootstrapjs.js"></script>

    <!--Popups js-->

    <script src="js/popperjs.js"></script>

    <!--Datatables js-->

    <script src="js/datatables.js"></script>

    <!-- Extra js -->

    <script src="js/extrajquery.js"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script type="text/javascript">
        // Sticky navbar
        // =========================
        $(document).ready(function () {
            // Custom function which toggles between sticky class (is-sticky)
            var stickyToggle = function (sticky, stickyWrapper, scrollElement) {
                var stickyHeight = sticky.outerHeight();
                var stickyTop = stickyWrapper.offset().top;
                if (scrollElement.scrollTop() >= stickyTop) {
                    stickyWrapper.height(stickyHeight);
                    sticky.addClass("is-sticky");
                } else {
                    sticky.removeClass("is-sticky");
                    stickyWrapper.height("auto");
                }
            };

            // Find all data-toggle="sticky-onscroll" elements
            $('[data-toggle="sticky-onscroll"]').each(function () {
                var sticky = $(this);
                var stickyWrapper = $("<div>").addClass("sticky-wrapper"); // insert hidden element to maintain actual top offset on page
                sticky.before(stickyWrapper);
                sticky.addClass("sticky");

                // Scroll & resize events
                $(window).on("scroll.sticky-onscroll resize.sticky-onscroll", function () {
                    stickyToggle(sticky, stickyWrapper, $(this));
                });

                // On page load
                stickyToggle(sticky, stickyWrapper, $(window));
            });
        });
    </script>
    
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
    </script>

    <script>
        AOS.init({
            duration: 2000,
        });
    </script>

    </body>
</html>
