<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home Page</title>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- Bootstrap CSS -->

        <link href="css/bootstrapcss.css" rel="stylesheet" />

        <!-- Fontawesome icons -->

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css" />

        <!-- Animations -->

        <link href="css/Animations.css" rel="stylesheet" />

        <!-- Font family -->

        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet" />

        <!--Datatables CSS-->

        <link href="css/datatables.css" rel="stylesheet" />

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />

        <!--Custom CSS-->

        <link href="css/custom.css" rel="stylesheet" />

        <link href="css/nriva.css" rel="stylesheet" />

        <link href="css/sidebar.css" rel="stylesheet" />
        
    </head>

    <body>
        <!-- Header -->

        <header class="header-bg">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 px-0 d-none d-lg-block">
                        <article class="text-center py12">
                            <a href="index.php">
                                <img src="images/logo.png" alt="" border="0" class="img-fluid" />
                            </a>
                        </article>
                    </div>
                    <div class="col-12 col-lg-8 pl-lg-0">
                        <div class="row pt-2 pb-2 pb-lg-4 pr-lg-3">
                            <div class="col-12 col-lg-8 pr-lg-0 pb-lg-2">
                                <div class="d-lg-flex text-white float-lg-right">
                                    <div class="my-auto d-none d-lg-block">
                                        <img src="images/location-icon1.png" class="my-auto pr-3" alt="" />
                                    </div>
                                    <div class="my-auto">
                                        <label class="mb-0 text-white fs17 d-none d-lg-block">Location</label>
                                        <marquee class="text-white text-center text-lg-left fs17">July 1st to 3rd, 2022 Renaissance Schaumburg Convention Center Hotel, 1551 Thoreau Dr N, Schaumburg, IL 60173</marquee>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 d-none d-lg-block">
                                <div class="d-flex text-white float-right">
                                    <div class="my-auto">
                                        <img src="images/mobile-icon1.png" class="my-auto pr-3" alt="" />
                                    </div>
                                    <div class="my-auto">
                                        <label class="mb-0 text-left text-white fs17">Phone</label>
                                        <div class="text-white fs15">1-855-WE NRIVA</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1 px-lg-0 my-auto pt10 d-none d-lg-block">
                                <ul class="social-media-icons">
                                    <li>
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <nav class="navbar navbar-expand-lg navbar-light p-0 justify-content-center d-none d-lg-block">

                            <!-- Navbar links -->

                            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                                <ul class="navbar-nav">
                                    <li class="nav-item active">
                                        <a class="nav-link lh20" href="index.php">
                                            <img src="images/home.png" alt="" />
                                        </a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Registration
                                        </a>
                                        <div class="dropdown-menu submenu l-menu">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="awards_registrations.php" class="">Awards</a>
                                                        </li>
                                                        <li>
                                                            <a href="cme_details.php" class="">CME</a>
                                                        </li>
                                                        <li>
                                                            <a href="exhibits_reservation.php" class="">Exhibits</a>
                                                        </li>
                                                        <li>
                                                            <a href="shathamanam_bhavathi_registration.php" class="">Shathamanam Bhavathi</a>
                                                        </li>
                                                        <li>
                                                            <a href="youth_activities_registration.php" class="">Youth Activities</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="business_conference_details.php" class="">Business Conference</a>
                                                        </li>
                                                        <li>
                                                            <a href="convention_details.php" class="">Convention</a>
                                                        </li>
                                                        <li>
                                                            <a href="matrimony_registration.php" class="">Matrimony</a>
                                                        </li>
                                                        <li>
                                                            <a href="v_got_talent.php" class="">Vasavites Got Talent</a>
                                                        </li>
                                                        <li>
                                                            <a href="lakshmi_narasimha_swami_kalyanam_details.php" class="">Lakshmi Narasimha Swami Kalyanam</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="business_plan_competition.php" class="">Business Plan Competition</a>
                                                        </li>
                                                        <li>
                                                            <a href="cultural_registrations.php" class="">Cultural</a>
                                                        </li>
                                                        <li>
                                                            <a href="miss_nriva_mrs_nriva_details.php" class="">Miss. NRIVA &amp; Mrs. NRIVA</a>
                                                        </li>
                                                        <li>
                                                            <a href="women's_conference_details.php" class="">Women's Conference</a>
                                                        </li>
                                                        <li>
                                                            <a href="souvenir_ads_registration.php" class="">Souvenir Registrations</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Team
                                        </a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="convention_leadership_committee.php">Convention Leadership</a>
                                                        </li>
                                                        <li>
                                                            <a href="committee_categories_list.php"> Convention Committees</a>
                                                        </li>
                                                        <li>
                                                            <a href="executive_committee.php">Executive Committee</a>
                                                        </li>
                                                        <li>
                                                            <a href="board_of_directors_committee.php"> Board Of Directors</a>
                                                        </li>
                                                        <li>
                                                            <a href="founding_members_committee.php"> Founding Members</a>
                                                        </li>
                                                        <li>
                                                            <a href="advisory_council_committee.php"> Advisory Council</a>
                                                        </li>
                                                        <li>
                                                            <a href="international_directors_committee.php"> International Directors</a>
                                                        </li>
                                                        <li>
                                                            <a href="presidents_circle_committee.php"> Presidents Circle</a>
                                                        </li>
                                                        <li>
                                                            <a href="our_convention_convener's_corner_committee.php"> Our Convention Convener's Corner</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Speakers
                                        </a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="business_speakers_details.php">Business Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="youth_conference_speakers_details.php">Youth Conference Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="stock_investments_speakers_details.php">Stock &amp; Investments Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="immigration_forum_speakers_details.php">Immigration Forum Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="women's_speakers_details.php">Women's Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="real_estate_details.php">Real Estate</a>
                                                        </li>
                                                        <li>
                                                            <a href="spiritual_speaker_details.php">Spiritual Speaker</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Programs
                                        </a>
                                        <div class="dropdown-menu submenu l-menu">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="banquet_night.php" class="">Banquet Night</a>
                                                        </li>
                                                        <li>
                                                            <a href="cme_details.php" class="">CME</a>
                                                        </li>
                                                        <li>
                                                            <a href="live_musical_concert_details.php" class="">Live Musical Concert</a>
                                                        </li>
                                                        <li>
                                                            <a href="real_estate_sessions.php" class="">Real Estate Sessions</a>
                                                        </li>
                                                        <li>
                                                            <a href="women's_conference_details.php" class="">Women Conference</a>
                                                        </li>
                                                        <li>
                                                            <a href="yoga_n_meditation.php" class="">Yoga &amp; Meditation</a>
                                                        </li>
                                                        <li>
                                                            <a href="stocks_investments_session_details.php" class="">Stocks & Investments Session</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="business_conference_details.php" class="">Business Conference</a>
                                                        </li>
                                                        <li>
                                                            <a href="cultural_registrations.php" class="">Cultural</a>
                                                        </li>
                                                        <li>
                                                            <a href="matrimony_registration.php" class="">Matrimony</a>
                                                        </li>
                                                        <li>
                                                            <a href="shathamanam_bhavathi_registration.php" class="">Shathamanam Bhavathi</a>
                                                        </li>
                                                        <li>
                                                            <a href="youth_activities_registration.php" class="">Youth Activities</a>
                                                        </li>
                                                        <li>
                                                            <a href="lakshmi_narasimha_swami_kalyanam_details.php" class="">Lakshmi Narasimha Swami Kalyanam</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="business_plan_competition.php" class="">Business Plan Competition</a>
                                                        </li>
                                                        <li>
                                                            <a href="immigration_sessions.php" class="">Immigration Sessions</a>
                                                        </li>
                                                        <li>
                                                            <a href="miss_nriva_mrs_nriva_details.php" class="">Miss. NRIVA &amp; Mrs. NRIVA</a>
                                                        </li>
                                                        <li>
                                                            <a href="v_got_talent.php" class="">V Got Talent</a>
                                                        </li>
                                                        <li>
                                                            <a href="youth_banquet_details.php" class="">Youth Banquet</a>
                                                        </li>
                                                        <li>
                                                            <a href="nriva_core_team_appreciation_night_details.php" class="">NRIVA Core Team Appreciation Night</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="exhibits_reservation.php">Exhibits</a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="convention_schedule_details.php">
                                            Schedule
                                        </a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Donors
                                        </a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="donors.php#sapphire">Sapphire</a> 
                                                        </li>
                                                        <li>
                                                            <a href="donors.php#diamond">Diamond</a>
                                                        </li>
                                                        <li>
                                                            <a href="donors.php#platinum">Platinum</a>
                                                        </li>
                                                        <li>
                                                            <a href="donors.php#gold">Gold</a>
                                                        </li>
                                                        <li>
                                                            <a href="donors.php#silver">Silver</a>
                                                        </li>
                                                        <li>
                                                            <a href="donors.php#bronze">Bronze</a>
                                                        </li>
                                                        <li>
                                                            <a href="donors.php#patron">Patron</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Logistics
                                        </a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="hotels_details.php">Hotels</a>
                                                        </li>
                                                        <li>
                                                            <a href="venue_details.php">Venue</a>
                                                        </li>
                                                        <li>
                                                            <a href="attractions_details.php">Attractions</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item active">
                                        <a class="nav-link lh25 text-white" href="signin.php">
                                            Login
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="col-lg-2 px-0 d-none d-lg-block">
                        <article class="tabhorizontal-hide text-center py15 r-p8">
                            <a href="index.php">
                                <img src="images/right-logo1.png" width="115" height="117" border="0" alt="" />
                            </a>
                        </article>
                    </div>
                </div>
            </div>
        </header>

        <nav class="navbar-dark d-block d-lg-none">
            <div class="navbar">
                <!-- Brand -->

                <a class="navbar-brand py-0" href="index.php">
                    <img src="images/logo.png" alt="" border="0" width="80" class="img-fluid" />
                </a>

                <div class="nav-item">
                    <a class="nav-link px-1" href="#">
                        <span class="text-dark fs40 font-weight-bold" onclick="openNav()"><i class='fas fs30'>&#xf039;</i></span>
                    </a>
                </div>
            </div>
        </nav>

        <div id="mySidenav" class="sidenav d-block d-lg-none">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <ul class="navbar-nav">
                <li class="sidemenu-nav-item sidemenu-active">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Home</span>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Registration</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                    <ul class="submenu d-none">
                        <li><a href="#">Awards</a></li>
                        <li><a href="#">Business Conference</a></li>
                        <li><a href="#">Business Plan Competition</a></li>
                        <li><a href="#">CME</a></li>
                        <li><a href="#">Convention</a></li>
                        <li><a href="#">Cultural</a></li>
                        <li><a href="#"> Exhibits</a></li>              
                        <li><a href="#">Matrimony</a></li>
                        <li><a href="#">Miss. NRIVA &amp; Mrs. NRIVA</a></li>
                        <li><a href="#">Shathamanam Bhavathi</a></li>
                        <li><a href="#">Vasavites Got Talent</a></li>
                        <li><a href="#">Women's Conference</a></li>
                        <li><a href="#">Youth Activities</a></li>
                        <li><a href="#">Lakshmi Narasimha Swami Kalyanam</a></li>
                        <li><a href="#">Souvenir Registrations</a></li>
                    </ul>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Team</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Speakers</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                </li>
                <li class="sidemenu-nav-item">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Exhibits</span>
                    </a>
                </li>
                <li class="sidemenu-nav-item">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Schedule</span>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Donors</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Logistics</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                </li>
            </ul>
        </div>