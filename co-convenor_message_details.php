<?php include 'header.php';?>
	
<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small px-sm-30 py-4 p-md-4 p40">
            	<div class="text-center float-sm-none float-md-right px-2">
            		<img src="images/user.png" class="" width="160" alt="Nagender_Aytha" />
            		<div class="py-2">
            			<div class="text-danger fs16 text-center">Co-Convenor</div>
            			<div class="text-danger fs12 text-center">- Co-Convenor</div>
            		</div>
            	</div>
                <h4 class="mb-3 text-violet">Dear Families,</h5>
                <p>
					I could not be more thankful to be a part of such a wonderful organization and serve as Co-Convener for NRIVA 5th Global Convention. 
				</p>
				<p>
					From the inauguration of this wonderful establishment, NRIVA has operated on the core ideals of service and compassion. Witnessing the beautiful accomplishments our members have made since the start has moved me beyond words. Seeing Vasavites from around the world come together in pursuit of such an influential mission, to help others, is truly inspiring. 
				</p>
				<p>
					“The best way to find yourself is to lose yourself in the service of others.” Mahatma Gandhi. 
				</p>
				<p>
					The overwhelming amount of underprivileged present in our society today necessitates generosity and compassion from others. I strongly believe that NRIVA showcases this kindness and willingness to serve in all of its actions, volunteer work, and more — especially seen in the global conventions. Such an organization has bestowed us with not only the ability to give back to the global community but to also connect with fellow Vasavites. Alongside the blessed opportunities to serve and connect, NRIVA celebrates the Indian culture and traditions that resonate within us all.  
				</p>
				<p>
					I’m very excited for the upcoming convention and know that it will be a great success. Thank you to all the Donors, Volunteers and Teams for working so hard to create a great convention for us all. Looking forward to seeing everyone there!
				</p>
				<div>
					<div class="text-violet font-weight-bold fs16">Thank you all so much,</div>
					<div class="text-black fs16">Co-Convenor</div>
					<div class="text-orange fs16">Co-Convenor, NRIVA</div>
					<div class="text-orange fs16">NRIVA 5th Global Convention</div>
					<div class="fs16 py-3">Visit <a href="#" class="text-orange">convention.nriva.org</a> for updates and further details</div>
				</div>
				<div class="text-violet fs15 font-weight-bold">
					Why wait? Go “Destination Detroit!”
				</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>