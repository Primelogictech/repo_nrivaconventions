<?php include 'header.php';?>
	
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small">
        <div class="row">
            <div class="col-12 px-0">
                <div class="leadership-heading-bg p-3">
                    <h4 class="mb-0">Presidents Circle Committee</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 py-4 p-md-4 p40">
            	<div class="row">
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 pt-3">
		            				<h6 class="mb-0">Anand Garlapati</h6>
		            				<div class="pt-1">2007 - 2012</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Ramesh Kalwala</h6>
		            				<div class="pt-1">2013 - 2015</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">ramxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">571-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">NAGENDRA GUPTA VELOOR</h6>
		            				<div class="pt-1">2016 - 2017</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">vngxxxxxxxxxx@yahoo.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">917-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Nagender Aytha</h6>
		            				<div class="pt-1">2018 - 2019</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">nagxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">515-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>