<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/miss-nriva-mrs-nriva.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="text-center text-md-right mb-3">
                            <a href="documents/Miss-and-Mrs-NRIVA-WAIVER-FORM.pdf" class="btn btn-danger btn-lg mr-0 mr-md-2 my-1" target="_blank">WAIVER FORM</a>
                            <a href="chapter_level_coordinators_guidelines_details.php" class="btn btn-warning btn-lg text-white fs-xs-15 fs-xss-12 my-1">Chapter Level Coordinators Guidelines</a>
                        </div>

                        <div class="row mx-md-1">
                            <div class="col-12 col-md-6 my-1 px-0 pl-md-0 pr-md-3">
                                <div>
                                    <img src="images/miss-and-mrs.png" class="img-fluid w-100 mx-auto d-block img-b2 bg-skyblue p5" alt="Businees Reg" />
                                </div>
                            </div>
                            <div class="col-12 col-md-6 shadow-small py-3 my-1">
                                <h5 class="text-danger">Contact Details :</h5>
                                <h6><strong class="text-violet">Chair Name:</strong> <span class="text-skyblue">Bharathi Aytha</span></h6>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">misxxxxxxxxxx@nriva.org</a></div>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">515-XXX-XXXX</span></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 mt-4">
                                <h4 class="text-violet">Competition Categories:</h4>
                                <ul class="list-unstyled fs16 miss-nriva-mrs-nriva-details-list pl20">
                                    <li>
                                        Miss NRIVA
                                        <ul class="list-unstyled fs16 miss-nriva-mrs-nriva-details-list-li-arrow pl20">
                                            <li>Age: Between 16 years and 25 years (as on July 5th 2019)</li>
                                            <li>Single</li>
                                        </ul>
                                    </li>
                                    <li>
                                        Mrs. NRIVA
                                        <ul class="list-unstyled fs16 miss-nriva-mrs-nriva-details-list-li-arrow">
                                            <li>Married women</li>
                                        </ul>
                                    </li>
                                </ul>
                                <p>
                                    To become Miss NRIVA or Mrs. NRIVA a contestant must first enter in to a qualifying round at local chapter competition, and then compete to represent their chapter at the national level. From each chapter
                                    <strong> FIRST, SECOND AND THIRD </strong> place winners will be selected.<strong> FIRST </strong> place contestant will be selected for Nationals and for any reason if
                                    <strong> FIRST place winner</strong> can’t make it our National Convention then <strong>SECOND</strong> place winner will be given opportunity to compete. If Second place winner is also unable to make it
                                    then the <strong>THIRD</strong> place winner will be given opportunity. The National Competition is going to be held at our NRIVA 5th Global Convention in Detroit during July 5th to July 7th, 2019.
                                </p>
                            </div>
                            <div class="col-12 mt-2">
                                <h4 class="text-violet">Competition Guidelines:</h4>
                                <h5 class="text-skyblue">Qualifying Round (Chapter Level Competition):</h5>
                                <ul class="list-unstyled fs16 miss-nriva-mrs-nriva-details-list pl20">
                                    <li>
                                        All performances will be judged live based on their introduction, Traditional Wear appearance and Interview questions.
                                    </li>
                                    <li>
                                        All the contestants should first brief their introductions in 1-2 minutes about who they are, what they do, their passion, strengths and anything that judges should know about them
                                    </li>
                                    <li>
                                        Each Participant will be asked 2-3 questions by the judges.
                                    </li>
                                    <li><strong>ONE </strong> winning contestants from each chapter will get a chance to compete for the title during NRIVA convention that is going to be held in Detroit during July 5th to July 7th.</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h5 class="text-skyblue">Finals (At the Convention)</h5>
                                <p class="mb-0">Finals will consist of two rounds. <strong>Only winners of the first round will advance to the Round 2</strong>.</p>
                                <p class="mb-0">
                                    All performances will be judged live.
                                </p>
                                <div class="pt-3">
                                    <h6 class="text-danger font-weight-bold">Round 1:</h6>
                                    <ul class="list-unstyled fs16 miss-nriva-mrs-nriva-details-list pl20">
                                        <li>
                                            All the contestants should first brief their introductions in 1 minute about who they are, what they do, their passion, strengths and anything that judges should know about them
                                        </li>
                                        <li>
                                            Each participant will then perform a fashion walk in Traditional Wear on the stage for one minute.
                                        </li>
                                        <li>
                                            10 winners from this round will advance to the Second round.
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <h6 class="text-danger font-weight-bold">Round 2:</h6>
                                    <ul class="list-unstyled fs16 miss-nriva-mrs-nriva-details-list pl20">
                                        <li>
                                            Each participant will perform a fashion walk on the stage for one minute in an Evening Gown.
                                        </li>
                                        <li>
                                            Each participant will be presenting a talent that they are good at, for 2 minutes
                                        </li>
                                        <li>
                                            Each Participant then will be asked 2-3 questions by the judges.
                                        </li>
                                        <li>
                                            3 winners from this round will be declared Winner and First and Second runner-ups
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <h4 class="text-violet mb-3">General Guidelines:</h4>
                                    <ul class="list-unstyled fs16 miss-nriva-mrs-nriva-details-list pl20">
                                        <li>
                                            This competition is only for NRIVA members. However, we encourage all the members to become Life member.
                                        </li>
                                        <li>
                                            Participants need to send a copy of age proof (for all contests) and marriage certificate (for Mrs. NRIVA only)
                                        </li>
                                        <li>
                                            Decent attire is expected at all times.
                                        </li>
                                        <li>
                                            Applicants must agree to abide by all the rules, as changed from time to time by NRIVA Organizers.
                                        </li>
                                        <li>
                                            Incorrect information will result in disqualification, whether discovered prior to, during or after participation.
                                        </li>
                                        <li>
                                            All expenses will have to be borne by the contestant
                                        </li>
                                        <li>
                                            The applicant will have to participate in a disciplined and diligent manner throughout as per schedule given.
                                        </li>
                                        <li>
                                            Incorrect information will result in disqualification, whether discovered prior to, during or after participation.
                                        </li>
                                        <li>
                                            The decision of the judges is final in all cases.
                                        </li>
                                        <li>
                                            The schedule of events and qualification rounds are subject to change at the discretion of the NRIVA.
                                        </li>
                                        <li>
                                            The Organizers are not responsible for non-completion / non-occurrence of the event.
                                        </li>
                                        <li>
                                            In the event of any dispute, the Organizers decisions are final and binding on the applicant.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12">
                                <h4 class="text-violet mb-3">Description of the Competition terms:</h4>
                                <h6 class="text-skyblue mb-1">Talent:</h6>
                                <p>
                                    Some possible talents include singing, dancing, gymnastics, instrumental music, dramatic or comedy monologues etc. Other talents that can be performed solo on a stage are also possible. Scoring is based
                                    on contestant's skill and personality, interpretive ability, technical skill level, stage presence and the totality of all elements, including costume, props, voice, use of body and choreography
                                </p>
                                <h6 class="text-skyblue mb-1">Traditional Wear:</h6>
                                <p>
                                    Each contestant appears on stage in an outfit of her own choosing, representative of what she would wear to anIndian traditional dress.
                                </p>
                                <h6 class="text-skyblue mb-1">Evening Gown:</h6>
                                <p>
                                    Each contestant appears on stage in an outfit of her own choosing, representative of what she would wear to a formal, black tie social event. Pantsuits, cocktail dresses and evening gowns are all
                                    appropriate for this phase of the competition.
                                </p>
                                <h6 class="text-skyblue mb-1">On stage Interview Questions:</h6>
                                <p>
                                    Each contestant will be asked one on-stage questions. Contestants are questioned on their background their educational and career goals, their opinions on current events and social issues, and their
                                    interests, hobbies and extracurricular activities. Scoring is based on overall communication skills, including personality, intelligence, validated opinions, emotional control, overall first impression
                                    and personal appearance.
                                </p>
                            </div>
                            <div class="col-12 mt-3">
                                <img src="images/Miss_MRS-NRIVA_Flyer.jpeg" class="img-fluid mx-auto d-block" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
