<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/convention.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-sm-5 col-md-4 col-lg-6 my-1 my-md-auto">
                                <h5 class="border-bottom pb-2 d-inline-block">Family / Individual</h5>
                            </div>
                            <div class="col-12 col-sm-7 col-md-8 col-lg-6 my-1 my-md-auto">
                                <div class="text-center text-sm-right">
                                    <a href="registration.php" class="btn btn-lg btn-danger text-uppercase fs-xs-15 px-3 px-md-4 fs-sm-15">Click Here For Registration </a>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card-body px-0">
                                    <div class="table-responsive">
                                        <table class="datatable table-striped table-bordered table table-hover table-center mb-0">
                                            <tbody class="fs16">
                                                <tr>
                                                    <th class="text-center" width="9%">S.No</th>
                                                    <th class="text-left" width="52%">Registration Categories:</th>
                                                    <th class="text-center" width="20%">Untill 07/05/2019</th>
                                                </tr>
                                                <tr>
                                                    <td align="center">1</td>
                                                    <td>Family of 4 Admission ( 2 Adults + 2 Children)</td>
                                                    <td align="center">$ 250.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">2</td>
                                                    <td>Kalyanam</td>
                                                    <td align="center">$ 116.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">3</td>
                                                    <td>Couple(2 Members - Admission Only)</td>
                                                    <td align="center">$ 125.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">4</td>
                                                    <td>Shatamanam Bhavathi (Per Family)</td>
                                                    <td align="center">$ 250.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">5</td>
                                                    <td>Single (Admission Only)</td>
                                                    <td align="center">$ 75.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">6</td>
                                                    <td>Business Conference (Per Person)</td>
                                                    <td align="center">$ 150.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">7</td>
                                                    <td>Womens Conference (Per Person)</td>
                                                    <td align="center">$ 50.00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <h5 class="border-bottom pb-2 d-inline-block">Donors Registration Categories:</h5>
                                <div class="card-body px-0">
                                    <div class="table-responsive">
                                        <table class="datatable table-striped table-bordered table table-hover table-center mb-0">
                                            <tbody>
                                                <tr class="latosemibold_italic">
                                                    <th class="text-center" width="7%">S.No</th>
                                                    <th class="text-left" width="13%">Sponsor Category</th>
                                                    <th class="text-center" width="10%">Amount</th>
                                                    <th class="text-left" width="40%">Benefits</th>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="text-center">
                                                        <span>1</span>
                                                    </td>
                                                    <td>
                                                        <span>Sapphire </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>$25000 </span>
                                                    </td>
                                                    <td class="tabletext">
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                                    <li>2 Vendor booth spaces</li>
                                                                    <li>3 Hotel Rooms(3 nights)</li>
                                                                    <li>2 Shathamanam Bhavathi</li>
                                                                    <li>Admission Tickets (20)</li>
                                                                    <li>Banquet Admission (20)</li>
                                                                    <li>Kalyanam Tickets (12)</li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                                    <li>Banner on the Website</li>
                                                                    <li>Recognition on main Statge</li>
                                                                    <li>Reserved Seating at the Convention</li>
                                                                    <li>Full page advertisement in Souvenir</li>
                                                                    <li>Lunch/Dinner in VIP Area</li>
                                                                    <li>Digital Banner on the Main Stage</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="text-center">
                                                        <span>2</span>
                                                    </td>
                                                    <td>
                                                        <span>Diamond </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>$20000 </span>
                                                    </td>
                                                    <td class="tabletext">
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                                    <li>1 Vendor booth space</li>
                                                                    <li>3 Hotel Rooms (3 nights)</li>
                                                                    <li>2 Shathamanam Bhavathi</li>
                                                                    <li>Admission Tickets (15)</li>
                                                                    <li>Banquet Admission (15)</li>
                                                                    <li>10 Kalyanam Tickets</li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                                    <li>Banner on the Website</li>
                                                                    <li>Recognition on main Statge</li>
                                                                    <li>Reserved Seating at the Convention</li>
                                                                    <li>Full page advertisement in Souvenir</li>
                                                                    <li>Lunch/Dinner in VIP Area</li>
                                                                    <li>Digital Banner on the Main Stage</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="text-center">
                                                        <span>3</span>
                                                    </td>
                                                    <td>
                                                        <span>Platinum </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>$15000 </span>
                                                    </td>
                                                    <td class="tabletext">
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                                    <li>
                                                                        1 Vendor booth space
                                                                    </li>
                                                                    <li>
                                                                        2 Hotel Rooms (3 nights)
                                                                    </li>
                                                                    <li>
                                                                        2 Shathamanam Bhavathi
                                                                    </li>
                                                                    <li>
                                                                        Admission Pack (12 Tickets)
                                                                    </li>
                                                                    <li>
                                                                        Banquet Admission (12)
                                                                    </li>
                                                                    <li>
                                                                        Kalyanam Tickets (8)
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                                    <li>
                                                                        Banner on the Website
                                                                    </li>
                                                                    <li>
                                                                        Recognition on main Statge
                                                                    </li>
                                                                    <li>
                                                                        Reserved Seating at the Convention
                                                                    </li>
                                                                    <li>
                                                                        Full page advertisement in Souvenir
                                                                    </li>
                                                                    <li>
                                                                        Lunch/Dinner in VIP Area
                                                                    </li>
                                                                    <li>
                                                                        Digital Banner on the Main Stage
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="text-center">
                                                        <span>4</span>
                                                    </td>
                                                    <td>
                                                        <span>Gold</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>$10000</span>
                                                    </td>
                                                    <td class="tabletext">
                                                        <div class="row">
                                                            <div class="col-12 col-lg-6">
                                                                <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                                    <li>
                                                                        1 Vendor booth space(10x10)
                                                                    </li>
                                                                    <li>
                                                                        2 Hotel Rooms (3 nights)
                                                                    </li>
                                                                    <li>
                                                                        2 Shathamanam Bhavathi
                                                                    </li>
                                                                    <li>
                                                                        Admission Tickets (10)
                                                                    </li>
                                                                    <li>
                                                                        Banquet Tickets (10)
                                                                    </li>
                                                                    <li>
                                                                        6 Kalyanam Tickets
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-12 col-lg-6">
                                                                <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                                    <li>
                                                                        Banner on the Website
                                                                    </li>
                                                                    <li>
                                                                        Recognition on main Statge
                                                                    </li>
                                                                    <li>
                                                                        Reserved Seating at the Convention
                                                                    </li>
                                                                    <li>
                                                                        Full page advertisement in Souvenir
                                                                    </li>
                                                                    <li>
                                                                        Lunch/Dinner in VIP Area
                                                                    </li>
                                                                    <li>
                                                                        Digital Banner on the Main Stage
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="text-center">
                                                        <span>5</span>
                                                    </td>
                                                    <td>
                                                        <span>Silver </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>$5,000 </span>
                                                    </td>
                                                    <td>
                                                        <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                            <li>
                                                                Banner on the Website
                                                            </li>
                                                            <li>
                                                                8 Admission, Banquet Tickets (8)
                                                            </li>
                                                            <li>
                                                                Kalyanam Tickets (4)
                                                            </li>
                                                            <li>
                                                                1 Hotel Rooms (3 nights)
                                                            </li>
                                                            <li>
                                                                Reserved Seating
                                                            </li>
                                                            <li>
                                                                Half page advertisement in Souvenir
                                                            </li>
                                                            <li>
                                                                Shathamanam Bhavathi ticket (1)
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="text-center">
                                                        <span>6</span>
                                                    </td>
                                                    <td>
                                                        <span>Bronze </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>$2,500</span>
                                                    </td>
                                                    <td>
                                                        <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                            <li>
                                                                <span>6Admission,6 Banquet Tickets</span>
                                                            </li>
                                                            <li>
                                                                <span>2 Kalyanam Tickets</span>
                                                            </li>
                                                            <li>
                                                                <span>1 Hotel Rooms (3 nights)</span>
                                                            </li>
                                                            <li>
                                                                <span>Half page advertisement in Souvenir</span>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="text-center">
                                                        <span>7</span>
                                                    </td>
                                                    <td>
                                                        <span>Patron </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>$1,500</span>
                                                    </td>
                                                    <td>
                                                        <ul class="list-unstyled convention-details-list-li-arrow pl20 mr10 pt10 fs16">
                                                            <li>Admission Tickets (4)</li>
                                                            <li>Banquet Tickets (2)</li>
                                                            <li>Kalyanam Tickets (1)</li>
                                                            <li>1 Hotel Rooms (2 nights)</li>
                                                            <li>Listing in Souvenir</li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-6">
                                <h5 class="text-violet">Additional DONOR Benefits (Admission to)</h5>
                                <ul class="list-unstyled convention-details-list pl20 pt10 fs16">
                                    <li>Business Conference</li>
                                    <li>Women's Conference</li>
                                    <li>Youth Conference</li>
                                    <li>Youth Late Night Lockin</li>
                                    <li>CME</li>
                                    <li>Matrimonial Admission</li>
                                    <li>Immigration Seminar</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 col-lg-6">
                                <h5 class="text-violet">$10,000 and above packages include</h5>
                                <ul class="list-unstyled convention-details-list pl20 pt10 fs16">
                                    <li>Banner on the Website</li>
                                    <li>Recognition on main Statge</li>
                                    <li>Reserved Seating at the Convention</li>
                                    <li>Full page advertisement in Souvenir</li>
                                    <li>Lunch/Dinner in VIP Area</li>
                                    <li>Digital Banner on the Main Stage</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="fs16 mb-1">All registrations are on first come first serve basis and are final</p>
                                <p class="fs16 mb-1">
                                    *Copy of ID / student ID/ Visitor’s Visa must be presented when collecting the registration kit.
                                </p>
                                <p class="fs16 mb-1">
                                    ** Family Picture in Souvenir for $1000 and above donors. $5K Donor ¼ page adv. ; $10K Donor ½ page adv. ; $25k and above full-page adv.
                                </p>
                                <p class="m0">
                                    <span>
                                        <span class="text-uppercase"><b>MAKE CHECKS PAYABLE TO:</b></span> NRIVA<br />
                                        Cheque Mailing Address:-<br />
                                        NRIVA 5th Global Convention<br />
                                        24328 Amanda Ln,<br />
                                        Novi, MI 48375
                                    </span>
                                </p>
                                <p class="m0">
                                    <span>
                                        <span><b>For online registration</b> and Convention programs &amp; updates, please visit <a class="text-orange" href="#" target="_blank">convention.nriva.org</a></span>
                                    </span>
                                </p>
                                <p class="m0">
                                    <span>
                                        <span>
                                            <b>For Registration Information, email,&nbsp;</b><a class="text-orange" href="#">regxxxxxxxxxx@nriva.org</a>&nbsp;or reach
                                            <a class="text-orange" href="event_registration_committee.php">Registration Committee.</a>
                                        </span>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
