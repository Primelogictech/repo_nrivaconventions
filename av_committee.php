<?php include 'header.php';?>
	
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small">
        <div class="row">
            <div class="col-12 px-0">
                <div class="leadership-heading-bg p-3">
                    <h4 class="mb-0">Av Committee</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 py-4 p-md-4 p40">
            	<div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/av.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
            	<div class="row">
		            <div class="col-12 py-4">
		            	<div class="text-center">
		            		<h5><span class="text-violet">Committee Email:</span><span class="text-danger">av2019@nriva.org</span></h5>
		            	</div>
		            </div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Ram Nichanametla</h6>
		            				<div class="pt-1">Chair</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">rnjxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Suneel Charugundla</h6>
		            				<div class="pt-1">Co-Chair</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">chsxxxxxxxxxx@nriva.org</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">732-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Mallik Guduguntla</h6>
		            				<div class="pt-1">Co-Chair</div>
		            				<div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">malxxxxxxxxxx@gmail.com</a>
		            				</div>
		            				<h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
		            			</div>
		            		</div>
            			</div>
            		</div>
            	</div>
            	<div class="row p-2">
            		<div class="col-12 col-md-6 col-lg-4 my-1 p-3 border-violet-dashed-1">
            			<div class="row">
            				<div class="col-4">
            					<div>
            						<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
            					</div>
            				</div>
            				<div class="col-8">
            					<h6 class="mb-0">Raghuram Bikkumalla</h6>
            					<div class="pt-1">Member</div>
            					<div>
            						<a href="#" class="text-danger text-decoration-none">rajxxxxxxxxxx.com</a>
            					</div>
            					<h6 class="mb-0 pt-1 text-violet font-weight-bold">860-XXX-XXXX</h6>
            				</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 p-3 border-violet-dashed-1">
            			<div class="row">
            				<div class="col-4">
            					<div>
            						<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
            					</div>
            				</div>
            				<div class="col-8">
            					<h6 class="mb-0">Anil Nichenametla</h6>
            					<div class="pt-1">Member</div>
            					<div>
            						<a href="#" class="text-danger text-decoration-none">anilxxxxxxxxxx.com</a>
            					</div>
            					<h6 class="mb-0 pt-1 text-violet font-weight-bold">818-XXX-XXXX</h6>
            				</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 p-3 border-violet-dashed-1">
            			<div class="row">
            				<div class="col-4">
            					<div>
            						<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
            					</div>
            				</div>
            				<div class="col-8">
            					<h6 class="mb-0">Venu Ramini</h6>
            					<div class="pt-1">Member</div>
            					<div>
            						<a href="#" class="text-danger text-decoration-none">anilxxxxxxxxxx.com</a>
            					</div>
            					<h6 class="mb-0 pt-1 text-violet font-weight-bold">586-XXX-XXXX</h6>
            				</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 p-3 border-violet-dashed-1">
            			<div class="row">
            				<div class="col-4">
            					<div>
            						<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
            					</div>
            				</div>
            				<div class="col-8">
            					<h6 class="mb-0">Giridhar Kotagiri</h6>
            					<div class="pt-1">Member</div>
            					<div>
            						<a href="#" class="text-danger text-decoration-none">girixxxxxxxxxx.com</a>
            					</div>
            					<h6 class="mb-0 pt-1 text-violet font-weight-bold">404-XXX-XXXX</h6>
            				</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 p-3 border-violet-dashed-1">
            			<div class="row">
            				<div class="col-4">
            					<div>
            						<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
            					</div>
            				</div>
            				<div class="col-8">
            					<h6 class="mb-0">Rajesh Chandupatla</h6>
            					<div class="pt-1">Member</div>
            					<div>
            						<a href="#" class="text-danger text-decoration-none">chaxxxxxxxxxx.com</a>
            					</div>
            					<h6 class="mb-0 pt-1 text-violet font-weight-bold">904-XXX-XXXX</h6>
            				</div>
            			</div>
            		</div>
            	</div>
        		<div class="row">
        			<div class="col-12 col-md-6 my-1 px-2">
		                <div class="bg-white p3 shadow-small">
		                    <div class="bg-light-violet p10">
		                        <div class="row">
		                            <div class="col-4 col-sm-3 col-md-3 pr-0">
		                                <div class="p3 border-radius-10 bg-white">
		                                    <img src="images/no-image1.jpg" class="img-fluid border-radius-10 w-100" alt="">
		                                </div>
		                            </div>
		                            <div class="col-8 col-sm-9 col-md-9 px-0">
		                                <div class="pl25 pr10">
		                                    <h5 class="mb-1">Ramesh Kalwala</h5>
		                                    <div class="mb-1">Advisor - National</div>
		                                    <h6 class="mb-1">
		                                        <a href="#" class="text-danger text-decoration-none text-break">ramxxxxxxxxxx@gmail.com</a>
		                                    </h6>
		                                </div>
		                                <div class="phone-stiff">
		                                    <i class="fas fa-mobile-alt text-skyblue fs18 mr-2"></i>
		                                    <span class="fs20">571-XXX-XXXX</span>
		                                </div>
		                                <div class="d-block d-lg-none pl25">
		                                    <span class="fs20">630-XXX-XXXX</span>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
        		</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>