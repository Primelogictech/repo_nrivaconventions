<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/business-speakers.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Grandhi Mallikarjuna Rao
                                        <span class="text-yellow fs16 pl10">- Founder Chairman of GMR Group,</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/grandhi-mallikarjuna.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height">
                                            <p class="pb-3"></p>
                                            <ul class="speakers-list list-unstyled pl20 pr10 pt10 fs16">
                                                <li>
                                                    Grandhi Mallikarjuna Rao is a mechanical engineer, billionaire industrialist, the founder chairman of<a href="#" target="_blank" class="text-danger text-decoration-none"> GMR Group</a>, a
                                                    global infrastructure developer and operator based in India
                                                </li>
                                                <li>G M Rao was born on 14 July 1950, in<a href="#" target="_blank" class="text-danger text-decoration-none"> Srikakulam district</a> in AP, India.</li>
                                                <li>
                                                    After graduating from<a href="#" target="_blank" class="text-danger text-decoration-none"> Andhra University</a>, Rao joined the Andhra Pradesh Government as a junior engineer. He soon
                                                    entered the trading of commodities, not content with staying on just a day job.
                                                </li>
                                                <li>
                                                    After developing good relations with suppliers and customers in the business of commodities trading, he acquired a failing jute mill at a bargain, this venture proved to be lucrative and
                                                    allowed GM Rao to use this leverage from local banks to acquire other assets.
                                                </li>
                                                <li>
                                                    Eventually GM Rao divested his stake in a multitude of industries and started a bank named Vysya Bank in collaboration with<a href="#" class="text-danger text-decoration-none"> ING</a>.
                                                </li>
                                                <li>
                                                    Rao eventually diluted his stake in ING Vysya by selling his stake in whole for 340 crores.
                                                </li>
                                                <li>
                                                    The cash from the bank stake sale allowed his entry into the power business and infrastructure development through
                                                    <a href="#" target="_blank" class="text-danger text-decoration-none"> GMR Group</a> the leader in India as an airport developer by revenues, asset size, and market capitalization.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Gampa Nageshwer Rao
                                        <span class="text-yellow fs16 pl10">- MA (psy ), MS , MPhil (psy) Psychologist and Inspirational Speaker Founder; IMPACT FOUNDATION</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/gampa-nageshwer-rao.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height">
                                            <p class="pb-3"></p>
                                            <ul class="speakers-list list-unstyled pl20 pr10 pt10 fs16">
                                                <li>
                                                    Psychologist, Soft Skills Trainer, Master Motivator, and Development Officer-LIC
                                                </li>
                                                <li>
                                                    He has been honored by Sri N. Chandrababu Naidu Chief Minister of Andhra Pradesh, India as an Outstanding Speaker
                                                </li>
                                                <li>
                                                    Travelled to many countries including Italy, USA, UK, France, Germany, Switzerland, China, Hong Kong, Sri Lanka, Japan, Turkey and Egypt
                                                </li>
                                                <li>
                                                    Participated in more than 1200 live discussions in many TV channels
                                                </li>
                                                <li>
                                                    Published books including PADANDI MUNDUKU, NEEDE JAYAM, COURAGE, TOP, Engineering Student No-1, and Be a Champion
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Harsha V. Agadi
                                        <span class="text-yellow fs16 pl10"> - President, CEO & Director, Crawford & Company</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/harsha.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <p class="pb-3"></p>
                                        <div class="speakers-content speakers-details-height">
                                            <ul class="speakers-list list-unstyled pl20 pr10 pt10 fs16">
                                                <li>
                                                    Mr. Harsha V. Agadi has been the Chief Executive Officer and President of Crawford &amp; Company since June 2016.
                                                </li>
                                                <li>
                                                    He served in C-Suite Executive roles at Quiznos, LLC, Little Caesar Enterprises, Inc, Friendly's Ice Cream, LLC, Cajun Operating Company, Inc. (Formerly Church's Chicken, Inc.), and other
                                                    large enterprises.
                                                </li>
                                                <li>
                                                    Mr. Agadi is an expert on global growth for American companies and a regular invited speaker at universities, conventions and professional associations.
                                                </li>
                                                <li>
                                                    He has over 30 years of experience across several companies, including American Express, PepsiCo Inc. and General Foods.
                                                </li>
                                                <li>
                                                    He has managed a $2.0 billion restaurant chain with 4,500 locations across 22 countries.
                                                </li>
                                                <li>
                                                    Mr. Agadi holds an MBA from Duke University and a B.Com from the University of Bombay in 1984.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Raj Neravati
                                        <span class="text-yellow fs16 pl10">- Founder & CEO Hug Innovations Corp</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/raja-neravati.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height">
                                            <p class="pb-3"></p>
                                            <p>
                                                Raj is a Technology Entrepreneur backed by 19 years of industry experience. Raj has started his career as a developer and grew in the corporate ladder playing several roles and his last was a
                                                Chief Operating Officer for a global IT Services company based out of Dallas. His domain experience spans across BFSI, Healthcare and Airline domains. An accomplished leader with a proven
                                                track record of building, managing and delivering profitable growth.
                                            </p>
                                            <h5 class="text-violet">Key Accomplishments</h5>
                                            <ul class="speakers-list list-unstyled pl20 pr10 pt10 fs16">
                                                <li>
                                                    Founded a wearable + IOT company with a vision of redefining Human Machine Interaction using Gestures.
                                                </li>
                                                <li>
                                                    Grew business from $2 million USD to $100 million+ as a corporate executive
                                                </li>
                                                <li>
                                                    Managed a publicly listed company as a “C” Level Executive
                                                </li>
                                                <li>
                                                    Serving as President for one of the largest tech communities in Asia (Non-Profit)
                                                </li>
                                                <li>
                                                    Industry speaker with active participation in several domestic and international conferences
                                                </li>
                                                <li>
                                                    Hug Innovations is a next gen IOT and wearable company that specializes in the fields of advanced Gesture recognition, Industrial IOT, and Healthcare.
                                                </li>
                                            </ul>
                                            <h5 class="text-violet">Key Learnings</h5>
                                            <ul class="speakers-list list-unstyled pl20 pr10 pt10 fs16">
                                                <li>Learnt the value of <strong>“60 seconds”</strong> while building an analytical platform for corporate bond market while working on Wall Street</li>
                                                <li>Learnt the value of <strong>“Customer”</strong> working on a CIO led transformation initiative for one of the largest Insurance companies in the world</li>
                                                <li>Learnt the value of <strong>“People, Emotions and Corporate Power Battles”</strong> being one of the core members driving the merger of two largest airlines in the world</li>
                                                <li>
                                                    <strong>“Learnt to let go”</strong> portfolios and re-discover inner-self on a fast track to leadership in corporate roles. Only executive to receive 8 promotions in a 9 year career for a
                                                    corporate.
                                                </li>
                                                <li>
                                                    Understood the meaning of <strong>“Competitive Strategy”</strong> from Michael Porter as part of 3 day CEO workshop for World’s fastest growing companies at Harvard Business School,
                                                    Boston.
                                                </li>
                                            </ul>
                                            <div class="fs16 pb-4">LinkedIn: <a href="#" class="text-danger text-decoration-none">https://in.linkedin.com/in/rajaneravati</a></div>
                                            <h5 class="text-violet">Hug Innovations:</h5>
                                            <p>
                                                Hug Innovations is a next gen IOT and wearable company that specializes in the fields of advanced Gesture recognition, Industrial IOT and Healthcare. Hug Innovations uses consumer technology
                                                form factors like smartwatch, smartphone, IOT devices, and handheld remotes to port its gesture control experience which is based on relative motion detection. Our patented technology on
                                                Gesture recognition technology uses a combination of Machine Learning, Artificial Intelligence, Cognitive Science, and Contextual Intelligence to deliver an unparalleled touchless interactive
                                                experience with smart devices.
                                            </p>
                                            <p>
                                                Hug is currently developing a first of its kind Developer platform and exposing its Gesture recognition technology in the form of a Gestures SDK, which developers (iOS, Android, .Net, Java)
                                                can use to build the future of human-machine interaction using gesture control.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Anil Gupta
                                        <span class="text-yellow fs16 pl10">- Founder of Sri Vasavi Group since 1954</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/anil-gupta1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height">
                                            <p class="pb-3"></p>
                                            <ul class="speakers-list list-unstyled pl20 pr10 pt10 fs16">
                                                <li>
                                                    Anil belongs to the elite businessmen family of Doranala Krishnaiah Setty, the founder of Sri Vasavi Group since 1954.
                                                </li>
                                                <li>
                                                    Anil is the CEO for RIGO water filtration equipment which are known to be the pioneers for lab and industrial water filtration market in India.
                                                </li>
                                                <li>
                                                    Anil was also bestowed upon as the CNBC future of India Young Entrepreneur of the year 2016.
                                                </li>
                                                <li>
                                                    He is the backbone and the mind behind WeVysya.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Raj N. Phani
                                        <span class="text-yellow fs16 pl10">- Founder and Chairman of Zaggle</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/raj-phan1i.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height">
                                            <p class="pb-3"></p>
                                            <ul class="speakers-list list-unstyled pl20 pr10 pt10 fs16">
                                                <li>
                                                    Raj N. Phani, Founder and Chairman of Zaggle, has over 20 years of business consulting experience.
                                                </li>
                                                <li>
                                                    He founded Zaggle in 2011 as a network-agnostic payment platform on which merchants and consumers could seamlessly interact.
                                                </li>
                                                <li>
                                                    A key contributor to the Indian start-up ecosystem and a true visionary, he won the “Small Scale Entrepreneur of the Year” award in 2007.
                                                </li>
                                                <li>
                                                    A business graduate from NYU Stern School of Business, Raj has successfully led the company to over INR300 Crores valuation and another two companies to over INR150 Crore valuation.
                                                </li>
                                                <li>
                                                    When he is not mentoring start-ups on the power of innovation, creativity and entrepreneurship, he enjoys spending personal time organic farming, watching comedy shows, playing golf,
                                                    cricket and badminton.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>

                            <div class="col-12">
                                <div class="bg-violet speaker-name px-4 py-3">
                                    <div class="mb-0 fs22 text-white">
                                        Dayakar Veerlapati
                                        <span class="text-yellow fs16 pl10">- Founder and CEO of S2Tech</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-2 d-none d-md-block pt10">
                                        <div>
                                            <img src="images/Dayakar1.jpg" class="img-fluid mx-auto d-block" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9 col-lg-9">
                                        <div class="speakers-content speakers-details-height">
                                            <p class="pb-3"></p>
                                            <ul class="speakers-list list-unstyled pl20 pr10 pt10 fs16">
                                                <li>Day, who is the founder and CEO of<a href="#" class="text-danger text-decoration-none"> S2Tech</a>, or Seven Seas Technologies, came to St. Louis from India in 1987.</li>
                                                <li>
                                                    Day established not only a successful IT consulting company in 1997, but also a nonprofit organization that funds the education of rural children living in India.
                                                </li>
                                                <li>
                                                    Day is looking towards future opportunities to better the Medicaid system through IT.
                                                </li>
                                                <li>
                                                    Mr. Day Veerlapati will deliver a talk on "Road to Robot Taxis" at our Business Conference which will cover areas of autonomous vehicles and artificial intelligence.
                                                </li>
                                                <li>
                                                    Day Veerlapati established the Fortune Fund to support educational opportunities for children from impoverished families in rural Indian communities.
                                                </li>
                                                <li>
                                                    Day enjoys reading books and taking walks.
                                                </li>
                                                <li>
                                                    A philosophy he has instilled in the company is for everyone to "Sharpen their saw" and grow a little more as every day passes.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div>
                                            <div class="speakers-more-details">Show More +</div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dashed-hr" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<script>
    $(".speakers-more-details").click(function () {
        $(this).parent().parent().parent().find(".speakers-content").toggleClass("speakers-details-height");

        if ($(this).text() == "Show More +") {
            $(this).text("Show Less - ");
        } else {
            $(this).text("Show More +");
        }
    });
</script>

<?php include 'footer.php';?>
