<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/youth-banquet.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row mx-md-1">
                            <div class="col-12 col-md-6 my-1 px-0 pl-md-0 pr-md-3">
                                <div>
                                    <img src="images/youth-banquent2.jpg" class="img-fluid w-100 mx-auto d-block img-b2 bg-skyblue p5" alt="" />
                                </div>
                            </div>
                            <div class="col-12 col-md-6 shadow-small py-3 my-1">
                                <h5 class="text-danger">Contact Details :</h5>
                                <h6><strong class="text-violet">Chair Name:</strong> <span class="text-skyblue">Vikhyathi Pallerla</span></h6>
                                <div class="py-1 my-auto"><i class="far fa-envelope text-skyblue fs18"></i><a href="#" class="icon-positions text-black text-decoration-none fs16 lh16">vikxxxxxxxxxx@gmail.com</a></div>
                                <div class="py-1 my-auto"><i class="fas fa-mobile-alt text-skyblue fs18"></i><span class="icon-positions fs16 lh16">248-XXX-XXXX</span></div>
                            </div>
                            <div class="col-12 pt-3">
                                <h4 class="text-violet">
                                    Lights Camera Action!
                                </h4>
                                <p>
                                    A Hollywood-themed awards-styled banquet, this event will take place the night of July 5th as an alternative youth banquet. Dress code is American formal and come ready to show off your movie knowledge
                                    and meet some new people! There will be:
                                </p>
                                <ul class="list-unstyled pl20 youth-banquet-details-list fs16">
                                    <li>Photo booth</li>
                                    <li>Awards show themed buffet</li>
                                    <li>Movie trivia</li>
                                    <li>Oscar styled awards (try to get Best Dressed!)</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h6 class="text-thickgrey font-weight-bold">Imagine, Inspire, Ignite</h6>
                                <p>
                                    This event is a professional and motivational conference geared towards the leaders of the future. It will feature talks by many accomplished people along with career building and networking activities.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
