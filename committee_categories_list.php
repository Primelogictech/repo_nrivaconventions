<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/convention-committees.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Hari Raini</h6>
                                            <div class="pt-1">President Elect</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">harxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Srinivasa Rao Pandiri</h6>
                                            <div class="pt-1">General Secretary</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">panxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Ravi Ellendula</h6>
                                            <div class="pt-1">Co-Convener</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">ellxxxxxxxxxx@yahoo.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Hari Raini</h6>
                                            <div class="pt-1">President Elect</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">harxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Srinivasa Rao Pandiri</h6>
                                            <div class="pt-1">General Secretary</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">panxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Ravi Ellendula</h6>
                                            <div class="pt-1">Co-Convener</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">ellxxxxxxxxxx@yahoo.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Hari Raini</h6>
                                            <div class="pt-1">President Elect</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">harxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Srinivasa Rao Pandiri</h6>
                                            <div class="pt-1">General Secretary</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">panxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Ravi Ellendula</h6>
                                            <div class="pt-1">Co-Convener</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">ellxxxxxxxxxx@yahoo.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Hari Raini</h6>
                                            <div class="pt-1">President Elect</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">harxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Srinivasa Rao Pandiri</h6>
                                            <div class="pt-1">General Secretary</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">panxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Ravi Ellendula</h6>
                                            <div class="pt-1">Co-Convener</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">ellxxxxxxxxxx@yahoo.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Hari_Raini.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Hari Raini</h6>
                                            <div class="pt-1">President Elect</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">harxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">847-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Srinivasa_Rao_Pandiri.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Srinivasa Rao Pandiri</h6>
                                            <div class="pt-1">General Secretary</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">panxxxxxxxxxx@gmail.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">314-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 my-1 px-2">
                                <div class="convention-leaders-bg">
                                    <div class="row">
                                        <div class="col-5 pr-0 my-auto">
                                            <div>
                                                <img src="images/Ravi_Ellendula.jpg" class="img-fluid rounded border-radius-5 w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-7 pr-3 my-auto">
                                            <h6 class="mb-0">Ravi Ellendula</h6>
                                            <div class="pt-1">Co-Convener</div>
                                            <div class="pt-1">
                                                <a href="#" class="text-danger text-decoration-none mail">ellxxxxxxxxxx@yahoo.com</a>
                                            </div>
                                            <h6 class="mb-0 pt-1 text-violet font-weight-bold">248-XXX-XXXX</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
