<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/womens-conference.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <h4 class="text-center text-warning mb-4">8:00 AM - 9:00 AM Registration and Breakfast</h4>
                        <div class="mx-auto" style="max-width: 800px;">
                            <table class="datatable table-bordered table table-hover table-center mb-0 table-striped">
                                <tbody>
                                    <tr>
                                        <th width="11%" class="text-center bg-violet text-white">S.no</th>
                                        <th width="28%" class="text-center bg-violet text-white">Time</th>
                                        <th width="61%" class="text-center bg-violet text-white">Event Name</th>
                                    </tr>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td class="text-center">9:00AM - 9:30AM</td>
                                        <td>Keynote Speaker</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td class="text-center">9:30AM - 10:00AM</td>
                                        <td>Women Entrepreneurship and Leadership</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td class="text-center">10:00AM - 10:30AM</td>
                                        <td>Each one – Coach one Program</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td class="text-center">10:30AM - 11:00AM</td>
                                        <td>Financial Planning for Women</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5</td>
                                        <td class="text-center">11:00AM - 11:30PM</td>
                                        <td>Work life balance &amp; Stress Management (Speaker Dr.Padmaja Nandigama,Ph.D)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">6</td>
                                        <td class="text-center">11:30AM - 12:00PM</td>
                                        <td>Games and Entertainment</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">7</td>
                                        <td class="text-center">12:00PM - 1:00PM</td>
                                        <td>Questions from Audience and Vote of thanks</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="pt-3 pt-lg-5">
                            <img src="images/Vartika-Flyer.jpeg" class="img-fluid mx-auto d-block" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
