<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/business-conference.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 py-2">
                                <div class="text-center">
                                    <a href="business_interest_group_committee.php" target="_blank" class="btn btn-lg btn-info fs-xs-15 mx-0 mx-sm-2 px-2 px-sm-4 my-1 text-uppercase">Team</a>
                                    <a href="registration.php" target="_blank" class="btn btn-lg btn-danger fs-xs-15 mx-0 mx-sm-2 px-2 px-sm-4 my-1 text-uppercase">Convention Registration</a>
                                    <a href="business_plan_competition.php" target="_blank" class="btn btn-lg btn-warning fs-xs-15 mx-0 mx-sm-2 px-2 px-sm-4 my-1 text-uppercase text-white">Business Plan Competition Reg</a>
                                    <a href="speakers.php" class="btn btn-lg btn-primary fs-xs-15 mx-0 mx-sm-2 px-2 px-sm-4 my-1 text-uppercase">Speakers</a>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card-body px-0">
                                    <div class="table-responsive">
                                        <table class="datatable table-bordered table table-hover table-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Time</th>
                                                    <th>Session</th>
                                                    <th>Guest(s) or Speaker(s)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>8:00 - 9:00 AM</td>
                                                    <td>Registration and Breakfast</td>
                                                    <td>--</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>9:00 - 9:30 AM</td>
                                                    <td>Vasavites Business 2.0</td>
                                                    <td>Anil Gupta</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>9:30 - 10:00 AM</td>
                                                    <td>Technology Trends: IoT and Connected World</td>
                                                    <td>Raj Neravati</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" height="41" lass="tabletext" style="text-align: center !important;">
                                                        4
                                                    </td>
                                                    <td>10:00 - 10:30 AM</td>
                                                    <td>Technology Trends: Road to Robot Taxis</td>
                                                    <td>Dayakar Veerlapati</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>10:30 -11:00 AM</td>
                                                    <td>Keynote Speaker</td>
                                                    <td>Harsha Agadi</td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>11:00 - Noon</td>
                                                    <td>Fireside Chat (Entrepreneurial Journey)</td>
                                                    <td>Raj Neravati, Vijay Pradeep and Day Veerlapati</td>
                                                </tr>
                                                <tr>
                                                    <td>7</td>
                                                    <td>Noon - 1:00 PM</td>
                                                    <td>Lunch</td>
                                                    <td>--</td>
                                                </tr>
                                                <tr>
                                                    <td>8</td>
                                                    <td>1:00 - 1:30 PM</td>
                                                    <td>Six Superd Strategies for Building Wealth</td>
                                                    <td>Sekhar Vemparala</td>
                                                </tr>
                                                <tr>
                                                    <td>9</td>
                                                    <td>1:30 - 3:30 PM</td>
                                                    <td>Business Plan Competition</td>
                                                    <td>Judges: Raj Neravati, Vijay Pradeep,&amp; Guru Narayan</td>
                                                </tr>
                                                <tr>
                                                    <td>10</td>
                                                    <td>3:30 - 4:00 PM</td>
                                                    <td>High Impact Leadership</td>
                                                    <td>Gampa Nageshwer Rao</td>
                                                </tr>
                                                <tr>
                                                    <td>11</td>
                                                    <td>4:00 - 4:30 PM</td>
                                                    <td>Business Plan Competition Results</td>
                                                    <td>Judges</td>
                                                </tr>
                                                <tr>
                                                    <td>12</td>
                                                    <td>4:30 - 5:00 PM</td>
                                                    <td>Collaborative Economy and Opportunities for New Sources of Income</td>
                                                    <td>Sambhu Bommisetty &amp; Sravan Kollapudi</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <p>
                                    <strong class="text-danger">Disclaimer :</strong>
                                    <span class="font-weight-bold">(* Please note that the above schedule is subject to change to accommodate the needs of our guests and to improve coordination with other events of the day!)</span>
                                </p>
                            </div>
                            <div class="col-12 pt-3">
                                <div>
                                    <img src="images/Business-conference-Flyer.jpeg" class="img-fluid mx-auto d-block" alt="Business conference Flyer" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
