<?php include 'header1.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
        	<div class="col-12">
        		<h5>Features</h5>
        	</div>
        	<div class="col-12">
        		<div class="card-body px-0">
                    <div class="table-responsive">
                        <table class="datatable table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                <th>Sl NO.</th>
                                <th>
                                    <div>Sponsor Category</div>
                                    <div>(Amount)</div>
                                </th>
                                <th style="min-width: 300px;">Benefits</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <div>Sapphire</div>
                                    <div class="text-orange font-weight-bold pt-2">$25000</div>
                                </td>
                                <td>
                                    <ul class="list-unstyled pl20 mb-0">
                                        <li class="py-1">1.&nbsp;&nbsp;&nbsp;&nbsp;2 Vendor booth spaces</li>
                                        <li class="py-1">2.&nbsp;&nbsp;&nbsp;&nbsp;3 Hotel Rooms(3 nights)</li>
                                        <li class="py-1">3.&nbsp;&nbsp;&nbsp;&nbsp;2 Shathamanam Bhavathi</li>
                                        <li class="py-1">4.&nbsp;&nbsp;&nbsp;&nbsp;Admission Tickets (20)</li>
                                        <li class="py-1">
                                            5.&nbsp;&nbsp;&nbsp;&nbsp;Banquet Admission (20)
                                        </li>
                                        <li class="py-1">6.&nbsp;&nbsp;&nbsp;&nbsp;Kalyanam Tickets (12)</li>
                                        <li class="py-1">7.&nbsp;&nbsp;&nbsp;&nbsp;Banner on the Website</li>
                                        <li class="py-1">8.&nbsp;&nbsp;&nbsp;&nbsp;Recognition on main Statge</li>
                                        <li class="py-1">9.&nbsp;&nbsp;&nbsp;&nbsp;Reserved Seating at the Convention</li>
                                        <li class="py-1">
                                            10.&nbsp;&nbsp;&nbsp;&nbsp;Full page advertisement in Souvenir
                                        </li>
                                        <li class="py-1">11.&nbsp;&nbsp;&nbsp;&nbsp;Lunch/Dinner in VIP Area</li>
                                        <li class="py-1">12.&nbsp;&nbsp;&nbsp;&nbsp;Digital Banner on the Main Stage</li>
                                    </ul>
                                </td>
                                <td>
                                	<ul class="list-unstyled pl20 mb-0">
                                		<li class="py-1">2</li>
                                		<li class="py-1">3</li>
                                		<li class="py-1">2</li>
                                		<li class="py-1">20</li>
                                		<li class="py-1">20</li>
                                		<li class="py-1">12</li>
                                		<li class="py-1">3</li>
                                		<li class="py-1">1</li>
                                		<li class="py-1">2</li>
                                		<li class="py-1">5</li>
                                		<li class="py-1">2</li>
                                		<li class="py-1">1</li>
                                	</ul>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>