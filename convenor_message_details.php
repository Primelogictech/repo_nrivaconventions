<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small px-sm-30 py-4 p-md-4 p40">
            	<div class="text-center float-sm-none float-md-right px-2">
            		<img src="images/user.png" class="" width="160" alt="Nagender_Aytha" />
            		<div class="py-2">
            			<div class="text-danger fs16 text-center">Convenor</div>
            			<div class="text-danger fs12 text-center">- Convenor</div>
            		</div>
            	</div>
                <h4 class="mb-3 text-violet">Dear Members,</h5>
                <p>
					I feel delighted and honored to have been appointed as the Convener for the NRIVA 5th Global Convention which is going to be held from July 5th-July 7th 2019 in Detroit, Michigan. It is a great honor and privilege and I would like to thank the NRIVA leadership team for giving me this opportunity.
				</p>
				<p>
					A convention of this magnitude does not happen without the support from dedicated Volunteers, generous sponsors, exhibitors, members and well-wishers of this esteemed organization. We appreciate all our Sponsors for their generous donations and our exhibitors for their participation and generous support.
				</p>
				<p>
					We appreciate all our Convention Committee Chairs, Co-Chairs, members and advisors for their hard work, dedication and commitment. They have been working very hard for the past several months to make this convention successful.
				</p>
				<p>
					The convention has a lot of firsts and is being organized to provide you with 3 days of entertainment and education, and will surely instill a festive and spiritual feeling in everyone there. There is unlimited entertainment, sumptuous food, Business/Women/Youth Conferences, Awards/Youth Banquets, Matrimony re-defined, Shatamanam Bhavathi to honor our elders, Sri Lakshmi Narasimha Swamy Kalyanam with priests/idols/prasadam from Yadadri. There is an Americas’ Got Talent styled V Got Talent, Beauty Pageant competitions, a Dance Ballet on Vasavi Matha, Real-Estate/Immigration/Financial Planning/Stocks and Investments Sessions, panel discussions and many more.
				</p>
				<p>
					Don’t miss this great opportunity. Be a part of this great occasion, witness our hospitality and carry away the great moments to cherish for your life time. Let us together make the Detroit Convention a grand success.
				</p>
				<div>
					<div class="text-violet font-weight-bold fs16">Yours truly,</div>
					<div class="text-black fs16">Convenor</div>
					<div class="text-orange fs16">Convenor, NRIVA</div>
					<div class="fs16 py-3">Visit <a href="#" class="text-orange">convention.nriva.org</a> for updates and further details</div>
				</div>
				<div class="text-violet fs15 font-weight-bold">
					Why wait? Go “Destination Detroit!”
				</div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>